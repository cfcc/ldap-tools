DIST_PATH=/opt/ldap-tools
PYTHON=/usr/bin/python3

.PHONY: clean verify_scripts dist copy_data_files install

clean:
	-rm MANIFEST
	-rm *.prof
	-rm -r build

dist:
	$PYTHON setup.py sdist

verify_scripts:
	grep -l "^[ ]*sys.path.append..src.." `$(PYTHON) -c "import setup; print('\n'.join(setup.INSTALL_SCRIPTS))"` || exit 0

copy_data_files:
	cp ./etc/clg.conf.example $(DIST_PATH)/etc
	cp ./etc/bad-words.txt $(DIST_PATH)/etc
	cp ./etc/specialeng.txt $(DIST_PATH)/etc
	cp ./etc/verify_ou.yaml $(DIST_PATH)/etc

install: verify_scripts copy_data_files
	$(PYTHON) setup.py install --prefix=$(DIST_PATH)

#!/usr/bin/env python2.6
import logging
import sys
import unittest

sys.path.append("src")
import cfccldap.config
import cfccldap.group
import cfccldap.util
import cfccldap.dbutil

from cfccldap.plugins.base import PLUGINS, initPlugins

DEBUG = False
LOG_FILENAME = 'test/test_groups_ad.log'
CONFIG_FILENAME = 'etc/clg.conf'

TEST_ENV = 'test'
TEST_GROUP = "test_group"
TEST_GROUP_DN = "cn=%s,ou=groups,dc=cfcc,dc=int" % TEST_GROUP


class GroupsActiveDirectoryTestCase(unittest.TestCase):
    def setUp(self):
        if not cfccldap.config.loadConfiguration(CONFIG_FILENAME):
            raise Exception("Unable to load config file: %s", CONFIG_FILENAME)
        self.my_config = cfccldap.config.get(TEST_ENV)

        if not cfccldap.isOpen():
            print("Connecting to the %s environment..." % TEST_ENV)
            cfccldap.connectAll(self.my_config['db_url'], self.my_config['db_password'])

        self.ldap_session = cfccldap.util.LdapSession(self.my_config['ldap_url'],
                                                 self.my_config['ldap_user'],
                                                 self.my_config['ldap_password'])
        self.ldap_session.set_default_domain(self.my_config['domain'],
                                             self.my_config['user_prefix'],
                                             self.my_config['group_prefix'])
        cfccldap.group.setDbSession(cfccldap.dbutil.getDbSession())
        cfccldap.initLogging(LOG_FILENAME,
                             full_logging=True,
                             debug=True)
        self.logger1 = logging.getLogger('main')

    def test_create_group(self):
        initPlugins()
        rule = PLUGINS['ProgramGroup'](group_name="CONED",
                                       desc="Continuing Education",
                                       current_terms=['2019FA', '2019CE3'])
        rule.setLdapSession(self.ldap_session)
        rule.setDebug(True)
        result = self.ldap_session.add_group(rule)
        self.assertTrue(result)

    def test_aaa_create_group2(self):
        group_dn = TEST_GROUP_DN
        this_group = cfccldap.util.ActiveDirectoryGroup()
        this_group.set_dn(group_dn)
        this_group.add_members(['cn=jfriant80,ou=staff,ou=employees,ou=cfcc users,dc=cfcc,dc=int'])
        rec = [
            ('objectClass', [b'top', b'group']),
            ('cn', [b'test_group']),
            ('samaccountname', [b'test_group']),
            ('member', this_group.get_b_members()),
        ]
        # pp(rec)
        result = self.ldap_session._connection.add_s(this_group.get_dn(), rec)
        self.assertTrue(result)

    def test_aaa_get_group2(self):
        # cfccldap.util.updateGroupMembersAD(TEST_GROUP_DN, ['cn=jfriant80,ou=staff,ou=employees,ou=cfcc users,dc=cfcc,dc=int'], [])
        result = self.ldap_session.get_ldap_group(TEST_GROUP)
        # print("[DEBUG] test_aaa_get_group2():", result.get_dn())
        # pp(result.get_b_members())
        self.assertFalse(result.empty())

    def test_add_member(self):
        group_dn = TEST_GROUP_DN
        add_list = [
            'cn=defriant40,ou=staff,ou=employees,ou=cfcc users,dc=cfcc,dc=int'
            ]
        del_list = [
            'cn=jfriant80,ou=staff,ou=employees,ou=cfcc users,dc=cfcc,dc=int'
            ]
        self.ldap_session.update_group_members(group_dn, add_list, del_list)

    def test_get_group(self):
        """Get all members from an AD group over the 1500 member limit."""
        result = self.ldap_session.get_ldap_group('CURRENT_EMPLOYEES')
        self.assertIsNotNone(result)

    def test_get_unknown_group(self):
        """Attempt to get the membership of a group that doesn't exist."""
        result = self.ldap_session.get_ldap_group('CURRENT_BADGUYS')
        #pp(result)
        self.assertTrue(result.empty())

    def test_remove_group(self):
        """Attempt to remove an existing group"""
        group_dn = "CN=CONED," + cfccldap.config.baseDn(self.my_config, "group_prefix")
        try:
            self.ldap_session.delete(group_dn)
        except Exception as exc:
            self.fail("Failed to delete group {} ({})".format(group_dn, exc))

    def test_zzz_remove_group(self):
        group_dn = TEST_GROUP_DN
        self.ldap_session.delete(group_dn)


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(GroupsActiveDirectoryTestCase)


if __name__ == "__main__":
    unittest.main()


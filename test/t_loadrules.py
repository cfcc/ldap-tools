import unittest
import sys

import intercall

sys.path.append("src")
import cfccldap
import cfccldap.config
import cfccldap.group
from cfccldap.plugins.base import initPlugins

TEST_ENV = 'test2'

TEST_PERSON = '0075580'
TEST_POSITION = 'UNIX Systems Administrator'
TEST_EMAIL = 'jfriant80@mail.nogo.bad'
TEST_CONTEXT = ['staff', 'employees']

TEST_UNIFILE = "GlAccts"
TEST_RECORD_ID = '11_362_80_518100_53422'
TEST_RECORD_DATE = '06/27/06'

CONFIG_FILENAME = "etc/clg.conf"

CURRENT_RULE_COUNT = 244


class CfccRulesTestCase(unittest.TestCase):

    def setUp(self):
        """Connect to test Colleague environment"""
        if not cfccldap.config.loadConfiguration(CONFIG_FILENAME):
            raise Exception("Failed to load configuration file: %s" % CONFIG_FILENAME)
        self.my_config = cfccldap.config.get(TEST_ENV)

        print("[DEBUG] setUp(): Connecting to environment %s..." % TEST_ENV)

        cfccldap.connectAll(self.my_config['db_url'], self.my_config['db_password'])
        cfccldap.group.setDbSession(cfccldap.dbutil.getDbSession())

        cfccldap.initLogging(full_logging=False, debug=True)

    def test_load_day(self):
        initPlugins()
        result = cfccldap.config.loadConfiguration(CONFIG_FILENAME)
        self.assertTrue(result)

        # fetch the configuration again after changes were loaded from
        # the file
        my_config = cfccldap.config.get(TEST_ENV)
        # pp(my_config)

        all_rules = cfccldap.group.loadRules(my_config['rules'], False)
        # print len(all_rules)
        # pp(["%s: %s" % (c.__class__.__name__, c.getGroupName()) for c in all_rules])
        try:
            self.assertTrue(len(all_rules) == CURRENT_RULE_COUNT)
        except AssertionError:
            ex = Exception("Current rule count is now %d" % (len(all_rules)))
            raise ex
        # We should not see after-hours rules
        names = [c.__class__.__name__ for c in all_rules]
        self.assertFalse('RecentStudents' in names)

    def test_load_night(self):
        initPlugins()
        result = cfccldap.config.loadConfiguration(CONFIG_FILENAME)
        self.assertTrue(result)

        my_config = cfccldap.config.get(TEST_ENV)

        all_rules = cfccldap.group.loadRules(my_config['rules'], True)
        names = [c.__class__.__name__ for c in all_rules]
        self.assertTrue('RecentStudents' in names)

    def test_load_from_db(self):
        initPlugins()
        result = cfccldap.config.loadConfiguration(CONFIG_FILENAME)
        self.assertTrue(result)

        my_config = cfccldap.config.get('global')

        all_rules = cfccldap.group.loadRulesFromDatabase(my_config['rules_uri'])
        names = [c.__class__.__name__ for c in all_rules]
        self.assertTrue('CurrentMembers' in names)

    def test_load_single_rules_from_db(self):
        RUN_ALONE_RULE_ID = 16
        DISABLED_RULE_ID = 48

        initPlugins()
        result = cfccldap.config.loadConfiguration(CONFIG_FILENAME)
        self.assertTrue(result)

        my_config = cfccldap.config.get('global')

        all_rules = cfccldap.group.loadRulesFromDatabase(my_config['rules_uri'], sql_rule_id=RUN_ALONE_RULE_ID)
        names = [c.__class__.__name__ for c in all_rules]
        self.assertTrue('AllStudents' in names)

        all_rules = cfccldap.group.loadRulesFromDatabase(my_config['rules_uri'], sql_rule_id=DISABLED_RULE_ID)
        names = [c.__class__.__name__ for c in all_rules]
        self.assertFalse('SchoolRules' in names)

    def test_dynamic_file(self):
        class GlAccts(intercall.UniFile):
            from_database = True

        # lg = cfccldap.group.LdapGroup()
        t = GlAccts(cfccldap.dbutil.getDbSession())
        r = t.get(TEST_RECORD_ID)
        self.assertEqual(r.gl_accts_add_date, TEST_RECORD_DATE)


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(CfccRulesTestCase)


if __name__ == '__main__':
    unittest.main()

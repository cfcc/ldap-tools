import unittest
import sys

sys.path.append("src")

from cfccldap import config

TEST_PLAINTEXT = 'P@ssw0rd1!'
TEST_KEY = b'SuperS3cr3tK3y1!'
TEST_PASSWORD = [b'{AES}', b'LHZi6Ij5K4LitZ8qIRC66w==', b'JCrWsE6wE0i7mxhB4nccvg==', b'qVk7XQoNLhi/KA==']


class ConfigTestCase(unittest.TestCase):

    def test_aa_load_config_file(self):
        config_fn = "etc/clg.conf"

        result = config.loadConfiguration(config_fn)
        self.assertIsNotNone(result)

    def test_encode_password(self):

        result = config.encode_password_v2(TEST_PLAINTEXT, TEST_KEY)

        self.assertEqual(len(result), 4)

    def test_decode_password(self):

        result = config.get_password_v2(TEST_PASSWORD, TEST_KEY)
        self.assertEqual(result, TEST_PLAINTEXT)


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(ConfigTestCase)


if __name__ == '__main__':
    unittest.main()

import sys
import unittest

import cProfile

sys.path.append("src")

import cfccldap.config
import cfccldap.passgen
import cfccldap.randompassword


class PasswordTestCase(unittest.TestCase):

    def setUp(self):
        cfccldap.config.loadConfiguration('etc/clg.conf')
        self.my_config = cfccldap.config.get('guests')

    def test_load_wordlist(self):
        pr = cProfile.Profile()
        pr.enable()
        wordlist = cfccldap.randompassword.loadBadWords(self.my_config['bad_words_file'])
        result = 'sexy' in wordlist
        pr.disable()
        #pr.dump_stats("list.stats")
        #### Uncomment to print the profile stats
        #print "loadBadWords -- uses List"
        #pr.print_stats()
        self.assertTrue(result)

    def test_load_wordlist2(self):
        pr = cProfile.Profile()
        pr.enable()
        wordlist = cfccldap.randompassword.loadBadWords2(self.my_config['bad_words_file'])
        result = 'sexy' in wordlist
        pr.disable()
        #pr.dump_stats("dict.stats")
        #### Uncomment to print the profile stats
        #print "loadBadWords2 -- uses Dict"
        #pr.print_stats()
        self.assertTrue(result)

    def test_gen_passwords(self):
        pw = cfccldap.randompassword.genPassword(3)
        self.assertTrue(len(pw) > 0)
        print("genPassword(3): " + pw)

        pw = cfccldap.randompassword.genPassword(3, 3, 5)
        self.assertTrue(len(pw) > 0)
        print("genPassword(3,3,5): " + pw)

        pw = cfccldap.randompassword.genPassword(5, 2, 4, '.,')
        self.assertTrue(len(pw) > 0)
        print("genPassword(5,2,4,'.,'): " + pw)

        wordlist = cfccldap.randompassword.loadBadWords2(self.my_config['bad_words_file'])

        pw = cfccldap.randompassword.genPassword(3, 3, 5, bad_words=wordlist)
        self.assertTrue(len(pw) > 0)
        print("genPassword(3,3,5,wordlist): " + pw)

        pw = cfccldap.randompassword.genPassword(3, bad_words=wordlist, digitsAtEnd=2)
        self.assertTrue(len(pw) > 0)
        print("genPassword(3,wordlist,digits=2): " + pw)

        pw = cfccldap.randompassword.genPassword(2, 3, 5, spacers=None, bad_words=wordlist, digitsAtEnd=2)
        self.assertTrue(len(pw) > 0)
        print("genPassword(2,3,5,spacers=None,wordlist,digits=2): " + pw)

        pw = cfccldap.randompassword.simplePassword(bad_words=wordlist)
        self.assertTrue(len(pw) > 0)
        print("simplePassword(wordlist): " + pw)

        pw = cfccldap.randompassword.simplePassword(dictionary="etc/specialeng.txt")
        self.assertTrue(len(pw) > 0)
        print("simplePassword(dictionary): " + pw)

        pw = cfccldap.passgen.adPassword(8)
        self.assertTrue(len(pw) > 7)
        print("adPassword(8): " + pw)

        pw = cfccldap.passgen.nonRandomPassword("guest", "account")
        self.assertTrue(len(pw) > 7)
        print("nonRandomPassword(4): " + pw)

        pw = cfccldap.passgen.nonRandomPassword("guest", "")
        self.assertTrue(len(pw) > 7)
        print("nonRandomPassword(4): " + pw)


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(PasswordTestCase)

if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python2.6
import logging
from optparse import OptionParser
from pprint import pprint as pp
import sys
import cProfile

sys.path.append("src")
import cfccldap.config
import cfccldap.group
import cfccldap.util
import cfccldap.dbutil
from cfccldap.group_membership import get_no_changes, update_membership, update_owners


from cfccldap.plugins.base import PLUGINS, initPlugins

DEBUG = False
CONFIG_FILENAME = "etc/clg.conf"
LOG_FILENAME = 'log/test_groups'


def main():
    global DEBUG

    parser = OptionParser(usage="%prog [options]")
    parser.add_option("-D", "--debug", dest="debug",
                      action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment as test or prod (default)")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Write detail log messages to a file.")

    (options, args) = parser.parse_args()

    if not cfccldap.config.loadConfiguration(CONFIG_FILENAME):
        parser.error("Failed to load configuration file: %s" % CONFIG_FILENAME)
    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    if options.debug:
        DEBUG = True

    cfccldap.initLogging(LOG_FILENAME, full_logging=options.log, debug=options.debug)
    logger1 = logging.getLogger('main')

    print("Connecting to the %s environment..." % options.environment)
    cfccldap.connectAll(my_config['db_url'],
                        my_config['db_password'])
    cfccldap.group.setDbSession(cfccldap.dbutil.getDbSession())

    ldap_session = cfccldap.util.LdapSession(my_config['ldap_url'], my_config['ldap_user'], my_config['ldap_password'])
    ldap_session.set_default_domain(my_config['domain'], my_config['user_prefix'], my_config['group_prefix'])

    initPlugins()

    print("Building group rules...")
    # TESTS #
    advisee_groups = [PLUGINS['AdviseeGroup'](key='0208090'),
                      PLUGINS['AdviseeGroup'](key='0087837'),
                      PLUGINS['AdviseeGroup'](key='0216504'),
                      PLUGINS['AdviseeGroup'](key='0269189')]
    dept_factory = PLUGINS['DeptRules']().getRules()
    advisor_factory = PLUGINS['AdvisorRules']().getRules()
    future_students_group = [PLUGINS['FutureStudents'](group_name='FUTURE_STUDENTS', desc="Colleague Group: Applicants")]
    office_group = PLUGINS['OfficeCodeRules']().getRules()
    returning_group = [PLUGINS['ReturningStudents'](group_name="RETURNING_STUDENTS", desc="Colleague Group: Returning Students")]
    recent_group = [PLUGINS['RecentStudents'](group_name="RECENT_STUDENTS", desc="Colleague Group: Recent Students")]
    all_students = [PLUGINS['AllStudents'](group_name="ALL_STUDENTS", desc="Colleague Group: All Students")]
    current_members = PLUGINS['CurrentMembers'](group_name="CURRENT_EMPLOYEES", desc="Colleague Group: Current Employees", key="EMPLOYEE", limit_removal=True)
    # campus_org = [PLUGINS['CampusOrgs'](key='ELRCR'),
    #               PLUGINS['CampusOrgs'](key='EEPMT'),
    #               PLUGINS['CampusOrgs'](key='LCAO'),
    #               PLUGINS['CampusOrgs'](key='LSECL')]
    # campus_org2 = [PLUGINS['CampusOrgs'](key='VPN')]
    campus_org = [PLUGINS['CampusOrgs'](key='EFTXT'),
                  PLUGINS['CampusOrgs'](key='EPTXT')]
    campus_org_factory = PLUGINS['CampusOrgRules']().getRules()
    program_factory = PLUGINS['ProgRules']().getRules()
    one_program_group = [PLUGINS['ProgramGroup'](group_name="A15320", desc="Marine Technology"), PLUGINS['ProgramGroup'](group_name="T90980H", desc="Test")]
    classes = [
        PLUGINS['ClassGroup'](key='MSC', allow_empty=True),
        PLUGINS['ClassGroup'](key='DDF', allow_empty=True),
        PLUGINS['ClassGroup'](key='DFT', allow_empty=True),
        PLUGINS['ClassGroup'](key='LEX', course_number=['210', '211'], allow_empty=True),
    ]
    # interior_design = [
    #     PLUGINS['ClassGroup'](key=['DES', '115']),
    #     PLUGINS['ClassGroup'](key=['DES', '120']),
    #     PLUGINS['ClassGroup'](key=['DES', '136']),
    # ]
    # art_classes = [
    #     PLUGINS['ClassGroup'](key=['ART', '266']),
    #     PLUGINS['ClassGroup'](key=['ART', '267'])
    # ]
    faculty_group = PLUGINS['CurrentFaculty'](group_name="CURRENT_FACULTY", desc="Colleague Group: Current Faculty")
    dev_math_group = [
        PLUGINS['StudentsForCourse'](group_name='dms_students', key='2015SU', desc="Colleague Group: DMS Students", owners=["0267216","0145245"]),
        PLUGINS['StudentsForCourse'](group_name='math_pe_students', key='2015SU', crs_subject=['MAT', 'PED'], desc="Math and PE Students", owners=["0267216","0145245"])
    ]
    division_group = PLUGINS['DivisionRules']().getRules()
    position_group = PLUGINS['PositionRules'](allow_empty=True).getRules()
    # FIXME: the ONLINE_STUDENTS group is generating an exception when invalid terms are selected
    dl_group = [PLUGINS['OnlineStudentsGroup'](group_name="ONLINE_STUDENTS", lead_time=14, allow_empty=True)]
    bldg_group = PLUGINS['BuildingRules']().getRules()
    locn_group = PLUGINS['LocationRules']().getRules()
    current_cu_faculty = PLUGINS['CurrentCuFaculty'](group_name="CURRENT_CU_FACULTY", general_ou=[], priv_ou=[])
    faculty_pt = PLUGINS['EmployeeByPosStatus'](group_name="FACULTY_PT", key="P", owners=['0422634', '0323824', '0075236', '0448715'])
    faculty_ft = PLUGINS['EmployeeByPosStatus'](group_name="FACULTY_FT", key="F", owners=['0422634', '0323824', '0075236', '0448715'])
    staff_pt = PLUGINS['EmployeeByPosStatus'](group_name="STAFF_PT", key="P")
    staff_ft = PLUGINS['EmployeeByPosStatus'](group_name="STAFF_FT", key="F")
    supervisors = PLUGINS['Supervisors'](group_name='SUPERVISORS')
    new_ee_group = PLUGINS['CurrentMembers'](group_name='CapeFearEEs2018', key='EMPLOYEE', exclude='TRUE', owners=['0422634', '0323824', '0075236', '0448715'])
    one_program_group = PLUGINS['ProgramGroup'](group_name='A15320', desc='Marine Technology')
    ce_program_group = PLUGINS['ProgramGroup'](group_name='CONED', desc='Continuing Education')

    # Assign the test group here
    test_list = []
    # test_list.extend(advisee_groups)
    # test_list.extend(dept_factory)
    # test_list.extend(division_group)
    # test_list.extend(dl_group)
    # test_list.extend(acad_programs)
    # test_list.extend(classes)
    # test_list.extend(bldg_group)
    # test_list.append(current_members)
    # test_list.append(faculty_group)
    #
    # test_list.extend(advisor_factory)
    # test_list.extend(future_students_group)
    # test_list.extend(office_group)
    # test_list.extend(returning_group)
    # test_list.extend(recent_group)
    # test_list.extend(all_students)
    # test_list.extend(campus_org)
    # test_list.extend(campus_org_factory)
    # test_list.extend(program_factory)
    #
    # test_list.extend(position_group)
    # test_list.extend(locn_group)
    # test_list.append(current_cu_faculty)
    #
    test_list.extend([faculty_ft, faculty_pt, staff_ft, staff_pt])
    # test_list.extend(dev_math_group)
    # test_list.append(new_ee_group)
    # test_list.append(supervisors)
    # test_list.append(one_program_group)
    # test_list.append(ce_program_group)

    RULE_ID = 55
    print(f"Loading rule {RULE_ID} from the database...")
    global_config = cfccldap.config.get('global')
    test_list.extend(cfccldap.group.loadRulesFromDatabase(global_config['rules_uri'], True, RULE_ID))

    no_changes_members = get_no_changes(ldap_session)
    
    for rule in test_list:
        checkGroup(rule, no_changes_members, ldap_session)

    cfccldap.disconnectAll()


def checkGroup(rule, no_changes_members, ldap_session):
    logger1 = logging.getLogger('main.checkGroup')
    rule.setLdapSession(ldap_session)

    msg = "Starting check of %s" % (rule.getGroupName())
    logger1.info(msg)

    this_group = ldap_session.get_ldap_group(rule.getGroupName())

    global_config = cfccldap.config.get('global')

    if DEBUG:
        rule.setDebug(DEBUG)

    if this_group.exists():
        if DEBUG:
            print("[DEBUG] Updating existing group")
        coll_group = rule.getMembers(ldap_session)
        logger1.debug("members count = %s", len(coll_group))

        msg = "Current member count is %d for %s" % (len(this_group.get_b_members()), rule.getGroupName())
        logger1.info(msg)

        msg = "New member count is %d for %s" % (len(coll_group), rule.getGroupName())
        logger1.info(msg)

        (members_to_add, members_to_remove) = update_membership(this_group,
                                                                rule,
                                                                no_changes_members,
                                                                max_percentage_to_remove=global_config['remove_max_percentage'],
                                                                is_quiet=False,
                                                                is_debug=DEBUG)
        msg = "Members to add: %d.  Members to remove: %d." % (len(members_to_add), len(members_to_remove))
        logger1.info(msg)

        (owners_to_add, owners_to_remove) = update_owners(this_group,
                                                          rule,
                                                          False,
                                                          DEBUG)
        msg = "Owners to add: %d.  Owners to remove: %d." % (len(owners_to_add), len(owners_to_remove))
        logger1.info(msg)

        if DEBUG:
            print("[DEBUG] new member list: ")
            pp(coll_group)
    else:
        result = rule.createGroup()
        if DEBUG:
            print("[DEBUG] createGroup(): ")
            pp(result)
        msg = "New group %s added with %d members" % (rule.getGroupName(),
                                                      rule.member_count)
        logger1.info(msg)

    owner_list = this_group.get_owners()
    if owner_list is not None:
        msg = "Group has %d owners (%s)" % (len(owner_list), repr(owner_list))
        logger1.info(msg)
    msg = "Completed update of %s" % (rule.getGroupName())
    logger1.info(msg)
    

if __name__ == "__main__":
    # cProfile.run('main()', 't_groups.prof')
    main()



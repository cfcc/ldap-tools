#!/usr/bin/env python
import datetime
from pprint import pprint as pp
import pytz
import sqlobject
import unittest
import sys

sys.path.append("src")
from cfccldap import config
from cfccldap.guest_model import GuestAccount, connectDb, DAYS_ACTIVE

CONFIG_FILENAME = "etc/clg.conf"


class GuestAccountDbTestCase(unittest.TestCase):
    def setUp(self):
        if not config.loadConfiguration(CONFIG_FILENAME):
            raise Exception("Failed to load configuration file: %s" % CONFIG_FILENAME)
        self.my_config = config.get("guests")
        connectDb(self.my_config['db_uri'])

    def test_newAcct(self):
        exp_result = "test.me"
        g = GuestAccount(userFirst="test",
                         userLast="me",
                         eventName="Testing",
                         beginDate="2013-10-14",
                         endDate="2013-10-15",
                         submitter="jfriant80@mail.cfcc.edu",
                         isValid=True)
        self.assertTrue(g is not None)
        g.createUsername()
        self.assertEqual(g.username, exp_result)
        #pp(g)

    def test_beginDateNotify(self):
        """Compare the notification date with the begin date.

        Notification date should show that an account creation date
        that falls on a weekend will be moved back to the previous
        Friday.

        """
        NOTIFY_ONLY = datetime.date(2014, 4, 2)
        EXPECTED_NOTIFY = datetime.date(2014, 3, 28)
        END_DATE = NOTIFY_ONLY + datetime.timedelta(days=5)

        g = GuestAccount(userFirst="date",
                         userLast="test",
                         eventName="Date Notify Test",
                         beginDate=NOTIFY_ONLY,
                         endDate=END_DATE,
                         submitter="jfriant80@mail.cfcc.edu",
                         isValid=True)

        self.assertEqual(g.getBeginDate(), EXPECTED_NOTIFY)

    def test_beginDateCreate(self):
        """Test the creation date.

        Again, if the creation date falls in a weekend, it should be
        adjusted back to the previous Friday.

        """
        CREATE_TODAY = datetime.date(2014, 1, 8)
        EXPECTED_CREATE = datetime.date(2014, 1, 3)
        END_DATE = datetime.date.today() + datetime.timedelta(days=30)
        EXPIRE_DATE = datetime.datetime.combine(
                END_DATE + datetime.timedelta(days=2),
                datetime.time(0, 0, tzinfo=pytz.timezone('US/Eastern')))

        g = GuestAccount(userFirst="create",
                         userLast="test",
                         eventName="Date Create Test",
                         beginDate=CREATE_TODAY,
                         endDate=END_DATE,
                         submitter="jfriant80@mail.cfcc.edu",
                         isValid=True)

        self.assertEqual(g.getBeginDate(), EXPECTED_CREATE)

        self.assertEqual(g.getExpirationDate(), EXPIRE_DATE)

    def test_endDateDelete(self):
        """Test the deletion date.

        A guest account should be removed the day after the end date.
        """
        CREATE_TODAY = datetime.date.today()
        ACCT_DAYS_ACTIVE = CREATE_TODAY - datetime.timedelta(days=DAYS_ACTIVE)
        EXPECTED_DELETE = CREATE_TODAY - datetime.timedelta(days=1)
        EXPECTED_EXPIRE = CREATE_TODAY + datetime.timedelta(days=1)

        g = GuestAccount(userFirst="delete",
                         userLast="test",
                         eventName="Date Delete Test",
                         beginDate=CREATE_TODAY,
                         endDate=CREATE_TODAY,
                         submitter="jfriant80@mail.cfcc.edu",
                         isValid=True)

        to_delete = GuestAccount.select(
            sqlobject.OR(
                sqlobject.AND(GuestAccount.q.endDate < EXPECTED_DELETE,
                              GuestAccount.q.username != ""),
                sqlobject.AND(GuestAccount.q.endDate == None,
                              GuestAccount.q.beginDate < ACCT_DAYS_ACTIVE)))

        self.assertTrue(len(list(to_delete)) == 0)
        

def suite():
    return unittest.TestLoader().loadTestsFromTestCase(GuestAccountDbTestCase)
       

if __name__ == '__main__':
    unittest.main()

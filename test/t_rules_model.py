import unittest
import sys
import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from sqlobject import connectionForURI, sqlhub

sys.path.append('src')

import cfccldap.config
from cfccldap.rules_model import Rules

DEBUG = False
LOG_FILENAME = 'test/test_rules_model.log'
CONFIG_FILENAME = 'etc/clg.conf'

TEST_ENV = 'test2'


class RulesModelTestCase(unittest.TestCase):

    def setUp(self):
        if not cfccldap.config.loadConfiguration(CONFIG_FILENAME):
            raise Exception("Unable to load config file: %s", CONFIG_FILENAME)
        self.my_config = cfccldap.config.get('global')

    def test_get_rules(self):
        connection = connectionForURI(self.my_config['rules_uri'])
        sqlhub.processConnection = connection

        result_dict = [r.get_as_dict() for r in Rules.select(Rules.q.disabled == False)]
        result_string = yaml.dump(result_dict, Dumper=Dumper)

        print(result_string)

        self.assertIsNotNone(result_string)


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(RulesModelTestCase)


if __name__ == '__main__':
    unittest.main()

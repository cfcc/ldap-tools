#!/usr/bin/env python
"""Show a cProfile result file"""

from optparse import OptionParser
import pstats

def main():
    parser = OptionParser(usage="%prog [options]")

    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error("You must specify the profile results file.")

    p = pstats.Stats(args[0])

    # show where the most time was spent
    p.strip_dirs().sort_stats('cumulative').print_stats(10)
    # show the functions with the top internal time
    p.strip_dirs().sort_stats('time', 'cum').print_stats(10)
    # show the functions called the most
    p.strip_dirs().sort_stats('calls').print_stats(10)

if __name__ == '__main__':
    main()

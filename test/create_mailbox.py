#!/usr/bin/env python
"""Create a test mailbox entry"""
from email.message import Message
import mailbox
import os
import shutil

TEST_MAILBOX = os.path.expanduser("~/Maildir")
TEST_CSV = ["jfriant80@mail.cfcc.edu|| |Blank Okay |2023-03-15|2013-10-16",
            "jfriant@cfcc.edu||Big|Deal|2013-03-16|2023-03-24",
            "jfriant@cfcc.edu||One|Day|2024-06-26|2024-06-26",
            "jfriant@cfcc.edu||duplicate|guest|2023-07-13|2023-07-14|Duplication Test 1",
            "jfriant@cfcc.edu||duplicate|guest|2023-07-13|2023-07-18|Duplication Test 2"]

# Remove the mailbox if it still exists
shutil.rmtree(TEST_MAILBOX)

# Set up a temporary mailbox
maildir = mailbox.Maildir(TEST_MAILBOX, None, True)
for line in TEST_CSV:
    test_msg = Message()
    test_msg.add_header("Subject", "Guest User")
    test_msg.set_payload(line)
    maildir.add(test_msg)

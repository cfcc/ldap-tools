"""Test database connection file.

Use from the python interpreter by running the statment:

    execfile('test/db.py')

"""
import sys
sys.path.append('src')
sys.path.append('lib/python2.7/site-packages')

from cfccldap import config
from cfccldap.guest_model import GuestAccount,connectDb

if not config.loadConfiguration('etc/clg.conf'):
    print("Failed to load configuration file: ./etc/clg.conf")
    sys.exit(1)

guest_config = config.get('guests')

connectDb(guest_config['db_uri'])

import datetime
from pprint import pprint as pp
import unittest
import sys

sys.path.append("src")
import cfccldap
import cfccldap.config
import cfccldap.group

from cfccldap.plugins.base import initPlugins, PLUGINS

TEST_ENV = 'test2'

TEST_PERSON = '0075580'
TEST_POSITION = 'UNIX Systems Administrator'
TEST_EMAIL = 'jfriant80@mail.nogo.bad'
TEST_CONTEXT = ['staff', 'employees']

class PluginTestCase(unittest.TestCase):
    def test_loadPlugin(self):
        initPlugins()
        print("All plugins:", PLUGINS)

def suite():
    return unittest.TestLoader().loadTestsFromTestCase(PluginTestCase)

if __name__ == '__main__':
    unittest.main()


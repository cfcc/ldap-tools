#!/usr/bin/env python
"""Create a blank database for testing"""
import os
import sys

sys.path.append("src")
from cfccldap import config
from cfccldap.guest_model import GuestAccount, connectDb, DAYS_ACTIVE

CONFIG_FILENAME = "etc/clg.conf"

if not config.loadConfiguration(CONFIG_FILENAME):
    parser.error("Failed to load configuration file: %s" % CONFIG_FILENAME)
my_config = config.get("guests")

# remove any existing database so we can test with a clean one
# NOTE: this only works on sqlite
db_filename = my_config['db_uri'][my_config['db_uri'].index("://") + 3:]
print("[SETUP] removing existing database: %s" % db_filename)
if os.path.exists(db_filename):
    os.unlink(db_filename)

connectDb(my_config['db_uri'])

import datetime
from pprint import pprint as pp
import unittest
import sys

sys.path.append("src")
from cfccldap import config
import cfccldap
from cfccldap.user import LdapUser
from intercall.main import IntercallError

TEST_ENV = 'test2'

TEST_PERSON = '0075580'
TEST_POSITION = 'Executive Director, Information Technology Services'
TEST_EMAIL = 'jfriant80@BADmail.cfcc.edu'
TEST_CONTEXT = ['staff', 'employees']
TEST_DEFAULT_CONTEXT = ['general', 'students']

CONFIG_FILENAME = "etc/clg.conf"


class CfccLdapModuleTestCase(unittest.TestCase):
    def setUp(self):
        """Connect to test Colleague environment"""
        if not config.loadConfiguration(CONFIG_FILENAME):
            raise Exception("Failed to load configuration file: %s", CONFIG_FILENAME)
        self.my_config = config.get(TEST_ENV)

        if not cfccldap.isOpen():
            print("Connecting to environment %s..." % TEST_ENV)
            
            cfccldap.connectAll(self.my_config['db_url'], self.my_config['db_password'])

    def test_email(self):
        person_id = TEST_PERSON
        email_type = None
        require_email = True
        expected_result = TEST_EMAIL
        
        result = cfccldap.getEmail(person_id, email_type, require_email)
        self.assertEqual(result, expected_result)

    def test_no_email(self):
        person_id = TEST_PERSON
        email_type = None
        require_email = False
        expected_result = None
        
        result = cfccldap.getEmail(person_id, email_type, require_email)
        self.assertEqual(result, expected_result)

    def test_position(self):
        person_id = TEST_PERSON
        expected_result = TEST_POSITION

        result = cfccldap.getPositionTitle(person_id)
        self.assertEqual(result, expected_result)

    def test_office(self):
        result = cfccldap.getOffice(TEST_PERSON)
        self.assertIsNotNone(result)

    def test_zip(self):
        result = cfccldap.getZip(TEST_PERSON)
        self.assertNotEqual(result, "")

    def test_context(self):
        expected_result = TEST_CONTEXT

        result = cfccldap.getLdapContext(TEST_PERSON)

        self.assertEqual(result, expected_result)

    def test_default_context(self):
        expected_result = TEST_DEFAULT_CONTEXT

        # using Datatel, which should have (MAILING, STAFF, VENDORS), note that a warning will be generated,
        # but it can be ignored since the record is found in the next table checked
        result = cfccldap.getLdapContext('0000001')
        self.assertTrue(result, expected_result)

    def test_user_object(self):
        oe_rec = cfccldap.org_entity.get(TEST_PERSON)
        user_rec = LdapUser(oe_rec, config.baseDn(self.my_config))
        email_addr = cfccldap.getEmail(TEST_PERSON, required=True)
        if email_addr is not None:
            user_rec.email = email_addr
        user_rec.zip = cfccldap.getZip(TEST_PERSON)
        #pp(user_rec.requiredFields())

    def test_no_username(self):
        def try_name():
            oe_rec = cfccldap.org_entity.get('0233307')
            result = oe_rec.fk_org_entity_env.oee_username
        self.assertRaises(IntercallError, try_name)


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(CfccLdapModuleTestCase)


if __name__ == '__main__':
    unittest.main()

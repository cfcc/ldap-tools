import datetime
from pprint import pprint as pp
import unittest
import sys

sys.path.append("src")
from cfccldap import config
from cfccldap import dbutil

CONFIG_FILENAME = "etc/clg.conf"
TEST_ENVIRONMENT = "test2"


class CurrentTermTestCase(unittest.TestCase):
    def setUp(self):
        """Connect to test Colleague environment"""
        if not config.loadConfiguration(CONFIG_FILENAME):
            raise Exception("Failed to load configuration file: %s", CONFIG_FILENAME)
        self.my_config = config.get(TEST_ENVIRONMENT)
        dbutil.connect(self.my_config['db_url'],
                       self.my_config['db_password'])

    def test_term(self):
        this_day = datetime.date(2011,11,23)
        expected_terms = ['2011FA', '2011CE3']

        result = dbutil.getCurrentTerms(this_day)
        # print("[DEBUG] test_term():", result, expected_terms)
        self.assertEqual(result, expected_terms)

    def test_lead_time(self):
        this_day = datetime.date(2020,8,20)
        lead_time = 14
        expected_terms = ['2020FA', '2020CE3']

        result = dbutil.getCurrentTerms(this_day, lead_time)
        self.assertEqual(result, expected_terms)

    def test_connect(self):
        dbutil.connect(self.my_config['db_url'],
                       self.my_config['db_password'])
        self.assertTrue(dbutil.isOpen())


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(CurrentTermTestCase)


if __name__ == '__main__':
    unittest.main()

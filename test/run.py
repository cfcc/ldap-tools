import unittest

import sys

sys.path.append("src")

import t_cfccldap
import t_config
import t_dbutil
import t_groups_ad
import t_guest_model
import t_loadrules
import t_mailbox
import t_passwdgen
import t_plugins
import t_rules_model
import t_util

all_tests = unittest.TestSuite([
        t_cfccldap.suite(),
        t_config.suite(),
        t_dbutil.suite(),
        t_groups_ad.suite(),
        # t_groups.suite(),
        t_guest_model.suite(),
        t_loadrules.suite(),
        t_mailbox.suite(),
        t_passwdgen.suite(),
        t_plugins.suite(),
        t_rules_model.suite(),
        t_util.suite(),
        ])

# results = unittest.TestResult()
text_runner = unittest.TextTestRunner().run(all_tests)

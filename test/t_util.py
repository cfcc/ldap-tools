from pprint import pprint as pp
import unittest
import sys

sys.path.append("src")
from cfccldap import config
from cfccldap import util
from cfccldap.filetimes import filetime_to_dt
from cfccldap.user import LdapUser

TEST_ENV = 'test'
TEST_USERNAME = 'jfriant80'
TEST_PERSON_ID = '0075580'
CONFIG_FILENAME = "etc/clg.conf"


class ADSearchTestCase(unittest.TestCase):
    
    def setUp(self):
        """Connect to test LDAP server"""
        if not config.loadConfiguration(CONFIG_FILENAME):
            raise Exception("Failed to load configuration file: %s" % CONFIG_FILENAME)
        self.my_config = config.get(TEST_ENV)
        self.ldap_session = util.LdapSession(self.my_config['ldap_url'],
                                             self.my_config['ldap_user'],
                                             self.my_config['ldap_password'])
        self.ldap_session.set_default_domain(self.my_config['domain'],
                                             self.my_config['user_prefix'],
                                             self.my_config['group_prefix'])
        if not self.ldap_session.is_open():
            print("[DEBUG] Connecting to the %s environment..." % TEST_ENV)
            self.ldap_session.connect()

    def test_search_results(self):
        query = '(&(objectClass=person)(employeeNumber=%s))' % TEST_PERSON_ID
        results = self.ldap_session.search(query)
        r = list(results)
        self.assertTrue(len(r) > 0)
        self.assertEqual(r[0].get_attr_values('employeeNumber')[0], TEST_PERSON_ID)

    def test_search_empty(self):
        query = r'(&(objectClass=person)(!(givenName=*)))'
        results = self.ldap_session.search(query)
        self.assertTrue(len(list(results)) > 0)
        
    def test_get_ldap_record(self):
        username = TEST_USERNAME
        return_all = True
        base_dn = config.baseDn(self.my_config)
        key_attr = self.my_config['key_attr']
        
        result = self.ldap_session.get_ldap_record(username, return_all, key_attr)
        # for r in result:
        #     print(r)
        self.assertTrue(len(result) > 0)
        self.assertEqual(result[0].get_attr_values('employeeNumber')[0], TEST_PERSON_ID)

    def test_search_results_paged(self):
        search_dn = "ou=staff,ou=employees,%s" % config.baseDn(self.my_config)
        result = [r for r in self.ldap_session.select_all_paged(alt_search_dn=search_dn)]
        self.assertTrue(len(result) > 0)
        
    def test_create_user(self):
        test_username = 'test_ldap_tools'
        user_rec = LdapUser(base_dn=self.ldap_session.get_search_base())
        user_rec.context = util.DEFAULT_CONTEXT
        user_rec.username = test_username
        user_rec.setUAC('544')
        print("[DEBUG]", user_rec.getDn(), user_rec.asDict())
        self.ldap_session.create_user(user_rec.getDn(), user_rec.asDict())

    def test_rename_user(self):
        new_ou = ['testOU']
        test_username = 'test_ldap_tools'

        return_all = False
        base_dn = config.baseDn(self.my_config)
        key_attr = self.my_config['key_attr']

        user_rec = self.ldap_session.get_ldap_record(test_username, return_all, key_attr)

        self.ldap_session.change_ou(user_rec, new_ou)

    def test_z_change_password(self):
        test_username = 'test_ldap_tools'
        test_password = 'P@ssw0rd1!'

        return_all = False
        base_dn = config.baseDn(self.my_config)
        key_attr = self.my_config['key_attr']

        user_rec = self.ldap_session.get_ldap_record(test_username, return_all, key_attr)

        self.ldap_session.set_ldap_password(user_rec.get_dn(), test_password)

        self.ldap_session.set_password_to_not_expire(user_rec.get_dn())

    def test_z_delete_user(self):
        # the z is to put this test last
        username = 'test_ldap_tools'
        return_all = False
        base_dn = config.baseDn(self.my_config)
        key_attr = self.my_config['key_attr']

        result = self.ldap_session.get_ldap_record(username, return_all, key_attr)
        if result is not None:
            self.ldap_session.delete(result.get_dn())

    def test_update_user(self):
        username = TEST_USERNAME
        return_all = True
        key_attr = self.my_config['key_attr']

        fields = {'company': b'1/1/2000',
                  'postalCode': b'28401'}

        result = self.ldap_session.get_ldap_record(username, return_all, key_attr)

        self.ldap_session.modify_user(result[0].get_dn(), fields, result[0].get_attributes())

    def test_get_user(self):
        username = TEST_USERNAME
        return_all = False
        # base_dn = config.baseDn(self.my_config)
        key_attr = self.my_config['key_attr']
        attributes = ['cn', 'dn']
        result = self.ldap_session.get_ldap_record(username,
                                                   return_all,
                                                   key_attr,
                                                   attributes=attributes)
        cn = result.get_attr_values('cn')[0]
        self.assertTrue(cn, TEST_USERNAME)

        user_ou = result.get_ou()
        self.assertEqual(user_ou, 'staff')

    def test_ado_datetime(self):
        test_date = 128812906535515110
        exp_result = "2009-03-12 00:17:33.551511"
        result = filetime_to_dt(test_date)
        self.assertEqual(str(result), exp_result)


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(ADSearchTestCase)


if __name__ == '__main__':
    unittest.main()

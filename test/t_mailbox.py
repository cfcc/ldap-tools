from email.message import Message
import mailbox
from pprint import pprint as pp
import shutil
import sys
import unittest

sys.path.append("src")

import cfccldap
from cfccldap.new_acct_mailbox import NewAcctMailbox, NewAccount

TEST_MAILBOX = "./Maildir"

TEST_CSV = ["jfriant@cfcc.edu||Mary|Smith|2012-12-01|2012-12-03",
            "jfriant@cfcc.edu||Garden||2013-4-01|2013-4-03",
            "jfriant@cfcc.edu|||Party|2013-4-04|2013-4-06",
            "jfriant@cfcc.edu||||2013-4-07|2013-4-09",
            "jfriant80@mail.cfcc.edu||Spaces |Are Fine |2013-05-01|2013-05-03",
            "jfriant80@mail.cfcc.edu|| |Blank Okay |2013-05-05|2013-05-05",
            "jfriant@cfcc.edu||NHC Sherriff's|MIST 07202013|2013-07-20|2013-07-21|NHC Sheriff's MIST Training - 07-20-2013",
            "jfriant@cfcc.edu,jfriant@nuit.cfcc.edu||Double|Email|2014-08-15|2014-08-15|Test multiple email addresses",
            "jfriant@cfcc.edu|jfriant@nuit.cfcc.edu|Has_CC|Email|2014-08-18|2014-08-19|Testing multiple email addresses"]

TEST_LONG_NAME = {'submitter': 'jfriant@cfcc.edu',
                  'cc': "",
                  'first': "NHC Sheriff's",
                  'last': "MIST 07202013",
                  'begin_date': "7/20/2013",
                  'end_date': "7/21/2013",
                  'event_name': "NHC Sheriff's MIST Training - 07-20-2013"}

class MailboxTestCase(unittest.TestCase):
    def setUp(self):
        cfccldap.initLogging("create-guestaccount-test.log",
                             full_logging=False,
                             debug=True)
        # Set up a temporary mailbox
        maildir = mailbox.Maildir(TEST_MAILBOX, None, True)
        for line in TEST_CSV:
            test_msg = Message()
            test_msg.add_header("Subject", "Guest User")
            test_msg.set_payload(line)
            maildir.add(test_msg)

    def tearDown(self):
        shutil.rmtree(TEST_MAILBOX)

    def testCsv(self):
        maildir = NewAcctMailbox(TEST_MAILBOX, "CSV")
        for msg in maildir.getMessages():
            result = msg.isValid()
            if result:
                for item in msg.requests:
                    print("[DEBUG] valid: ", msg)
            else:
                print("[DEBUG] invalid request: ", msg)

    def testUsername(self):
        EXP_RESULT = "nhcsheriffs"
        new_acct = NewAccount(TEST_LONG_NAME)
        result = new_acct.username()
        self.assertEqual(result, EXP_RESULT)

def suite():
    return unittest.TestLoader().loadTestsFromTestCase(MailboxTestCase)

if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python

INSTALL_SCRIPTS = [
    'bin/colleague_ldap_groups.py',
    'bin/configure.py',
    # 'bin/create-guestaccount.py',
    'bin/delete_user.py',
    # 'bin/import_user.py',
    'bin/ldap_update_email.py',
    'bin/ldap_update_staff.py',
    'bin/ldap_verify_users.py',
    'bin/new_user.py',
    'bin/verify_current_cu.py',
    'bin/verify_employees.py',
    'bin/get-randompassword.py',
    'bin/n99sa_export.py',
    'bin/reg_count_export.py'
]

if __name__ == "__main__":
    from distutils.core import setup

    setup(name='cfccldap',
          version='1.4.4',
          description='Python LDAP Utilities for CFCC',
          author='Jakim Friant',
          author_email='jfriant@cfcc.edu',
          url='http://cfcc.edu/share/',
          package_dir={'': 'src'},
          packages=['cfccldap',
                    'cfccldap.plugins'],
          scripts=INSTALL_SCRIPTS,
          requires=['paramiko', 'PyYAML', 'sqlobject', 'ldif', 'ldap'])

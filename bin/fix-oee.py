#!/usr/bin/env python

"""Fix corrupted ORG.ENTITY.ENV records in Colleague.

"""
__author__ = 'jfriant80'

import logging
from optparse import OptionParser
import os.path
from pprint import pprint as pp
import sys
import yaml

sys.path.append("/usr/local/lib/python2.6/site-packages/")
#PRODUCTION PATH#
sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
#TESTING PATH#
#sys.path.append("src")

import cfccldap
from cfccldap import config
import intercall

CONFIG_FILENAME = "etc/clg.conf"

DEBUG_LIMIT = 10

LOG_PATH = "log"
LOG_FILENAME = "fix_oee.log"

def main():
    """Parse the command line options and run the tasks."""
    parser = OptionParser()
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file (default=%s)" % CONFIG_FILENAME)
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment (default='prod')")
    parser.add_option("-f", "--force", dest="force", action="store_true",
                      default=False, help="Force the update")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")

    (options, args) = parser.parse_args()

    if not config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    global_config = config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    if not options.quiet:
        print("Connecting to the source environment: %s..." % options.environment)

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'],
                        my_config['db_password'],
                        my_config['ldap_password'])

    db_session = cfccldap.dbutil.getDbSession()
    db_session.getList("FIX.OEE")

    for record_id in db_session.readNext():
        logger1.info("Reading %s from ORG.ENTITY.ENV", record_id)
        oee_rec = cfccldap.org_entity_env.recordRead(record_id)
        if chr(intercall.I_FM) not in oee_rec:
            new_str = oee_rec.replace('?', chr(intercall.I_FM))
            #pp(oee_rec)
            pp(new_str)
            cfccldap.org_entity_env.recordWrite(record_id, new_str)
            logger1.info("Fixed the record")
        else:
            logger1.info("Skipped record because it has Field Marks")

if __name__ == '__main__':
    main()

#!/usr/bin/env python

"""Update the new user accounts in LDAP, Colleague, and Informer.

Updates the LDAP tree and updates the Informer user record with the
correct security classes.

This script ties together all the other utility scripts that I was
running separately.

"""
import logging
from optparse import OptionParser
import sys
import traceback

sys.path.append("/usr/local/lib/python2.6/site-packages/")
#PRODUCTION PATH#
sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
#TESTING PATH#
#sys.path.append("src")

#from cfccldap import informer
import cfccldap
from cfccldap import unidata_model  # NOTE: groups are imported from this module
from cfccldap import util
from cfccldap import dbutil
from cfccldap import config
from cfccldap import group

def main():
    """Parse the command line options and run the processes."""
    global session
    
    parser = OptionParser(usage="%prog [options] EMPLOYEE_ID")
    parser.add_option("-d", dest="debug", action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-f", "--force", dest="force",
                      action="store_true", default=False,
                      help="Force the update, even on records with an email.")
    parser.add_option("-t", "--email-type", metavar="CODE",
                      dest="email_type", default='GW',
                      help="Use a specific email type (ex: CC, W, OTH), default=GW")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment as 'test' or 'prod' (default)")
    parser.add_option("-K", "--no-keep", dest="keep_old_groups",
                      action="store_false", default=True,
                      help="Do not keep the existing LDAP groups")
    parser.add_option("-O", "--change-ou", dest="new_ou",
                      default=None, metavar="NAME",
                      help="Specify the Organizational Unit (OU) for the user")

    (options, args) = parser.parse_args()

    try:
        my_config = config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    if len(args) < 1:
        finished = False
        while not finished:
            person_id = input("Enter the 7-digit employee ID: ")
            if person_id.strip() == "":
                sys.exit(0)
            if person_id.isdigit():
                finished = True
            else:
                print("ERROR: the person ID must be only numbers")
    else:
        person_id = args[0]

    cfccldap.initLogging("log/new_user", full_logging=options.debug, debug=options.debug)
    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'])
    cfccldap.group.setDbSession(cfccldap.dbutil.getDbSession())

    logger1 = logging.getLogger('main')

    # check for a valid OU
    rec = cfccldap.util.getLdapRecord(person_id, base_dn=config.baseDn(my_config), key_attr=my_config['key_attr'])
    if rec is not None:
        if options.new_ou is not None:
            valid_ou = options.new_ou
        else:
            valid_ou = ['staff','faculty']
        current_ou = rec.get_ou()
        if current_ou not in valid_ou:
            # One more check to see if this is really the wrong OU.
            # This will catch the students who are still students.
            new_group = cfccldap.getLdapContext(person_id)
            if current_ou not in new_group:
                logger1.error("Wrong LDAP group %s for %s" % (rec.get_ou(),
                                                              person_id))
                # give the option to change the user's group
                usr_ans = input("Move %s to %s (y/N)? " % (rec.get_attr_values('uid')[0], new_group))
                if usr_ans.lower() == 'y':
                    cfccldap.util.moveLdapRecord(rec, new_group)
                else:
                    sys.exit(1)
    
    # update the email address in LDAP from Colleague
    rec = cfccldap.org_entity.get(person_id)
    new_mail = cfccldap.getEmail(person_id, options.email_type)
    cfccldap.util.updateEmail(rec, new_mail, options.force, config.baseDn(my_config), my_config['key_attr'])

    # update the group membership based on Colleague STAFF codes and
    # the X810.ID.CARD file
    rec = cfccldap.staff.get(person_id)
    if rec.person_pin.user_id != "":
        ldap_groups = list(unidata_model.role(rec.staff_office_code, True).keys())
        coll_group = cfccldap.group.getCollStatus(person_id)
        logger1.debug("person id: %s, group: %s", person_id, coll_group)
        if coll_group is not None:
            ldap_groups.append(coll_group)
            cfccldap.util.updateGroups(rec.person_pin.user_id,
                                       ldap_groups,
                                       options.keep_old_groups,
                                       config.baseDn(my_config),
                                       my_config['key_attr'])
            cfccldap.util.updateOfficeCodes(rec.person_pin.user_id,
                                            rec.staff_office_code,
                                            my_config['key_attr'])
    else:
        logger1.error("No STAFF->PERSON.PIN username for %s", rec.record_id)
    
    # pause to initialize Informer account
    print("")
    print(("Initialize the informer user account for %s" % (rec.person_pin.user_id)))
    usr_ans = input("Press enter when done (or 'q' to skip)...")
    if usr_ans.lower() != 'q':
        # update Informer
        raise DeprecationWarning("Informer 3.2 udpates have been disabled.")
        #informer.connect(None)
        #informer.processRecord(person_id, True, True)
        #informer.disconnect()

    cfccldap.disconnectAll()

if __name__ == "__main__":
    main()

#!/usr/bin/env python

"""Update the staff codes in the LDAP database"""

import logging
from optparse import OptionParser

import sys

sys.path.append("/usr/local/lib/python2.6/site-packages/")
#PRODUCTION PATH#
sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
#TESTING PATH#
#sys.path.append("src")

import cfccldap
import cfccldap.config
import cfccldap.group
import cfccldap.unidata_model
import cfccldap.dbutil

DEBUG = False
VERBOSITY = 0

def main():
    """Parse the options and run the requested update."""
    global DEBUG
    
    parser = OptionParser(usage="%prog [options]")
    parser.add_option("--all", dest="all_staff",
                      action="store_true", default=False,
                      help="Select all records with OEE.OPERS = WEBFACSTAFF")
    parser.add_option("-d", "--debug", dest="debug",
                      action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment as 'test' or 'prod' (default)")
    parser.add_option("-e", dest="employee_id",
                      metavar="PERSON.ID", default=None,
                      help="Update the given record only")
    parser.add_option("-k", "--keep", dest="keep_old_groups",
                      action="store_true", default=False,
                      help="Keep the existing LDAP groups and only add new ones")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-s", "--savedlist",
                      dest="savedlist_name", metavar="NAME",
                      help="Set a savedlist to use")
    parser.add_option("-u", dest="user_id", default=None, metavar="UID",
                      help="Select a user by the LDAP UID")

    (options, args) = parser.parse_args()

    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    log_level = logging.INFO
    if options.debug:
        DEBUG = True
        log_level = logging.DEBUG

    cfccldap.initLogging('log/ldap_update_staff', log_level, options.log, debug=options.debug)

    cfccldap.connectAll(my_config['db_url'],
                           my_config['ldap_url'],
                           my_config['ldap_user'])

    logger1 = logging.getLogger("main")
        
    if options.all_staff:
        options.savedlist_name = cfccldap.dbutil.selectAllStaff()
        
    if options.savedlist_name is not None:
        logger1.info("Updating records from savedlist: %s", options.savedlist_name)
        session = cfccldap.dbutil.getDbSession()
        session.getList(options.savedlist_name, 3)
        for rec_id in session.readNext(3):
            updateRecord(None, rec_id, options.keep_old_groups)
    elif options.employee_id is not None:
        logger1.info("Updating single record: %s", options.employee_id)
        updateRecord(None, options.employee_id, options.keep_old_groups)
    elif options.user_id is not None:
        logger1.info("Retriving employeeNumber for %s", options.employee_id)
        empl_id = cfccldap.util.getEmployeeNumber(options.user_id)
        updateRecord(None, empl_id, options.keep_old_groups)
    else:
        logger1.info("Updating IT records only")
        records = cfccldap.staff.select(cfccldap.staff.staff_office_code.eq("IT"))
        for rec in records:
            updateRecord(rec, None, options.keep_old_groups)

    cfccldap.disconnectAll()

def updateRecord(staff_rec = None, person_id = None, keep_old_groups = False):
    logger1 = logging.getLogger("main")

    user_id = None
    ldap_groups = []

    if staff_rec is None and person_id is not None:
        staff_rec = cfccldap.staff.get(person_id)

    if staff_rec is not None:
        if staff_rec.person_pin.user_id != "":
            user_id = staff_rec.person_pin.user_id
            ldap_groups = list(cfccldap.unidata_model.role(staff_rec.staff_office_code, True).keys())
            if not DEBUG:
                cfccldap.group.updateOfficeCodes(user_id, staff_rec.staff_office_code)
            else:
                logger1.debug("would update %s with codes: %s", staff_rec.person_pin.user_id, staff_rec.staff_office_code)
        else:
            logger1.warning("No STAFF->PERSON.PIN username for %s", staff_rec.record_id)
    else:
        logger1.error("No staff record selected.  Person ID = %s", person_id)

    # now check the PERSON.PIN file directly for a username if we
    # don't have one from the staff record
    if user_id is None and person_id is not None:
        pp_rec = cfccldap.person_pin.get(person_id)
        if pp_rec.user_id != "":
            user_id = pp_rec.user_id
        else:
            logger1.debug("No PERSON.PIN username for %s", students.record_id)

    # get the active colleague group
    coll_group = cfccldap.group.getCollStatus(person_id)
    if coll_group is not None:
        ldap_groups.append(coll_group)

    # finally run the update
    if not DEBUG:
        cfccldap.util.updateGroups(user_id, ldap_groups, keep_old_groups)
    else:
        logger1.debug("Would update %s with groups: %s", pp_rec.user_id, ldap_groups)
                
if __name__ == "__main__":
    main()

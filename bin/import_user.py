#!/usr/bin/env python

"""Create users in LDAP based on the information in Colleague

Written to import users in to Active Directory during the migration.

"""

# Research from:
# http://snipt.net/Fotinakis/change-active-directory-password-via-ldap-modify-call/
# http://pig.made-it.com/pig-adusers.html
# https://groups.google.com/group/comp.lang.python/browse_thread/thread/8228d27d558a7158

import logging
from optparse import OptionParser
import sys
from pprint import pprint as pp

sys.path.append("/usr/local/lib/python2.6/site-packages/")
#PRODUCTION PATH#
#sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
#TESTING PATH#
sys.path.append("src")

import ldap
import ldap.modlist
import intercall
import cfccldap
import cfccldap.config
from cfccldap.progress_bar import ProgressBar
from cfccldap.user import LdapUser
from cfccldap.passgen import adPassword

CONFIG_FILENAME = "etc/clg.conf"
MAX_PASSWD_ATTEMPTS = 2


def main():
    """Parse the command line options"""

    parser = OptionParser()
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment as 'test' or 'prod' (default)")
    parser.add_option("-e", dest="person_id", metavar="ID",
                      help="Specify a PERSON record ID")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-p", dest="pbar_limit", metavar="NUM",
                      help="Display a progress bar for NUM records")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")

    parser.add_option("-s", dest="savedlist_name", metavar="NAME",
                      help="Set a savedlist to use")
    parser.add_option("-t", "--email-type", metavar="CODE",
                      dest="email_type", default=None,
                      help="Use a specific email type (ex: EE, SE)")
    parser.add_option("-u", "--update",
                      action="store_true", default=False,
                      help="Update an existing user, do not try to create")

    parser.add_option("-w", "--write", metavar="FILENAME",
                      dest="result_filename", default=None,
                      help="Write the results to an output file.")

    (options, args) = parser.parse_args()

    if options.savedlist_name is None and options.person_id is None:
        print("Selecting all active records in ORG.ENTITY.")
        usr_ans = input("Do you want to continue (y/N)?")
        if usr_ans.lower() != "y":
            sys.exit(0)

    if not cfccldap.config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        my_config = dict()
        parser.error(str(ex))

    cfccldap.initLogging("log/import_user", full_logging=options.log, debug=options.debug)

    cfccldap.connectAll(my_config['db_url'], my_config['db_password'])

    ldap_session = cfccldap.util.LdapSession(my_config['ldap_url'], my_config['ldap_user'], my_config['ldap_password'])
    ldap_session.set_default_domain(my_config['domain'], my_config['user_prefix'], my_config['group_prefix'])

    logger1 = logging.getLogger('main')

    if options.savedlist_name is not None:
        msg = "Importing records from savedlist: %s" % options.savedlist_name
        logger1.info(msg)
        if not options.quiet:
            print(msg)
        session = cfccldap.dbutil.getDbSession()
        session.getList(options.savedlist_name, 3)
        # initialize the optional progress bar
        if options.pbar_limit is not None:
            limit = int(options.pbar_limit)
        else:
            limit = 100
        prog = ProgressBar(0, limit, 74, mode='fixed')
        oldprog = str(prog)
        pbar_cnt = 0
        # If we are saving a results file, then open it now
        if options.result_filename is not None:
            result_fd = open(options.result_filename, 'w')
        else:
            result_fd = None
        # now import the records in the savedlist
        for rec_id in session.readNext(3):
            rec = cfccldap.org_entity.get(rec_id)
            result = do_import(rec, options.email_type, options.debug, options.quiet, options.update, ldap_session)
            if result['password'] is not None and result_fd is not None:
                result_fd.write("%s|%s\n" % (result['username'], result['password']))
            if prog is not None:
                pbar_cnt += 1
                prog.update_amount(pbar_cnt)
                if oldprog != str(prog):
                    print(prog, "\r", end=' ')
                    sys.stdout.flush()
                    oldprog=str(prog)
        if result_fd is not None:
            result_fd.close()
    elif options.person_id is not None:
        msg = "Importing single record: %s" % options.person_id
        logger1.info(msg)
        if not options.quiet:
            print(msg)
        rec = cfccldap.org_entity.get(options.person_id)
        do_import(rec, options.email_type, options.debug, options.quiet, options.update, ldap_session)
    else:
        msg = "Importing all active records in ORG.ENTITY."
        logger1.info(msg)
        if not options.quiet:
            print(msg)
        records = cfccldap.org_entity.select(cfccldap.org_entity.oe_org_entity_env.ne(""))
        for rec in records:
            do_import(rec, options.email_type, options.debug, options.quiet, options.update, ldap_session)

    msg = "Import complete, closing connections..."
    logger1.info(msg)
    if not options.quiet:
        print(msg)
    cfccldap.disconnectAll()


def do_import(rec, email_type, debug, quiet, update_only, ldap_session):
    """Use the ORG.ENTITY record to build a LDAP record for import."""

    result = {'username': None,  'password': None}

    logger1 = logging.getLogger('main.import')
    try:
        user_rec = LdapUser(rec, ldap_session.get_search_base(), server_domain_name=ldap_session.get_server_domain_name())
    except intercall.main.IntercallError as err:
        msg = "Unable to load Colleague data for %s: %s" % (rec.record_id, err)
        logger1.warning(msg)
        return result

    email_addr = cfccldap.getEmail(rec.record_id, email_type, True)
    if email_addr is not None:
        user_rec.email = email_addr
    user_rec.context = cfccldap.getLdapContext(rec.record_id)
    user_rec.title = cfccldap.getPositionTitle(rec.record_id)
    user_rec.depts_desc = cfccldap.getDeptDesc(rec.record_id)
    user_rec.importOffice(cfccldap.getOffice(rec.record_id))
    try:
        user_rec.zip = cfccldap.getZip(rec.record_id)
    except intercall.IntercallError as ex:
        logger1.warning("No zipcode found for %s: %s", rec.record_id, ex)

    if debug:
        if not quiet:
            print(user_rec.getDn())
            pp(ldap.modlist.addModlist(user_rec.asDict()))
    else:
        try:
            if not update_only:
                try:
                    ldap_session.create_user(user_rec.getDn(), user_rec.asDict())
                    logger1.info("Created user object for %s", user_rec.getDn())
                    # then modify user w/ password
                    logger1.info("Resetting password...")
                    attempt = 0
                    while attempt < MAX_PASSWD_ATTEMPTS:
                        try:
                            attempt += 1
                            result['password'] = adPassword()
                            ldap_session.set_ldap_password(user_rec.getDn(), result['password'])
                            break
                        except ldap.UNWILLING_TO_PERFORM:
                            logger1.warning("Password change attempt %d failed", attempt)
                    if attempt >= MAX_PASSWD_ATTEMPTS:
                        result['password'] = None
                        logger1.warning("Failed to set password after %d attempts", attempt)
                    # finally enable the account
                    logger1.info("Enabling account...")
                    try:
                        ldap_session.enable_account(user_rec.getDn())
                    except ldap.UNWILLING_TO_PERFORM:
                        logger1.warning("Account not enabled for %s", user_rec.person_id)
                except (ldap.ALREADY_EXISTS, ldap.CONSTRAINT_VIOLATION) as ex:
                    logger1.info("User %s already in directory, skipping...", user_rec.person_id)
                    if not quiet:
                        logger1.error(ex, exc_info=True)
            else:
                curr_rec = ldap_session.get_ldap_record(user_rec.username)
                logger1.info("Updating existing user %s: %s",
                             user_rec.person_id,
                             curr_rec.get_dn())
                ldap_session.modify_user(curr_rec.get_dn(),
                                         user_rec.asDict(),
                                         curr_rec.attrs)
            result['username'] = user_rec.username

        except (ldap.NO_SUCH_OBJECT, ldap.OTHER) as ex:
            logger1.error("LDAP account failed for %s", user_rec.person_id)
            if not quiet:
                logger1.error(ex, exc_info=True)
        return result


if __name__ == "__main__":
    main()

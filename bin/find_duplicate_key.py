#!/usr/bin/env python
"""Find and remove duplicate entries in GADS Non-Address table.

"""


import base64
import csv
import getpass
from optparse import OptionParser
import os.path
import paramiko
import shutil
import sys
import tempfile
import traceback

sys.path.append("/usr/local/lib/python2.6/site-packages/")
# PRODUCTION PATH #
# sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
# TESTING PATH #
sys.path.append("src")

import cfccldap
import cfccldap.config
import cfccldap.util

KEY_FN = '/home/jfriant/opt/GoogleAppsDirSync/nonAddressPrimaryKeyFile.tsv'
OUT_FN = 'nonAddressPrimaryKeyFile_new.tsv'

CONFIG_FILENAME = "etc/clg.conf"
RULES = []
LOG_PATH = "log"
LOG_FILENAME = "fix_duplicate_key.log"


def main():
    parser = OptionParser(usage="%prog [options] USER_NAME")

    parser.add_option("-a", "--all", dest="search_all",
                      action="store_true", default=False,
                      help="Find duplicates in the whole file.")
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-d", "--debug", dest="debug",
                      action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="env",
                      default="prod", metavar="NAME",
                      help="Specify the environment as test or prod (default)")
    parser.add_option("-f", "--filename", dest="key_filename",
                      default=None, metavar="PATH",
                      help="Load primary keys from a local file")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Write detail log messages to a file.")
    parser.add_option("-o", "--outfile", dest="out_filename",
                      default=None, metavar="PATH",
                      help="Write results to the given file.")
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true", default=False,
                      help="Display more status messages when running.")

    (options, args) = parser.parse_args()

    if not cfccldap.config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: {}".format(options.config_file))

    try:
        my_config = cfccldap.config.get(options.env)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    log_fn = os.path.join(LOG_PATH, LOG_FILENAME)
    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    
    cfccldap.util.connect(my_config['ldap_url'],
                          my_config['ldap_user'],
                          my_config['ldap_password'])

    cfccldap.util.setDefaultDomain(my_config['domain'],
                                   my_config['user_prefix'],
                                   my_config['group_prefix'])

    # Verify that we have a username to search for
    if len(args) > 0:
        username = args[0]

        my_guid = get_guid(username)
        if options.verbose:
            print("Current GUID for %s is %s" % (username, my_guid))

        if options.key_filename is not None:
            csv_fd = open(options.key_filename, 'rb')
            keyfile_contents = csv_fd.readlines()
            csv_fd.close()
        else:
            keyfile_contents = download("informer", KEY_FN)

        reader = csv.reader(keyfile_contents, delimiter="\t")
        print("-" * 75)
        for row in reader:
            if username in row[0]:
                if row[1] in my_guid:
                    valid = "VALID"
                else:
                    valid = "INVALID"
                print(", ".join(row) + ", " + valid)
        print("-" * 75)
    elif options.search_all:
        # build a dictionary based on the username, if we find a
        # duplicate entry then check LDAP for the correct GUID
        user_hash = dict()
        file_header = ""

        if options.key_filename is not None:
            csv_fd = open(options.key_filename, 'rb')
            keyfile_contents = csv_fd.readlines()
            csv_fd.close()
        else:
            keyfile_contents = download("informer", KEY_FN)

        reader = csv.reader(keyfile_contents, delimiter="\t")

        line_count = 0
        for row in reader:
            if not row[0].startswith("#"):
                line_count += 1
                username = row[0].split('@')[0]  # strip off any domain information
                if username not in user_hash:
                    user_hash[username] = row
                else:
                    print("[INFO] Found duplicate: {} and {}".format(user_hash[username][0], row[0]))
                    correct_guid = get_guid(username)
                    fixed_guid = False
                    for this_guid in (user_hash[username][1], row[1]):
                        if this_guid not in correct_guid:
                            # this is only to print the value that was wrong
                            print("[INFO] Corrected GUID from {} to {}".format(this_guid, correct_guid))
                        else:
                            # when the GUID matches what is in Active Directory, then we save it.
                            user_hash[username][1] = this_guid
                            fixed_guid = True
                    if not fixed_guid:
                        print("[INFO] Found duplicate for {} but neither GUID matched {}".format(username, correct_guid))
            else:
                file_header += row[0] + "\n"
        print("[INFO] Processed {} rows in the file".format(line_count))
        if options.out_filename is not None:
            if os.access(options.out_filename, os.F_OK):
                print("[INFO] Making backup copy of {}".format(options.out_filename))
                shutil.copy(options.out_filename, options.out_filename + "~")
            with open(options.out_filename, 'wb') as out_fd:
                out_fd.write(file_header)
                for key in sorted(user_hash.keys()):
                    out_fd.write("{0}\t{1}\n".format(user_hash[key][0], user_hash[key][1]))
            print("[INFO] wrote output to {}".format(options.out_filename))
    else:
        parser.error("You must specify a username")


def get_guid(username):
    """Contact the LDAP server and return a string with the GUID attribute

    For some reason GADS tends to switch around the altchars.  For a while it appeared to be using
    the characters '+_', but now at least the first one has switched, so '-_' are being used for now.

    :param username: a string with the AD username

    """
    my_guid = None
    result = cfccldap.util.getLdapRecord(username, attributes=['objectGUID'])
    if result is not None:
        my_guid = base64.b64encode(result.get_attr_values('objectGUID')[0], '-_')
    return my_guid


def download(hostname, remote_fn):
    """Download the key file from a remote server and return a string with the contents

    :param hostname: a string with the DNS name or IP address of the server
    :param remote_fn: a string with the filename to download from the remote server

    """
    contents = list()
    
    local_fn = tempfile.TemporaryFile()
    
    # get the local config
    ssh_config = paramiko.SSHConfig()
    user_config_fn = os.path.expanduser("~/.ssh/config")
    if os.path.exists(user_config_fn):
        with open(user_config_fn) as fd:
            ssh_config.parse(fd)

    cfg = {'hostname': hostname,
           'user': getpass.getuser(),
           'port': 22,
           'pkey': None,
           'hostkey': None}

    user_config = ssh_config.lookup(cfg['hostname'])
    for k in ('hostname', 'user', 'port'):
        if k in user_config:
            cfg[k] = user_config[k]
            print("Found config entry {} = {}".format(k, cfg[k]))

    # see if we have a private key to use
    private_key_file = os.path.expanduser('~/.ssh/id_rsa')
    if os.access(private_key_file, os.R_OK):
        try:
            cfg['pkey'] = paramiko.RSAKey.from_private_key_file(private_key_file)
        except paramiko.PasswordRequiredException:
            print("[INFO] Private key requires password")
            pkpass = getpass.getpass("password: ")
            cfg['pkey'] = paramiko.RSAKey.from_private_key_file(
                private_key_file,
                pkpass)

    # get host key, if we know one
    try:
        host_keys = paramiko.util.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
    except IOError:
        print('[ERROR] Unable to open host keys file')
        host_keys = {}

    if host_keys.lookup(hostname):
        hostkeytype = list(host_keys[hostname].keys())[0]
        cfg['hostkey'] = host_keys[hostname][hostkeytype]
        print('[INFO] Using host key of type {}'.format(hostkeytype))

    # now, connect and use paramiko Transport to negotiate SSH2 across
    # the connection
    try:
        t = paramiko.Transport((cfg['hostname'], cfg['port']))
        t.connect(username=cfg['user'],
                  pkey=cfg['pkey'],
                  hostkey=cfg['hostkey'])

        sftp = paramiko.SFTPClient.from_transport(t)
        sftp.get(remote_fn, local_fn.name)
        sftp.close()
        t.close()

        with open(local_fn.name, 'rb') as fd:
            for line in fd:
                contents.append(line)

        os.remove(local_fn.name)

    except Exception as e:
        print('[WARNING] Caught exception: {}: {}'.format(e.__class__, e))
        traceback.print_exc()
    return contents

if __name__ == "__main__":
    main()

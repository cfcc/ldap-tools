#!/usr/bin/env python

"""Create and update groups in LDAP based on selection criteria in Colleague.

This started out as a workaround script to get the Colleague LDAP groups working
while we don't have the portal, since there is a bug that ties the LDAP processes
to the Ellucian Portal configuration.

Now that CFCC is using Active Directory, we've continued to expand and develop this
process.  It runs on the Wilm-ERP-CLG server under Maestro, using multiple schedules
to update groups throughout the day and in the evening.

"""
import logging
from optparse import OptionParser
import os
import sys
import site

from pprint import pprint as pp

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')

#PRODUCTION PATH#
# site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
#TESTING PATH#
site.addsitedir("src")

import cfccldap
import cfccldap.dbutil
import cfccldap.group
import cfccldap.config
from cfccldap.plugins.base import initPlugins
from cfccldap.group_membership import update_membership, update_owners, get_no_changes

CONFIG_FILENAME = "etc/clg.conf"
RULES = []
LOG_PATH = "log"
LOG_FILENAME = "coll_ldap_groups.log"


def main():
    """Parse the command line options and run the processes."""
    global RULES

    parser = OptionParser(usage="%prog [options]")
    parser.add_option("-a", "--after-hours", dest="after_hours",
                      action="store_true", default=False,
                      help="Run rules marked as after-hours, usually the dynamically built groups.")
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-d", "--debug", dest="debug",
                      action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("--dynamic", dest="after_hours",
                      action="store_true", default=False,
                      help="DEPRECATED, use --after-hours")
    parser.add_option("-E", "--environment", dest="env",
                      default="prod", metavar="NAME",
                      help="Specify the environment as test or prod (default)")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Write detail log messages to a file.")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")
    parser.add_option("-r", "--rules", dest="rules_file",
                      default=None, metavar="PATH",
                      help="Specify the path to an YAML rules file.")
    parser.add_option("--sql-rule-id", dest="sql_rule_id",
                      default=None, metavar="ID",
                      help="Run a single SQL rule matching the ID")
    parser.add_option("--show-config", dest="show_config",
                      action="store_true", default=False,
                      help="show configuration and exit")

    (options, args) = parser.parse_args()

    if not cfccldap.config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = cfccldap.config.get(options.env)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    global_config = cfccldap.config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    if options.debug:
        pp(global_config)
        pp(my_config)
        input("Press enter to continue...")

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    initPlugins()

    cfccldap.connectAll(my_config['db_url'], my_config['db_password'])
    cfccldap.group.setDbSession(cfccldap.dbutil.getDbSession())

    ldap_session = cfccldap.util.LdapSession(my_config['ldap_url'], my_config['ldap_user'], my_config['ldap_password'])
    ldap_session.set_default_domain(my_config['domain'], my_config['user_prefix'], my_config['group_prefix'])

    # load the rules here, note that the default rules file in the config is deprecated in favor of the SQL database
    # but you can still specify a YAML rules file from the command line
    if options.rules_file is not None:
        all_rules = cfccldap.group.loadRules(options.rules_file, options.after_hours)
    else:
        all_rules = cfccldap.group.loadRulesFromDatabase(global_config['rules_uri'],
                                                         options.after_hours,
                                                         options.sql_rule_id)
    msg = "Loaded %d group rules" % len(all_rules)
    logger1.info(msg)
    if not options.quiet:
        print(msg)

    no_changes_members = get_no_changes(ldap_session)

    # DEBUG #######
    if options.show_config:
        usr_ans = input("Press enter to display rules and exit...")
        # show which rules were loaded
        print("\n".join(sorted([c.group_info() for c in all_rules])))
        sys.exit(0)
    # DEBUG #######

    for rule in all_rules:
        rule.setLdapSession(ldap_session)
        try:
            msg = "Starting check of " + rule.getGroupName() + " (" + cfccldap.fullname(rule) + ")"
            logger1.info(msg)
            if not options.quiet:
                print(msg)
            this_group = ldap_session.get_ldap_group(rule.getGroupName())

            if options.debug:
                rule.setDebug(options.debug)
        except TypeError:
            logger1.error("Failed to process rule %r", rule)
            this_group = None

        if this_group.exists():
            try:
                # Determine if any user accounts need to be added or removed from the group
                (members_to_add, members_to_remove) = update_membership(this_group,
                                                                        rule,
                                                                        no_changes_members,
                                                                        global_config['remove_max_percentage'],
                                                                        options.quiet,
                                                                        options.debug,
                                                                        ldap_connection=ldap_session)
                # Determine if any owners need to be added or removed
                (owners_to_add, owners_to_remove) = update_owners(this_group,
                                                                  rule,
                                                                  options.quiet,
                                                                  options.debug)
                # Check for and remove empty groups
                if len(this_group.get_members()) == 0 and len(members_to_add) == 0:
                    if not rule.allow_empty:
                        logger1.info("Removing empty group %s", rule.getGroupName())
                        if this_group.get_dn() is not None:
                            ldap_session.delete(this_group.get_dn())
                        else:
                            logger1.info("Empty group not found in AD: %s", rule.getGroupName())
                    else:
                        logger1.info("Keeping empty group %s", rule.getGroupName())
                else:
                    # write the new group membership to the LDAP record
                    ldap_session.update_group_members(this_group.get_dn(),
                                                      members_to_add,
                                                      members_to_remove,
                                                      owners_to_add,
                                                      owners_to_remove)
            except Exception as e:
                logger1.error("Failed to update group %s", rule.getGroupName(), exc_info=True)
        else:
            # Create a new group based on the rule object
            logger1.info("Group not found in LDAP directory: %s", rule.getGroupName())
            try:
                result = ldap_session.add_group(rule)
                if result:
                    msg = "New group %s added with %d members" % (rule.getGroupName(), rule.member_count)
                    if not options.quiet:
                        print(msg)
                    logger1.info(msg)
                else:
                    if not options.quiet:
                        print("Empty group %s not added." % rule.getGroupName())
            except Exception as ex:
                msg = "Failed to create new group %s" % rule.getGroupName()
                logger1.warning(msg, exc_info=True)
                # if not options.quiet:
                #     print "[DEBUG]"
                #     pp(rule.createGroup())
                #     print msg
        msg = "Completed update of " + rule.getGroupName()
        logger1.info(msg)
        if not options.quiet:
            print(msg)


if __name__ == "__main__":
    main()

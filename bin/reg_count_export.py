#!/usr/bin/env python

"""Export the registration head and seat count numbers for import into a data warehouse.

"""
import datetime
import logging
import site
from optparse import OptionParser
import os.path
from pprint import pprint as pp
import sys

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')
#PRODUCTION PATH#
site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
#TESTING PATH#
# site.addsitedir("src")

import cfccldap
from cfccldap import config
import intercall
from intercall import AND

CONFIG_FILENAME = "etc/clg.conf"

LOG_PATH = "log"
LOG_FILENAME = "reg_count_export.log"


class CourseSections(intercall.UniFile):
    ic_filename = "COURSE.SECTIONS"
    sec_capacity = intercall.NumCol(loc=5, dbname='SEC.CAPACITY', conv='MD0')
    sec_term = intercall.StringCol(loc=15, dbname='SEC.TERM')
    sec_status_date = intercall.DateCol(loc=46, dbname='SEC.STATUS.DATE', mv=True)
    sec_status = intercall.StringCol(loc=88, dbname='SEC.STATUS', mv=True)
    sec_current_status = intercall.ComputedCol('SEC.CURRENT.STATUS')


class StudentAcadCred(intercall.UniFile):
    ic_filename = "STUDENT.ACAD.CRED"
    stc_person_id = intercall.StringCol(loc=1, dbname='STC.PERSON.ID')
    stc_title = intercall.StringCol(loc=2, dbname='STC.TITLE')
    stc_acad_level = intercall.StringCol(loc=3, dbname='STC.ACAD.LEVEL')
    stc_course_level = intercall.StringCol(loc=4, dbname='STC.COURSE_LEVEL')
    stc_status = intercall.StringCol(loc=9, dbname='STC.STATUS', mv=True)
    stc_term = intercall.StringCol(loc=25, dbname='STC.TERM')
    stc_current_status = intercall.ComputedCol('STC.CURRENT.STATUS')


class StudentCourseSec(intercall.UniFile):
    ic_filename = 'STUDENT.COURSE.SEC'
    scs_course_section = intercall.StringCol(loc=1, dbname='SCS.COURSE.SECTION')
    scs_student = intercall.StringCol(loc=2, dbname='SCS.STUDENT')


class Terms(intercall.UniFile):
    ic_filename = 'TERMS'
    term_reporting_year = intercall.NumCol(loc=1, dbname='TERM.REPORTING.YEAR')
    term_desc = intercall.StringCol(loc=2, dbname='TERM.DESC')
    term_prereg_start_date = intercall.DateCol(loc=3, dbname='TERM.PREREG.START.DATE')
    term_reg_start_date = intercall.DateCol(loc=4, dbname='TERM.REG.START.DATE')
    term_start_date = intercall.DateCol(loc=5, dbname='TERM.START.DATE')
    term_end_date = intercall.DateCol(loc=6, dbname='TERM.END.DATE')
    term_session = intercall.StringCol(loc=7, dbname='TERM.SESSION')
    term_reg_end_date = intercall.DateCol(loc=8, dbname='TERM.REG.END.DATE')
    term_prereg_end_date = intercall.DateCol(loc=9, dbname='TERM.PREREG.END.DATE')
    term_acad_levels = intercall.StringCol(loc=28, dbname='TERM.ACAD.LEVELS')


def main():
    """Parse the command line options"""

    parser = OptionParser(usage="reg_count_export.py EXPORT_FILENAME")
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file (default=%s)" % CONFIG_FILENAME)
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment (default='prod')")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")
    parser.add_option('-t', '--term', dest='term',
                      default=None, metavar='TERM',
                      help='Set the reporting term (default is to use the active terms)')

    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error("You must specify an export filename")

    if not config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = config.get(options.environment)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    global_config = config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    if not options.quiet:
        print("Connecting to the source environment: %s..." % options.environment)

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'],
                        my_config['db_password'],
                        my_config['ldap_password'])

    student_acad_cred = StudentAcadCred(cfccldap.dbutil.getDbSession())
    course_sections = CourseSections(cfccldap.dbutil.getDbSession())

    if options.term is None:
        term_list = list()
        terms = Terms(cfccldap.dbutil.getDbSession())
        result = terms.select(AND(terms.term_acad_levels.eq('CU'),
                                  terms.term_end_date.ge(datetime.date.today()),
                                  terms.term_reg_start_date.ne("")))
        for rec in result:
            term_list.append(rec.record_id)
    else:
        term_list = [options.term]

    timestamp = datetime.datetime.now()
    with open(args[0], 'wb') as fd:
        fd.write(b"add_date_time|term|head_count|seat_count|seat_capacity\n")
        for term in term_list:
            print("[INFO] Select course sections from %s ..." % term)
            result = course_sections.select(AND(course_sections.sec_term.eq(term),
                                                course_sections.sec_current_status.eq('A')))
            seat_capacity = 0
            for rec in result:
                try:
                    seat_capacity += int(rec.sec_capacity)
                except ValueError:
                    pass
            print("[INFO] Found %d seat capacity in %s" % (seat_capacity, term))

            print("[INFO] Select students from %s ..." % term)
            result = student_acad_cred.select(AND(student_acad_cred.stc_acad_level.eq('CU'),
                                                  student_acad_cred.stc_current_status.eq(['A', 'N']),
                                                  student_acad_cred.stc_term.eq(term)))
            seat_count = 0
            headcount = dict()
            for rec in result:
                seat_count += 1
                headcount[rec.stc_person_id] = True
            if seat_count > 0:
                print("[INFO] Found %d seats in %s" % (seat_count, term))
                line_out = "%s|%s|%d|%d|%d\n" % (timestamp, term, len(list(headcount.keys())), seat_count, seat_capacity)
                fd.write(line_out.encode())
            else:
                print("[INFO] Skipped empty term %s" % term)

    cfccldap.disconnectAll()

if __name__ == "__main__":
    main()

#!/usr/bin/env python
"""Lock a LDAP account and stop the user from resetting the password

The password in the LDAP tree is scrambled to prevent the user from
logging in and then the Security Question and Answer in X810.PARAMS is
set to LOCKED to prevent the user from using ResetPassword to get back
in.

"""

import binascii
import hashlib
from optparse import OptionParser
import random

import logging
import sys

sys.path.append("/usr/local/lib/python2.6/site-packages/")
#PRODUCTION PATH#
#sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
#TESTING PATH#
sys.path.append("src")

raise Exception("Still Being Developed!")

import ldap

import cfccldap.config
import cfccldap.dbutil
import intercall

DEBUG = False
ID_LEN = 7

def main():
    """Log into the servers and set the fields to a locked value"""
    global DEBUG

    parser = OptionParser(usage="%prog [options] PERSON_ID")
    parser.add_option("-d", dest="debug", action="store_true", default=False,
                      help="Run in debug mode against the test environment")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment as 'test1', 'test2', or 'prod' (default)")
    parser.add_option("-u", "--unlock", dest="unlock",
                      action="store_true", default=False,
                      help="Reverse the process and unlock the account")

    (options, args) = parser.parse_args()

    if options.debug:
        DEBUG=True
        options.environment = 'test2'

    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    if (len(args) < 1):
        parser.error('You must specify a Person ID')

    person_id = args[0]
    if len(person_id) < ID_LEN:
        padding = ID_LEN - len(person_id)
        person_id = "0" * padding + person_id

    if options.unlock:
        print("Unlocking LDAP Account")
    else:
        print("Locking LDAP Account")
    print("")

    if DEBUG:
        print("NOTICE: Running in DEBUG mode, using Test databases.")
        print("")


    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'])

#    person = Person(session)
#    x810_params = X810Params(session)
#    person_pin = PersonPin(session)

    # Turn off SSL Cert validation only if necessary
    ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
    #ldap.set_option(ldap.OPT_X_TLS_CACERTFILE,
    #                "/usr/local/ssl/certs/Verisign_RSA_Secure_Server_CA.pem")
    
    # if the person doesn't exist then this will fail
    person_rec = cfccldap.person.get(person_id)
    ppin_rec = cfccldap.person_pin.get(person_id)

    if not options.unlock:
        print("")
        print("")
        print("*******************************************************************************")
        print("*********************************** WARNING ***********************************")
        print("*******************************************************************************")
        print("")
        print("You are about to lock the account of %s %s (%s)!" % (person_rec.first_name, person_rec.last_name, person_id))
        print("")
        usr_ans = input("Are you sure (y/N)?")

        if usr_ans.lower() == 'y':
            print("Locking the security question...")
            print("")

            params_rec = cfccldap.x810_params.get(person_id)
            params_rec.x810_p_question = "LOCKED"
            params_rec.x810_p_answer = randPassword()

            dn = "uid=%s,%s,dc=cfcc,dc=edu" % (ppin_rec.user_id,
                                               context(person_rec.where_used))

            print("Scrambling LDAP password field for " + str(dn))
            try:
                modlist = [(ldap.MOD_REPLACE, 'userPassword', randPassword())]
                l.modify(dn, modlist)
            except ldap.LDAPError as error_message:
                print("[LDAPError]", error_message)
        else:
            print("")
            print("Procedure aborted...")
            print("")
    else:
        # Unlock an account, only if it's locked
        params_rec = x810_params.get(person_id)
        if params_rec.x810_p_question == "LOCKED":
            params_rec.x810_p_question = "";
            params_rec.x810_p_answer = "";

            print("Cleared lock on record %s for %s %s" % (person_id, person_rec.first_name, person_rec.last_name))
        else:
            print("Record %s for %s %s was not locked"% (person_id, person_rec.first_name, person_rec.last_name))

    try:
        session.close()
    except Exception as e:
        print(e)

    try:
        l.unbind()
    except Exception as e:
        print(e)

    print("Done")
    print("")

def context(where_used):
    """Return the context based on the WHERE.USED field from PERSON."""
    if 'EMPLOYES' in where_used:
        ctx = 'ou=Employees'
    elif 'FACULTY' in where_used:
        ctx = 'ou=Faculty'
    else:
        ctx = 'ou=Students'
    return ctx

def randPassword(length=10):
    """Return a randomly generated password of the given length."""
    rand_str = ""
    for i in range(length):
        rand_str += chr(random.getrandbits(8))
    rand_str = binascii.hexlify(rand_str)
    m = hashlib.sha1()
    m.update(rand_str)
    password = "{SHA}" + m.hexdigest()
    return password

if __name__ == "__main__":
    main()

#!/usr/bin/env python
"""
Generate and return a random password using the same algorithm that is
used for the guest accounts.

"""
from optparse import OptionParser

import site

site.addsitedir('/usr/local')
#PRODUCTION PATH#
site.addsitedir("/opt/ldap-tools")
#TESTING PATH#
site.addsitedir("src")

from cfccldap import config
from cfccldap.passgen import adPassword, nonRandomPassword
from cfccldap.randompassword import genPassword, simplePassword, loadBadWords2

PASSWORD_ALGORITHM = 'wordlist'
PASSWORD_LENGTH = '8'
CONFIG_FILENAME = 'etc/clg.conf'

DEBUG = False
WORDLIST = []

def main():
    """Parse options and generate the password."""

    global DEBUG
    global WORDLIST

    parser = OptionParser(usage="%prog [options] [NUMBER_OF_PASSWORDS]")
    parser.add_option("-a", "--algorithm", dest="password_algorithm",
                      default=PASSWORD_ALGORITHM, metavar="NAME",
                      help="Select the password algorithm (random, wordlist, simple, guest)")
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-d", dest="debug", action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-l", "--length", dest="password_length",
                      default=PASSWORD_LENGTH, metavar="LEN",
                      help="Number of characters in the password")

    (options, args) = parser.parse_args()

    DEBUG = options.debug

    NUM_PASSWORDS = 1
    if len(args) > 0:
        try:
            NUM_PASSWORDS = int(args[0])
        except ValueError:
            parser.usage("Number of passwords must be an integer")

    if not config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        guest_config = config.get("guests")
    except KeyError as ex:
        parser.error(str(ex))

    for i in range(NUM_PASSWORDS):
        print(generatePassword(options.password_algorithm,
                               guest_config))

def generatePassword(password_algorithm, guest_config):
    password_out = ""
    
    if password_algorithm == 'random':
        password_out = adPassword(guest_config['password_length'])
    elif password_algorithm == 'wordlist':
        #
        # NOTE: for now I'm using the default english dictionary from
        # the system, and ignoring the password_word_file setting from
        # the config file.
        #
        WORDLIST = loadBadWords2(guest_config['bad_words_file'])
        password_out = genPassword(guest_config['password_word_count'],
                                   guest_config['password_min_len'],
                                   guest_config['password_max_len'],
                                   bad_words=WORDLIST)
    elif password_algorithm == 'simple':
        #
        # NOTE: This uses the password_word_file config setting
        #
        WORDLIST = loadBadWords2(guest_config['bad_words_file'])
        password_out = simplePassword(bad_words=WORDLIST,
                                      dictionary=guest_config['password_word_file'])
    elif password_algorithm == 'guest':
        password_out = nonRandomPassword("","")
    else:
        raise Exception("Invalid password algorithm")
    return password_out

if __name__ == "__main__":
    main()

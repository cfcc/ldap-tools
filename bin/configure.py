#!/usr/bin/env python

"""Set and update the configuration file used by the LDAP tools.

"""

import getpass
import os.path
import argparse
from pprint import pprint as pp
import site
import sys

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')
site.addsitedir(os.path.join('/', 'usr', 'local', site_packages))

# PRODUCTION PATH #
# site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
# TESTING PATH #
sys.path.append("src")

import cfccldap.config

CONFIG_FILENAME = "etc/clg.conf"


def user_input(msg, valid_inputs):
    okay = False
    usr_ans = None
    while not okay:
        usr_ans = input(msg)
        if usr_ans in valid_inputs:
            okay = True
        elif usr_ans.lower() == 'q':
            usr_ans = None
            okay = True
        else:
            print("You must enter one of: ", ', '.join(valid_inputs))
    return usr_ans


def main():
    """Parse the command line and run the processes."""

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", dest="config_file",
                        default=CONFIG_FILENAME, metavar="PATH",
                        help="Specify the full path to a configuration file")
    parser.add_argument("-E", "--environment", dest="env",
                        default="prod", metavar="NAME",
                        help="Specify the environment as test or prod (default)")
    parser.add_argument("-p", "--password", dest="gen_passwd",
                        action="store_true", default=False,
                        help="Generate an obfuscated password update the configuration.")
    parser.add_argument("-s", "--show", dest="show_config",
                        action="store_true", default=False,
                        help="show configuration and exit")

    args = parser.parse_args()

    if args.gen_passwd:
        if not cfccldap.config.loadConfiguration(args.config_file, decrypt_passwords=False):
            parser.error("Failed to load configuration file: %s" % args.config_file)

        svc_name = user_input('Update which password: [d]atabase or [l]dap? ', ['d', 'l'])
        if svc_name is None:
            sys.exit(0)

        okay = False
        new_passwd = None
        while not okay:
            new_passwd = getpass.getpass("Enter a new password: ")
            chk_passwd = getpass.getpass("Enter password again: ")
            if new_passwd == chk_passwd and new_passwd != "":
                okay = True
            else:
                print("Password do not match, try again...")
                print("")
        print("")
        cfccldap.config.update_password(args.env, svc_name, new_passwd)
        cfccldap.config.save_configuration(args.config_file)

    if args.show_config:
        if not cfccldap.config.loadConfiguration(args.config_file):
            parser.error("Failed to load configuration file: %s" % args.config_file)

        for env in cfccldap.config.getEnv():
            print("[ENV] ", env)
            pp(cfccldap.config.get(env))
        sys.exit(0)


if __name__ == "__main__":
    main()

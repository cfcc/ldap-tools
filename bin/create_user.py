#!/usr/bin/env python

"""Create new user accounts in Active Directory via LDAP.

This script is focused on created a series of generic users for use in
Student PC Labs.

"""
import logging
from optparse import OptionParser
from pprint import pprint as pp
import sys

sys.path.append("/usr/local/lib/python2.6/site-packages/")
#PRODUCTION PATH#
#sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
#TESTING PATH#
sys.path.append("src")

import ldap
import ldap.modlist
import cfccldap.config
from cfccldap.progress_bar import ProgressBar
from cfccldap.user import LdapUser

def main():
    """Parse the command line options"""

    parser = OptionParser()
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment (default='prod')")
    parser.add_option("-f", "--force", dest="force", action="store_true",
                      default=False, help="Force the update")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")
    parser.add_option("-u", "--user", metavar="USERNAME",
                      dest="username", default=None,
                      help="Specify a username to synchronize.")
    parser.add_option("-y", "--fix", dest="fix", action="store_true", default=False, help="Move the record to the student OU")

    (options, args) = parser.parse_args()
    
    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    if len(args) < 1:
        parser.error("you must specify a user prefix")
    elif len(args) < 2:
        parser.error("you must specify the number of users to create")
    else:
        prefix = args[0]
        try:
            num_users = int(args[1])
        except ValueError:
            parser.error("the number of users must be a positive integer")

    cfccldap.initLogging("log/create_generic_user",
                          full_logging=options.log,
                          debug=options.debug)
    logger1 = logging.getLogger('main')

    print("Connect to the %s environment" % options.environment)
    #cfccldap.util.connect(my_config['ldap_url'], my_config['ldap_user'])

    base_dn = "ou=guests," + config.baseDn(my_config)
    logger1.info("Creating users in from %s", base_dn)

    # Create users here
    for i in range(num_users):
        new_username = "%s_%02i" % (prefix, i)
        new_user = LdapUser(base_dn=config.baseDn(my_config), username=new_username)
        pp(new_user.asDict())

    msg = "Checks complete, closing connections..."
    logger1.info(msg)
    if not options.quiet:
        print(msg)
    cfccldap.util.close()

if __name__ == "__main__":
    main()


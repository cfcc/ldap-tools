#!/usr/bin/env python3
"""Create an AD Guest User Account based on input from a web form

According to policy the guest account lasts a limited amount and is
set to expire automatically.  On each run this script removes expired
guest accounts after another time limit has expired.  That way these
accounts will have additional limitations that will make it inconvenient
to use them for anything other than the original intent.

"""
import datetime
from email.mime.text import MIMEText
import ldap
import logging
import os
from optparse import OptionParser
from pprint import pprint as pp
import smtplib
from socket import gethostname
import sqlobject
from sqlobject.main import SQLObjectNotFound
import sys
import site

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')

#PRODUCTION PATH#
# site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
#TESTING PATH#
site.addsitedir("src")

import cfccldap
from cfccldap import config
from cfccldap.util import LdapReturnCodeError
from cfccldap.user import LdapUser
from cfccldap.passgen import adPassword, nonRandomPassword
from cfccldap.randompassword import genPassword, simplePassword, loadBadWords2
from cfccldap.new_acct_mailbox import NewAcctMailbox, DATE_FORMAT
from cfccldap.guest_model import GuestAccount, connectDb, LEAD_DAYS, DAYS_ACTIVE

SCRIPT_USERNAME = "newacct"
SYSADMINS = []

CONFIG_FILENAME = "etc/clg.conf"
LOG_FILENAME = "create-guestaccount"
LOG_PATH = "log"
MAILBOX_PATH = '/home/%s/Maildir' % SCRIPT_USERNAME
MAX_PASSWD_ATTEMPTS = 5
GUEST_GROUP = "cn=GuestAccounts,ou=groups,dc=ad,dc=cfcc,dc=edu"
SERVER_DOMAIN = ""
MAIL_DOMAIN = ""
DEBUG = False
QUIET = False

# global time/date variables
TODAY = datetime.datetime.now()
# NOTE: if the creation date falls on a weekend, then we'll move it
#       back to the previous Friday, since we are doing this in
#       reverse...  Counting Monday as 0, if TODAY.isoweekday() < 1
#       then add 2 to the lead days to account for the lost weekend.
if TODAY.isoweekday() > 1:
    WEEKEND = 2
else:
    WEEKEND = 0
ACCOUNT_CREATE_DATE = TODAY + datetime.timedelta(days=(LEAD_DAYS + WEEKEND))
ACCOUNT_DELETE_DATE = TODAY - datetime.timedelta(days=1)
ACCOUNT_DAYS_ACTIVE = TODAY - datetime.timedelta(days=DAYS_ACTIVE)


def main():
    """Parse the command line options and run the processes."""
    global DEBUG
    global QUIET
    global SYSADMINS
    global SERVER_DOMAIN
    global MAIL_DOMAIN
    global MAILBOX_PATH
    
    parser = OptionParser(usage="%prog [options]")
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-d", dest="debug", action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment as 'test1' or 'prod' (default)")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-m", "--mail-only", dest="check_mail_only",
                      action="store_true", default=False,
                      help="Check the mailbox only and exit")
    parser.add_option("-n", "--dry-run", dest="dry_run",
                      action="store_true", default=False,
                      help="Run in non-update mode and summarize what would be done.")
    parser.add_option("-p", "--purge", dest="purge_errors",
                      action="store_true", default=False,
                      help="Purge messages that cannot be processed because of errors.")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")
    parser.add_option("-r", "--report", dest="report_mode",
                      action="store_true", default=False,
                      help="list the messages waiting in the Inbox.")
    parser.add_option("-R", "--remove", dest="remove_id",
                      default=None, metavar="ID",
                      help="Remove a request from the database and AD")

    (options, args) = parser.parse_args()

    DEBUG = options.debug
    QUIET = options.quiet

    if not cfccldap.config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    try:
        guest_config = cfccldap.config.get("guests")
        SYSADMINS = guest_config['notification']
        SERVER_DOMAIN = guest_config['server_domain']
        MAIL_DOMAIN = guest_config['mail_domain']
        MAILBOX_PATH = guest_config['mailbox_path']
    except KeyError as ex:
        guest_config = None
        parser.error(str(ex))

    global_config = cfccldap.config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)

    connectDb(guest_config['db_uri'])

    ldap_session = cfccldap.util.LdapSession(my_config['ldap_url'], my_config['ldap_user'], my_config['ldap_password'])
    ldap_session.set_default_domain(my_config['domain'], my_config['user_prefix'], my_config['group_prefix'])

    # logger1 = logging.getLogger('main')

    # Version 2 Changes:
    # on each run we'll do the following processes:
    #  1. Check the mailbox for new messages
    #   a. Parse incoming requests and if good store in database (do I need an
    #      incoming request table?)
    #   b. Send acceptance or rejection mail
    #  2. Run a select on the database to find any accounts that should be ...
    #   a. created
    #   b. modified/extended
    #   c. purged

    if not options.report_mode:
        if options.remove_id is not None:
            try:
                this_record = GuestAccount.get(options.remove_id)
                delete_guest_account(ldap_session, this_record)
            except SQLObjectNotFound:
                print(f'[ERROR] {options.remove_id} is not a valid GuestAccount record id')
        else:
            if options.dry_run:
                print("[INFO] dry-run mode")
            # Check the mailbox for new messages
            #
            process_mailbox(options.dry_run, options.purge_errors)
            #
            # Run queries on the database and process the records, unless
            # we've been told to only process the mailbox
            #
            if not options.check_mail_only:
                #
                # Check for expired accounts
                #
                remove_expired(ldap_session, options.dry_run)
                #
                # Send a notice about the errors and remove the request
                #
                remove_invalid(options.dry_run)
                #
                # Check for requests that are new but will not be processed today
                #
                notify_future_new(options.dry_run)
                #
                # Check for new accounts that have reached their date of creation
                #
                create_accounts(ldap_session, guest_config, options.dry_run)
    else:
        standard_report()


def process_mailbox(dry_run=False, purge_errors=False):
    """Import messages from the Maildir mailbox"""
    logger1 = logging.getLogger('main.process_mailbox')
    mailbox = NewAcctMailbox(MAILBOX_PATH, 'CSV')
    for msg in mailbox.getMessages():
        complete = False
        if DEBUG:
            print(("[DEBUG] " + str(msg)))
        for new_req in msg.requests:
            if not dry_run:
                try:
                    g = GuestAccount(userFirst=new_req.first,
                                     userLast=new_req.last,
                                     eventName=new_req.event_name,
                                     beginDate=new_req.begin_date,
                                     endDate=new_req.end_date,
                                     submitter=new_req.submitter,
                                     ccAddr=new_req.cc_addr,
                                     isValid=new_req.is_valid)
                    complete = True
                except sqlobject.dberrors.DataError as exc:
                    logger1.error('Failed to process guest account request for %s: %s', new_req.event_name, str(exc))
                    if purge_errors:
                        complete = True
            else:
                print(("[INFO] would process %s" % new_req))
        for item in msg.errors:
            if not dry_run:
                try:
                    g = GuestAccount(userFirst=item.first,
                                     userLast=item.last,
                                     eventName=item.event_name,
                                     beginDate=item.begin_date,
                                     endDate=item.end_date,
                                     submitter=item.submitter,
                                     ccAddr=item.cc_addr,
                                     isValid=item.is_valid,
                                     invalidReason=item.invalid_reason)
                    complete = True
                except sqlobject.dberrors.DataError as exc:
                    logger1.error('Failed to send denial on guest account request for %s: %s', item.event_name, str(exc))
            else:
                print(("[INFO] would process %s" % item))
        if complete and not DEBUG:
            # delete email after processing
            mailbox.remove(msg)


def remove_expired(ldap_session, dry_run=False):
    to_delete = GuestAccount.select(
        sqlobject.OR(
            sqlobject.AND(GuestAccount.q.endDate < ACCOUNT_DELETE_DATE,
                          GuestAccount.q.username != ""),
            sqlobject.AND(GuestAccount.q.endDate == None,
                          GuestAccount.q.beginDate < ACCOUNT_DAYS_ACTIVE)))

    # logger1 = logging.getLogger('main.removeExpired')

    if not QUIET:
        print(("Need to remove %d expired accounts today..." % (len(list(to_delete)))))
    if not dry_run:
        for item in to_delete:
            delete_guest_account(ldap_session, item)


def delete_guest_account(ldap_session, guest_record):
    logger1 = logging.getLogger('main.delete_guest_account')
    logger1.info("Removing expired account %s", guest_record.username)
    if DEBUG:
        print("[DEBUG] skipped delete...")
    else:
        if guest_record.username != "":
            for rec in ldap_session.search("(cn=%s)" % guest_record.username, ['DistinguishedName']):
                if rec.has_attribute('distinguishedname'):
                    dn = rec.get_attr_values('distinguishedname')[0]
                    ldap_session.delete(dn)
        GuestAccount.delete(guest_record.id)


def remove_invalid(dry_run=False):
    denied_requests = GuestAccount.select(GuestAccount.q.isValid == False)

    logger1 = logging.getLogger('main.removeInvalid')

    if not QUIET:
        print(("Need to notify %d submitters of an invalid request..." % (len(list(denied_requests)))))
    if not dry_run:
        for item in denied_requests:
            logger1.info("Sending denied request message to %s", item.submitter)
            send_denied_notice(item)
            if not DEBUG:
                GuestAccount.delete(item.id)


def notify_future_new(dry_run=False):
    future_accounts = GuestAccount.select(
        sqlobject.AND(GuestAccount.q.isNew == True,
                      GuestAccount.q.isValid == True,
                      GuestAccount.q.beginDate > ACCOUNT_CREATE_DATE))

    logger1 = logging.getLogger('main.notifyFutureNew')

    if not QUIET:
        print(("Need to notify submitter on %d accounts..." % (len(list(future_accounts)))))
    if not dry_run:
        for item in future_accounts:
            send_received_notice(item)
            if not DEBUG:
                item.isNew = False


def create_accounts(ldap_session, guest_config, dry_run=False):
    to_create = GuestAccount.select(
        sqlobject.AND(GuestAccount.q.beginDate <= ACCOUNT_CREATE_DATE,
                      GuestAccount.q.username == ""))

    logger1 = logging.getLogger('main.createAccounts')

    if not QUIET:
        print(("Need to create %d accounts today..." % len(list(to_create))))
    if not dry_run:
        for item in to_create:
            try:
                logger1.info("New guest account")
                result = create_user(ldap_session, guest_config, item)
                item.isNew = False
                if result:
                    send_created_notice(item)
                    logger1.info("Sent creation notice...")
                else:
                    send_denied_notice(item)
                    logger1.info("Sent denied notice...")
            except ldap.NO_SUCH_ATTRIBUTE as exc:
                logger1.error(f'Failed to create {item.username}: {exc}')
    

def standard_report():
    line_fmt = "%5s  %-20s  %-10s  %-10s  %1s  %s"
    #
    # List all the requests in the database
    #
    print(("=" * 79))
    print((line_fmt % ("ID", "Full Name", "Begin", "End", "S", "Submitter")))
    print(("-" * 79))
    for rec in GuestAccount.select(GuestAccount.q.isValid == True,
                                   orderBy=GuestAccount.q.beginDate):
        print((line_fmt % (rec.id,
                          rec.fullName()[:20],
                          rec.getBeginDate(),
                          rec.getEndDate(),
                          rec.status(),
                          rec.submitter)))
    #
    # List any invalid records still in the database
    #
    line_fmt = "%3s  %-20s  %-50s"
    invalid_accounts = GuestAccount.select(GuestAccount.q.isValid == False)
    if len(list(invalid_accounts)) > 0:
        print(("=" * 79))
        print((line_fmt % ("ID", "Submitter", "Invalid Reason")))
        print(("-" * 79))
        for rec in invalid_accounts:
            print((line_fmt % (rec.id, rec.submitter, rec.invalidReason)))


def create_user(ldap_session, guest_config, guest_account):
    """Returns true if the user was created in LDAP.

    The guest account record will be updated with a reason if the user
    creation fails.

    A random password is generated when the user object is created in
    Active Directory and it is stored in the guest account record so
    we can report to the submitter what password was used.

    """
    result = False
    logger1 = logging.getLogger('main.createUser')

    # First we'll generate a username and password in the database
    guest_account.createUsername()

    #
    # Now check for duplicate usernames and merge the records if necessary
    #
    logger1.info('Checking for duplicate account requests for %s', guest_account.username)
    s = GuestAccount.select(GuestAccount.q.username == guest_account.username,
                            orderBy=GuestAccount.q.beginDate)
    if len(list(s)) > 1:
        if not DEBUG:
            s[0].eventName = s[1].eventName
            s[0].endDate = s[1].endDate
            s[0].isValid = s[1].isValid
            # Copy the password to our current record so that we can send it in the creation notice
            guest_account.password = s[0].password
            # Need to remove the duplicate and set a flag so the AD
            # account is modified and not created!!!
            GuestAccount.delete(s[1].id)
        else:
            print(("[DEBUG] duplicate record for %s" % guest_account.username))
        duplicate_record = True
    else:
        duplicate_record = False
        # Since this is not a duplicate, we can generate a new password for it
        guest_account.password = create_password(guest_config, guest_account)

    user_rec = LdapUser(username=guest_account.username,
                        base_dn=ldap_session.get_search_base(),
                        server_domain_name=guest_config['server_domain'])
    user_rec.company = SCRIPT_USERNAME
    user_rec.email = ""
    user_rec.description = "Requested by %s" % guest_account.submitter
    user_rec.first = str(guest_account.userFirst)
    user_rec.last = str(guest_account.userLast)
    user_rec.context = guest_config['guest_ou']
    # Disabled for now to eliminate one variable when troubleshooting
    # user_rec.account_expires = guest_account.getExpirationDate()
    if not duplicate_record:
        user_rec.new_password = guest_account.password
        # Make sure that Active Directory will create account without a password, the UAC
        # is updated later by setting the password
        user_rec.setUAC('544')

    logger1 = logging.getLogger('main.createUser')
    if DEBUG:
        print(("[DEBUG] would create user " + user_rec.getDn()))
        if duplicate_record:
            pp(user_rec.asDict(no_blanks=True))
        else:
            pp(user_rec.asDict())
        # remove the username and set the result to false so that the
        # record will be processed again.
        guest_account.username = ""
        guest_account.password = ""
        result = False
    else:
        try:
            if duplicate_record:
                ldap_session.modify_user(user_rec.getDn(), user_rec.asDict(no_blanks=True))
                logger1.info("Account successfully modified")
                msg = "Updated user %s" % user_rec.getDn()
                result = True
            else:
                ldap_session.create_user(user_rec.getDn(), user_rec.asDict())
                msg = "Created user %s" % user_rec.getDn()
                ldap_session.update_group_members(GUEST_GROUP, [user_rec.getDn()], [])
                # then modify user w/ password
                logger1.info("Setting password...")
                attempt = 0
                while attempt < MAX_PASSWD_ATTEMPTS:
                    try:
                        attempt += 1
                        ldap_session.set_ldap_password(user_rec.getDn(), user_rec.new_password)
                        logger1.info("Enabling account...")
                        ldap_session.enable_account(user_rec.getDn())
                        logger1.info("Setting password to not expire...")
                        ldap_session.set_password_to_not_expire(user_rec.getDn())
                        logger1.info("Account successfully created")
                        result = True
                        break
                    except ldap.UNWILLING_TO_PERFORM:
                        logger1.error("Password creation attempt %d failed for %s", attempt, guest_account.username)
                        result = False
            if not QUIET:
                print(msg)
            logger1.info(msg)
        except ldap.ALREADY_EXISTS:
            # Now if this ever happens, we'll make a note and move on
            logger1.info("User %s already in directory, skipping...", guest_account.username)
            # guest_account.isValid = False
            guest_account.invalidReason = "User %s already exists" % guest_account.username
        except (ldap.NO_SUCH_OBJECT, ldap.OTHER, ldap.UNWILLING_TO_PERFORM, LdapReturnCodeError) as ex:
            logger1.error("LDAP account creation failed for %s", user_rec.getDn())
            logger1.error(ex, exc_info=True)
            guest_account.invalidReason = f'Account creation failed ({ex})'
    return result


def create_password(guest_config, guest_account):
    """Return a randomly generated password.

    The password is checked against the username to make sure the name
    is not contained in the password.

    Throws an Exception if the password algorithm cannot be found.

    """
    password = None
    if guest_config['password_algorithm'] == 'random':
        password = adPassword(guest_config['password_length'])
    elif guest_config['password_algorithm'] == 'wordlist':
        wordlist = loadBadWords2(guest_config['bad_words_file'])
        password = genPassword(guest_config['password_word_count'],
                               guest_config['password_min_len'],
                               guest_config['password_max_len'],
                               bad_words=wordlist)
    elif guest_config['password_algorithm'] == 'simple':
        #
        # NOTE: Using the safe word list for this one
        #
        password = simplePassword(
            guest_config['password_min_len'],
            guest_config['password_max_len'],
            dictionary=guest_config['password_word_file'])
    elif guest_config['password_algorithm'] == 'nonrandom':
        password = nonRandomPassword(guest_account.userFirst, guest_account.userLast)
    else:
        raise Exception("Invalid password algorithm")
    return password


def send_created_notice(guest_account):
    """
    Send an email that the account has been created.

    sendCreatedNotice(GuestAccount)
    """
    subject = "Guest Account Created"
    msg_body = """New Guest Account

Event: %s

Username: %s
Password: %s

Start Date: %s
Expires: %s

""" % (guest_account.eventName,
       guest_account.username,
       guest_account.password,
       guest_account.getBeginDate(False).strftime(DATE_FORMAT),
       guest_account.getEndDate().strftime(DATE_FORMAT))
    send_notification(guest_account.submitter, subject, msg_body, cc_addr=guest_account.ccAddr)


def send_received_notice(request_rec):
    """
    Send a message that the account will be created in the future.

    sendReceivedNotice(GuestAccount)
    
    """
    subject = "Guest Account Request Received"
    msg_body = """Guest User: %s

Event Name: %s

Your request for a guest account has been received and will be processed on %s.  You will receive a second email when the account is created.
""" % (request_rec.fullName(),
       request_rec.eventName,
       request_rec.whenCreate())
    send_notification(request_rec.submitter, subject, msg_body, request_rec.ccAddr)


def send_denied_notice(request_rec):
    """
    Send a message that the account will not be created.

    sendDeniedNotice(GuestAccount)

    """
    subject = "Guest Account Request Denied"
    if request_rec.invalidReason != "":
        msg_body = request_rec.invalidReason
    else:
        msg_body = 'Account creation failed'
    logger1 = logging.getLogger('main.sendDeniedNotice')
    try:
        send_notification(request_rec.submitter, subject, msg_body)
    except IndexError as ex:
        logger1.warning('Unable to send message: %s', str(ex))


def send_notification(submitter, subject, msg_body, cc_addr=None):
    """Send an email notification that the guest account has been created

    sendNotification("email", "subject", "message"[, cc_addr="email"])
    """
    # recipients
    mail_server = "10.6.8.43"
    # set this to ldap_user.email
    toaddrs = [submitter]
    toaddrs.extend(SYSADMINS)

    # sender identity
    nodename = "%s.%s" % (gethostname(), SERVER_DOMAIN)
    fromaddr = "%s@%s" % (SCRIPT_USERNAME, MAIL_DOMAIN)

    # build the email message
    msg = MIMEText(msg_body)
    msg['To'] = ", ".join(toaddrs)
    msg['From'] = fromaddr
    msg['Subject'] = subject

    if cc_addr is not None:
        msg['Cc'] = cc_addr
        # also add the CC to the list that is sent to the mail server
        toaddrs.append(cc_addr)

    if DEBUG:
        print(("[DEBUG] sendNotification(): %r" % msg))
    else:
        server = smtplib.SMTP(mail_server)
        try:
            server.helo(nodename)
            server.sendmail(fromaddr, toaddrs, msg.as_string())
        finally:
            server.quit()


if __name__ == "__main__":
    main()

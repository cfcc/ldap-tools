#!/usr/bin/env python
"""Rename a user in Colleague as part of the monthly name change

"""
import logging
from optparse import OptionParser
import os.path
from pprint import pprint as pp
import sys

sys.path.append("/usr/local/lib/python2.6/site-packages/")
# PRODUCTION PATH #
# sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
# TESTING PATH #
sys.path.append("src")

import cfccldap
from cfccldap import config
import intercall

CONFIG_FILENAME = "etc/clg.conf"

DEBUG_LIMIT = 10

LOG_PATH = "log"
LOG_FILENAME = "rename_user.log"

def main():
    """Parse the command line options and run the tasks."""
    parser = OptionParser()
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file (default=%s)" % CONFIG_FILENAME)
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment (default='prod')")
    parser.add_option("-f", "--force", dest="force", action="store_true",
                      default=False, help="Force the update")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")

    (options, args) = parser.parse_args()

    if not config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    global_config = config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    # make sure we have a PERSON ID
    if len(args) < 1:
        usr_ans = input("Enter a person ID (blank to quit): ")
        if usr_ans.strip() != "":
            person_id = usr_ans
        else:
            sys.exit(0)
    else:
        person_id = args[0]

    if not options.quiet:
        print("Connecting to the source environment: %s..." % options.environment)

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'],
                        my_config['db_password'],
                        my_config['ldap_password'])

    db_session = cfccldap.dbutil.getDbSession()

    # get the existing username (and user's full name?)
    person_pin = cfccldap.getTable("PersonPin")
    ppin_rec = person_pin.get(person_id)
    current_username = ppin_rec.user_id
    if current_username.strip() == "":
        print("[ERROR] Username is blank in PERSON.PIN")
        sys.exit(1)
    print("Current Username:", current_username)

    # Can I run the subroutine that generates the new username?
    person = cfccldap.getTable('Person')
    person_rec = person.get(person_id)
    if person_rec.middle_name != "":
        middle_initial = person_rec.middle_name[0]
    else:
        middle_initial = ""
    suggested_username = "%s%s%s%s" % (person_rec.first_name[0],
                                       middle_initial,
                                       person_rec.last_name,
                                       person_id[-3:])
    suggested_username = suggested_username.strip().lower()

    # prompt for the new username
    new_username = None
    while new_username is None:
        usr_ans = input("Enter the new username [%s]: " % suggested_username)
        usr_ans = usr_ans.strip()
        if usr_ans == "":
            if suggested_username != "":
                new_username = suggested_username
            else:
                print("You must enter a username (q to quit)")
        elif usr_ans.lower() == 'q':
            sys.exit(0)
        else:
            new_username = usr_ans

    # change the PERSON.PIN username
    if not options.debug:
        print("Updating username in PERSON.PIN ...", end=' ')
        ppin_rec.user_id = new_username

    # change the ORG.ENTITY.ENV username
    if not options.debug:
        print("and ORG.ENTITY.ENV ...", end=' ')

        org_entity = cfccldap.getTable('OrgEntity')
        org_entity_env = cfccldap.getTable('OrgEntityEnv')

        oe_rec = org_entity.get(person_id)
        oee_rec = org_entity_env.get(oe_rec.oe_org_entity_env)
        oee_rec.oee_username = new_username

    # change the email address?
    if not options.debug:
        print("done.")
    else:
        print("[DEBUG] no changes made.")

    print("%s: %s => %s" % (person_id, current_username, new_username))

if __name__ == '__main__':
    main()

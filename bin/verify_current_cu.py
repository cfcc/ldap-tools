#!/usr/bin/env python

"""Verify that all users in the selected OU are curriculum students/faculty.

Uses the same plugin system as the colleague_ldap_groups program.

"""

import logging
from optparse import OptionParser
import os.path
from pprint import pprint as pp
import site
import sys

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')
#PRODUCTION PATH#
site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
#TESTING PATH#
# site.addsitedir("src")

import ldap
import ldap.modlist
import cfccldap
import cfccldap.group
from cfccldap import config
# from cfccldap.progress_bar import ProgressBar
from cfccldap.plugins.base import initPlugins

CONFIG_FILENAME = "etc/clg.conf"
RULE_FILENAME = "etc/verify_ou.yaml"
DEBUG_LIMIT = 10

LOG_PATH = "log"
LOG_FILENAME = "verify_current_cu.log"


def main():
    """Parse the command line options"""

    parser = OptionParser()
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file (default=%s)" % CONFIG_FILENAME)
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment (default='prod')")
    parser.add_option("-f", "--force", dest="force", action="store_true",
                      default=False, help="Force the update")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-o", "--one-way", dest="one_way", action="store_true",
                      default=False, help="Only move users into the priv OU, not out")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")
    parser.add_option("-r", "--rule", dest="rules_file",
                      default=RULE_FILENAME, metavar="PATH",
                      help="Specify the full path to a rule file (default=%s)" % RULE_FILENAME)
    parser.add_option("--show-config", dest="show_config",
                      action="store_true", default=False,
                      help="show configuration and exit")
    parser.add_option("-u", "--user", metavar="USERNAME",
                      dest="username", default=None,
                      help="Specify a username to synchronize.")
    parser.add_option("-y", "--fix", dest="fix", action="store_true", default=False, help="Move the record to the general OU")

    (options, args) = parser.parse_args()
    
    if not config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    global_config = config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    initPlugins()

    if not options.quiet:
        print("Connecting to the source environment: %s..." % options.environment)

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'],
                        my_config['db_password'],
                        my_config['ldap_password'])
    cfccldap.group.setDbSession(cfccldap.dbutil.getDbSession())

    cfccldap.util.setDefaultDomain(my_config['domain'],
                                   my_config['user_prefix'],
                                   my_config['group_prefix'])

    #
    # Load the rules
    #
    if options.rules_file is not None:
        fn_rules = options.rules_file
    else:
        fn_rules = my_config['rules']
    all_rules = cfccldap.group.loadRules(fn_rules)

    msg = "Loaded %d group rules" % len(all_rules)
    logger1.info(msg)
    if not options.quiet:
        print(msg)

    for rule in all_rules:

        msg = "Starting check of " + cfccldap.fullname(rule)
        logger1.info(msg)
        if not options.quiet:
            print(msg)
        #
        # set up the counters for later
        #
        total_ad_cnt = 0
        moved_out_cnt = 0
        tot_coll_cnt = 0
        moved_in_cnt = 0
        #
        # Prerequisite: Get the list of active curriculum students/faculty from
        #               Colleague as a list of user names
        #
        current_cu_members = rule.getMembers()
        #
        # A command line option can be used to skip this step
        #
        if not options.one_way:
            #
            # First: make sure the CurrentCU OU only has current curriculum
            #        students/faculty, move everyone else to the parent OU
            #
            search_base = ",".join(["ou=%s" % i for i in rule.priv_ou])
            search_base += "," + config.baseDn(my_config)
            if options.username:
                user_records = [cfccldap.util.getLdapRecord(options.username)]
            else:
                user_records = cfccldap.util.selectAllPaged(search_base)
            logger1.info("Selecting users from %s", search_base)
            for ldap_rec in user_records:
                if ldap_rec is not None:
                    username = ldap_rec.get_attr_values('cn')[0]
                    dn = ldap_rec.get_dn()
                    if not dn in current_cu_members:
                        if ldap_rec.has_attribute('employeeNumber'):
                            person_id = ldap_rec.get_attr_values('employeeNumber')[0]
                        else:
                            person_id = ""
                        logger1.warning('User %s (%s) is not a current cu student/faculty',
                                        username,
                                        person_id)
                        if options.fix:
                            try:
                                new_ou = cfccldap.util.changeOU(ldap_rec, rule.general_ou)
                                msg = "Moved %s to %s" % (username, new_ou)
                                if not options.quiet:
                                    print(msg)
                                logger1.info(msg)
                                moved_out_cnt += 1
                            except (ldap.INSUFFICIENT_ACCESS, ldap.OTHER) as ex:
                                logger1.error("Move failed for %s", username)
                                logger1.debug(ex, exc_info=True)
                        else:
                            logger1.info("Need to move %s to %s", username, rule.general_ou)
                    total_ad_cnt += 1
                    if options.debug and total_ad_cnt > DEBUG_LIMIT:
                        logger1.info("Debug limit reached %d, exiting...", total_ad_cnt)
                        break
        #
        # Second: Check that everyone who is marked as a current CU
        #         student in Colleague is in the correct OU in Active
        #         Directory, if not move them there
        #
        logger1.info("Verifying all current CU students/faculty are in the correct OU")

        for dn in current_cu_members:
            username = cfccldap.util.dn2username(dn)
            ldap_rec = cfccldap.util.getLdapRecord(username)
            if ldap_rec is not None:
                if not rule.priv_ou[0] in ldap_rec.dn.lower():
                    if rule.general_ou[0] in ldap_rec.dn.lower() or rule.any_source_ou is True:
                        person_id = ldap_rec.get_attr_values('employeeNumber')[0]
                        if ldap_rec.get_ou() != rule.priv_ou[0]:
                            if options.fix:
                                try:
                                    new_ou = cfccldap.util.changeOU(ldap_rec, rule.priv_ou)
                                    msg = "Moved %s to %s" % (username, new_ou)
                                    if not options.quiet:
                                        print(msg)
                                    logger1.info(msg)
                                    moved_in_cnt += 1
                                except (ldap.INSUFFICIENT_ACCESS, ldap.OTHER) as ex:
                                    logger1.error("Move failed for %s", username)
                                    logger1.debug(ex, exc_info=True)
                            else:
                                logger1.info("Need to move %s to %s", username, rule.priv_ou)
                        else:
                            logger1.info("Strangely current OU (%s) actually matches move-to OU (%s)", ldap_rec.get_ou(), rule.priv_ou[0])
                    else:
                        logger1.warning("User %s is in an OU other than %s, not moving.", username, rule.general_ou)
            else:
                logger1.warning("User %s not found in AD", username)
            tot_coll_cnt += 1
            if options.debug and tot_coll_cnt > DEBUG_LIMIT:
                logger1.info("Debug limit reached %d, exiting...", tot_coll_cnt)
                break
        #
        # print a summary
        #
        msg = "Summary: %d of %d removed, %d of %d added to %s" % (
            moved_out_cnt,
            total_ad_cnt,
            moved_in_cnt,
            tot_coll_cnt,
            cfccldap.fullname(rule))
        if not options.quiet:
            print(msg)
        logger1.info(msg)

    logger1.info("Checks complete, closing connections...")
    cfccldap.util.close()


if __name__ == "__main__":
    main()

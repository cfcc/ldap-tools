#!/usr/bin/env python

"""Get the current list of contacts from Active Directory"""

import csv
import logging
import os
from optparse import OptionParser
from pprint import pprint

import intercall

import sys

sys.path.append("/usr/local/lib/python2.6/site-packages/")
#PRODUCTION PATH#
#sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
#TESTING PATH#
sys.path.append("src")

import cfccldap
import cfccldap.config
from cfccldap.user import LdapUser

DEBUG = False

CONFIG_FILENAME = "etc/clg.conf"

OUTPUT_FILENAME = "contacts.csv"

LOG_PATH = "log"
LOG_FILENAME = 'get_contacts'

DEFAULT_COMPANY = 'Cape Fear Community College'
DEFAULT_EMAIL_TYPES = ['SE', 'EE'] # ['GW', 'CC', 'W']
DEFAULT_HEADER = "Action,ID,Name,E-mail Address,Notes,Mobile Phone,Company,Job Title,Business Phone,Business Address,Other Phone,Other Address"
DEFAULT_OPERS = 'WEBFACSTAFF'

def main():
    global DEBUG

    parser = OptionParser(usage="%prog [options]")
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-d", dest="debug", action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment as 'test' or 'prod' (default)")
    parser.add_option("-e", dest="person_id", metavar="ID",
                      help="Specify a PERSON record ID")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-t", "--email-type", metavar="CODE",
                      dest="email_type", default=None,
                      help="Use a specific email type (ex: EE, SE)")

    (options, args) = parser.parse_args()

    if not cfccldap.config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    if options.debug:
        DEBUG = True

    global_config = cfccldap.config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    if options.debug:
        pp(my_config)
        input("Press enter to continue...")

    cfccldap.initLogging(LOG_FILENAME, full_logging=options.log, debug=options.debug)
    logger1 = logging.getLogger('main')

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'],
                        my_config['db_password'],
                        my_config['ldap_password'])

    records = []
    if options.person_id is not None:
        msg = "Reporting single record: " + str(options.person_id)
        print(msg)
        logger1.info(msg)
        records.append(cfccldap.org_entity.get(options.person_id))
    else:
        msg = "Selecting all %s records in ORG.ENTITY." % (DEFAULT_OPERS)
        print(msg)
        logger1.info(msg)
        records = cfccldap.org_entity.select(cfccldap.org_entity.oee_opers.eq(DEFAULT_OPERS))

    # now fetch the information and build the contact list
    contact_list = []
    for rec in records:

        # reusing the LdapUser code to build the information
        try:
            user_rec = LdapUser(rec)
        except intercall.main.IntercallError as err:
            msg = "Error loading student data for %s: %s" % (rec.record_id, err)
            logger1.error(msg)
            return

        email_addr = cfccldap.getEmail(rec.record_id, options.email_type, True)
        if email_addr is not None:
            user_rec.email = email_addr
        user_rec.context = cfccldap.getLdapContext(rec.record_id)
        user_rec.title = cfccldap.getPositionTitle(rec.record_id)
        user_rec.importOffice(cfccldap.getOffice(rec.record_id))
        user_rec.zip = cfccldap.getZip(rec.record_id)

        row = { 'Action': 'add',
                'ID': '',
                'Name': user_rec.getDispName(),
                'E-mail Address': user_rec.email,
                'Company': DEFAULT_COMPANY,
                'Job Title': user_rec.title,
                'Business Phone': user_rec.phone,
                'Business Address': user_rec.office,
                'Other Phone': user_rec.other_phone,
                'Other Address': user_rec.other_office,
                }

        contact_list.append(row)

    with open(OUTPUT_FILENAME, 'wb') as csvfile:
        my_writer = csv.DictWriter(csvfile, DEFAULT_HEADER.split(","))
        my_writer.writerows(contact_list)

    fd = open(OUTPUT_FILENAME, 'r')
    lines = fd.readlines()
    fd.close()

    fd = open(OUTPUT_FILENAME, 'w')
    fd.write(DEFAULT_HEADER + "\n")
    fd.writelines(lines)
    fd.close()
        
    logger1.info("Wrote contacts to %s", OUTPUT_FILENAME)

    cfccldap.disconnectAll()

if __name__ == '__main__':
    main()

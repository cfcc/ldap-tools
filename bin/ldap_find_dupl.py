#!/usr/bin/env python

"""Find students or employees with duplicate records in the LDAP database"""

import logging
from optparse import OptionParser
import os.path
from pprint import pprint
import sys

sys.path.append("/usr/local/lib/python2.6/site-packages/")
# PRODUCTION PATH #
sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
# TESTING PATH #
# sys.path.append("src")

import cfccldap
import cfccldap.config
import cfccldap.group
import cfccldap.unidata_model
import cfccldap.dbutil
import cfccldap.progress_bar

DEBUG = False
VERBOSITY = 0

CONFIG_FILENAME = "etc/clg.conf"

LOG_PATH = "log"
LOG_FILENAME = "ldap_find_dupl.log"

LIMIT = 1000


def main():
    """Parse the options and run the tasks."""
    global DEBUG
    
    parser = OptionParser(usage="%prog [options]")
    parser.add_option("--all", dest="all_staff",
                      action="store_true", default=False,
                      help="Select all records with OEE.OPERS = WEBFACSTAFF")
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-d", "--debug", dest="debug",
                      action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment as 'test' or 'prod' (default)")
    parser.add_option("-e", dest="employee_id",
                      metavar="PERSON.ID", default=None,
                      help="Update the given record only")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-p", "--progress", dest="progress_limit",
                      metavar="LIMIT", default=LIMIT,
                      help="Set a limit for the progress bar")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")
    parser.add_option("-s", "--savedlist",
                      dest="savedlist_name", metavar="NAME",
                      help="Set a savedlist to use")
    parser.add_option("-u", dest="user_id", default=None, metavar="UID",
                      help="Select a user by the LDAP UID")

    (options, args) = parser.parse_args()

    if not cfccldap.config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    global_config = cfccldap.config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    if not options.quiet:
        print("Connecting to the source environment: %s..." % options.environment)

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'],
                        my_config['db_password'],
                        my_config['ldap_password'])

    if options.all_staff:
        options.savedlist_name = cfccldap.dbutil.selectAllStaff()

    report = []
        
    if options.savedlist_name is not None:
        logger1.info("Searching from savedlist: %s", options.savedlist_name)
        try:
            limit = int(options.progress_limit)
        except ValueError:
            limit = LIMIT
        prog = cfccldap.progress_bar.ProgressBar(0, limit, 74, mode='dynamic', char='-')
        oldprog = str(prog)
        session = cfccldap.dbutil.getDbSession()
        session.getList(options.savedlist_name, 3)
        for rec_id in session.readNext(3):
            result = find_duplicates(rec_id)
            if len(result) > 0:
                report.append(result)
            prog.increment_amount()
            if oldprog != str(prog):
                print(prog, "\r", end=' ')
                sys.stdout.flush()
                oldprog = str(prog)
    elif options.employee_id is not None:
        logger1.info("Checking a single record: %s", options.employee_id)
        result = find_duplicates(options.employee_id)
        if len(result) > 0:
            report.append(result)
    elif options.user_id is not None:
        logger1.info("Checking employeeNumber: %s", options.employee_id)
        empl_id = cfccldap.util.getEmployeeNumber(options.user_id)
        result = find_duplicates(empl_id)
        if len(result) > 0:
            report.append(result)
    else:
        logger1.error("No task selected")

    cfccldap.disconnectAll()

    pprint(report)


def find_duplicates(person_id):
    """Return a list with duplicates if found, or empty if not."""
    duplicates = []
    records = cfccldap.util.getLdapRecord(person_id, return_all=True, key_attr='employeeNumber')
    if len(records) > 1:
        duplicates = [lr.get_attr_values('cn')[0] for lr in records]
    return duplicates

if __name__ == "__main__":
    main()

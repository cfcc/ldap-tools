#
# This script is used to find users in Active Directory that look like they should have a Colleague account but do not
#
from argparse import ArgumentParser
import site
import sys
from rich.progress import Progress
from rich.console import Console

from intercall import IntercallError

site.addsitedir('src')

import cfccldap.config

CONFIG_FILENAME = 'etc/clg.conf'

def main():
    parser = ArgumentParser(usage='%(prog)s [options]')
    parser.add_argument("-b", "--base", dest="base_dn",
                        metavar="DN", default=None,
                        help="Set an alternate search DN")
    parser.add_argument('-c', '--config', dest='config_file',
                        default=CONFIG_FILENAME, metavar='FILE',
                        help='Specify the path to a configuration file')
    parser.add_argument('-E', '--environment', dest='env',
                        default='prod', metavar='NAME',
                        help='Specify the environment as test or prod (default)')

    args = parser.parse_args()

    if not cfccldap.config.loadConfiguration(args.config_file):
        print(f'Failed to load configuration file: {args.config_file}')
        sys.exit(1)

    try:
        my_config = cfccldap.config.get(args.env)
    except KeyError as ex:
        my_config = None
        print(str(ex))
        sys.exit(1)

    my_console = Console()

    my_console.print(f'Connecting to the Colleague {args.env} environment')
    cfccldap.connectAll(my_config['db_url'], my_config['db_password'])

    ldap_session = cfccldap.util.LdapSession(my_config['ldap_url'], my_config['ldap_user'], my_config['ldap_password'])
    ldap_session.set_default_domain(my_config['domain'], my_config['user_prefix'], my_config['group_prefix'])

    if args.base_dn is not None:
        search_base = args.base_dn
    else:
        search_base = cfccldap.config.baseDn(my_config)

    totals = {'passed': 0,
              'failed': 0,
              'missing': 0}
    count = 0

    with Progress(console=my_console) as progress:
        task = progress.add_task('Checking LDAP records...', total=1)
        for rec in ldap_session.select_all_paged(alt_search_dn=search_base):
            if rec is not None:
                result = verify(rec, my_console)
                totals[result] += 1
                count += 1
        progress.update(task, advance=1)
        my_console.print("Verified %s records.  %s" % (count, repr(totals)))


def verify(ldap_rec, my_console):
    """Check Colleague to see if this LDAP record has a matching Registry record."""
    dn = ldap_rec.get_dn()
    user = dn[:dn.lower().find("dc=") -1]
    if ldap_rec.has_attribute('employeeNumber'):
        person_id = ldap_rec.get_attr_values('employeeNumber')[0]
        try:
            oe_rec = cfccldap.org_entity.get(person_id)
            if oe_rec.oe_org_entity_env != "":
                result = 'passed'
            else:
                my_console.print(f'[WARNING] ORG.ENTITY record found but OEE is empty for {user}')
                result = 'failed'
        except IntercallError:
            my_console.print(f'[WARNING] ORG.ENTITY record not found for {user}')
            result = 'failed'
    else:
        my_console.print(f'[WARNING] No employee number found in LDAP for {user}')
        result = 'missing'
    return result


if __name__ == '__main__':
    main()

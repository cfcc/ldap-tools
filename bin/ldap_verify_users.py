#!/usr/bin/env python

"""Verify all users in the LDAP tree.

Two things are checked by default: (1) MyCFCC required fields are
populated: email, birthdate, zip code, and ..., (2) an active Datatel
Registry record exists for the account.  Inactive accounts can
generate a warning, or be deleted automatically.

"""

import logging
import os
import site
import sys
from optparse import OptionParser
from pprint import pprint as pp

import ldap
from intercall.main import IntercallError

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')
# PRODUCTION PATH#
# site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
# TESTING PATH#
site.addsitedir("src")

import cfccldap.config
import cfccldap.util
from cfccldap.user import LdapUser

DEBUG = False
VERBOSITY = 0

CONFIG_FILENAME = "etc/clg.conf"
LOG_FILENAME = 'ldap_verify.log'


def main():
    """Parse the options and run the requested update."""
    global DEBUG

    parser = OptionParser(usage="%prog [options]")
    parser.add_option("-a", "--all", dest="all_records",
                      action="store_true", default=False,
                      help="Run validation on all records in the search base")
    parser.add_option("-b", "--base", dest="base_dn",
                      metavar="DN", default=None,
                      help="Set an alternate search DN")
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-D", "--debug", dest="debug",
                      action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-d", "--delete", dest="delete_inactive",
                      action="store_true", default=False,
                      help="Delete inactive accounts instead of generating a warning.")
    parser.add_option("-E", "--environment", dest="env",
                      default="prod", metavar="NAME",
                      help="Specify the environment as test or prod (default)")
    parser.add_option("-e", dest="employee_id", metavar="PERSON.ID",
                      help="Update the given record only")
    parser.add_option("-f", "--fix-duplicates", dest="duplicates",
                      action="store_true", default=False,
                      help="Fix duplicate records.")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-n", "--dry-run", dest="dry_run",
                      action="store_true", default=False,
                      help="Do not perform any updates")
    parser.add_option("-m", "--missing", dest="missing_data",
                      action="store_true", default=False,
                      help="Select LDAP records missing data required for MyCFCC")
    parser.add_option("-u", "--user", dest="username", metavar="USERNAME",
                      help="Update the record USERNAME only.")

    (options, args) = parser.parse_args()

    if not cfccldap.config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    log_level = logging.INFO
    if options.debug:
        DEBUG = True
        log_level = logging.DEBUG

    try:
        my_config = cfccldap.config.get(options.env)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    global_config = cfccldap.config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = LOG_FILENAME

    cfccldap.initLogging(log_fn, log_level, options.log, debug=options.debug)
    logger = logging.getLogger('main')

    print('Connecting to the Colleague %s environment' % options.env)
    cfccldap.connectAll(my_config['db_url'], my_config['db_password'])

    ldap_session = cfccldap.util.LdapSession(my_config['ldap_url'], my_config['ldap_user'], my_config['ldap_password'])
    ldap_session.set_default_domain(my_config['domain'], my_config['user_prefix'], my_config['group_prefix'])

    totals = {'passed': 0,
              'fixed': 0,
              'deleted': 0,
              'missing': 0,
              'inactive': 0}
    if options.employee_id is not None:
        logger.info("Verifying single record: %s", options.employee_id)
        if options.duplicates:
            rec = ldap_session.get_ldap_record(options.employee_id, True, key_attr='employeeNumber')[0]
            result = duplicates(rec, options.dry_run, ldap_session)
        else:
            rec = ldap_session.get_ldap_record(options.employee_id, key_attr='employeeNumber')
            result = verify(rec, ldap_session, options.employee_id, options.delete_inactive, options.dry_run)
        print("Verified record: %s" % result)
    elif options.username is not None:
        logger.info("Verifying single record: %s", options.username)
        if options.duplicates:
            rec = ldap_session.get_ldap_record(options.username, True, key_attr=my_config['key_attr'])[0]
            result = duplicates(rec, options.dry_run, ldap_session)
        else:
            rec = ldap_session.get_ldap_record(options.username, key_attr=my_config['key_attr'])
            if rec.has_attribute('employeeNumber'):
                employee_id = rec.get_attr_values('employeeNumber')[0]
            else:
                employee_id = None
            result = verify(rec, ldap_session, employee_id, options.delete_inactive, options.dry_run)
        print("Verified record: %s" % result)
    elif options.duplicates:
        logger.info("Checking for duplicates on all LDAP records")
        count = 0
        if options.base_dn is not None:
            search_base = options.base_dn
        else:
            search_base = cfccldap.config.baseDn(my_config)
        for rec in ldap_session.select_all_paged(search_base):
            if rec is not None:
                result = duplicates(rec, options.dry_run, ldap_session)
                count += 1
                totals[result] += 1
            else:
                print("Done")
        print("Checked %s records for duplicates.  %s" % (count, repr(totals)))
    elif options.missing_data:
        logger.info("Checking for records missing required fields")
        count = 0
        query = r'(&(objectClass=person)(!(mail=*))(employeeNumber=*))'
        for rec in ldap_session.search(query):
            if rec is not None:
                result = verify(rec, ldap_session, None, options.delete_inactive, options.dry_run)
                count += 1
                totals[result] += 1
            else:
                print("Done")
        print("Verified %s records.  %s" % (count, repr(totals)))
    elif options.all_records:
        logger.info("Verifying all LDAP records")
        count = 0
        if options.base_dn is not None:
            search_base = options.base_dn
        else:
            search_base = cfccldap.config.baseDn(my_config)
        for rec in ldap_session.select_all_paged(alt_search_dn=search_base):
            if rec is not None:
                result = verify(rec, ldap_session, None, options.delete_inactive, options.dry_run)
                count += 1
                totals[result] += 1
            else:
                print("Done")
            # usr_ans = raw_input("press enter to continue: ")
        print("Verified %s records.  %s" % (count, repr(totals)))
    else:
        logger.info("No action selected, exiting...")

    cfccldap.disconnectAll()


def verify(ldap_rec, ldap_session, person_id=None, delete_inactive=False, dry_run=False):
    """Validate an individual records."""
    result = 'passed'
    in_registry = False
    oe_rec = None
    logger = logging.getLogger("main.verify")
    if ldap_rec is not None:
        # Truncate the DN for logging purposes
        dn = ldap_rec.get_dn()
        user = dn[:dn.lower().find("dc=") - 1]
        if person_id is None:
            if ldap_rec.has_attribute("employeeNumber"):
                person_id = ldap_rec.get_attr_values("employeeNumber")[0]
            else:
                logger.warning("Person ID not found for %s", user)
                result = 'missing'
        # if we have a person ID check that the record exists and is
        # active in the Colleague Registry
        logger.debug("Checking record %s (%s)", user, person_id)
        if person_id is not None:
            try:
                oe_rec = cfccldap.org_entity.get(person_id)
                if oe_rec.oe_org_entity_env != "":
                    in_registry = True
                    logger.debug("Validated registry for %s", person_id)
            except IntercallError:
                logger.warning("ORG.ENTITY record not found for %s", user)
            # if a record is not in the registry then we either delete
            # it or print a warning
            if not in_registry:
                if delete_inactive is False or dry_run is True:
                    logger.debug("Inactive record: %s", person_id)
                    result = 'inactive'
                else:
                    result = 'deleted'
                    ldap_session.delete(ldap_rec.get_dn())

        # Since we found the Colleague record, now check to see that the AD record has all the required information
        if in_registry:
            # get user record
            user_rec = LdapUser(oe_rec)

            user_rec.zip = cfccldap.getZip(person_id)
            user_rec.email = cfccldap.getEmail(person_id, required=True)
            user_rec.title = cfccldap.getPositionTitle(person_id)
            user_rec.depts_desc = cfccldap.getDeptDesc(person_id)
            user_rec.importOffice(cfccldap.getOffice(person_id))

            if DEBUG:
                pp(user_rec.asDict())
            missing_fields = user_rec.missingRequiredFields(ldap_rec)
            if len(missing_fields) > 0:
                if not dry_run:
                    try:
                        ldap_session.modify_user(ldap_rec.get_dn(), user_rec.requiredFields(missing_fields))
                        logger.info("Updated MyCFCC required fields for %s (%s)", user, person_id)
                        result = "fixed"
                    except ldap.INSUFFICIENT_ACCESS:
                        logger.critical("Failed to update %s (%s): Insufficient Access", user, person_id)
                    except ldap.NO_SUCH_ATTRIBUTE:
                        logger.critical("Failed to update %s (%s): No such attribute", user, person_id)
                else:
                    logger.info("Need to update LDAP record for %s (%s): %s", user, person_id, repr(missing_fields))
            else:
                result = "passed"
                logger.debug("LDAP record is up to date for %s (%s)", user, person_id)
        else:
            logger.debug("Not validating user when not found in registry")
    else:
        logger.error("No LDAP record found for %s", person_id)
        result = 'missing'
    return result


def duplicates(ldap_rec, dry_run, ldap_session):
    result = ""
    person_id = None
    logger = logging.getLogger("main.dupl")
    if ldap_rec is not None:
        # Truncate the DN for logging purposes
        dn = ldap_rec.get_dn()
        user = dn[:dn.find("dc=") - 1]
        if ldap_rec.has_attribute("employeeNumber"):
            person_id = ldap_rec.get_attr_values("employeeNumber")[0]
        else:
            logger.warning("Person ID not found for %s", user)
            result = 'missing'
        if person_id is not None:
            records = ldap_session.get_ldap_record(person_id, return_all=True, key_attr='employeeNumber')
            # first check to see if any of these records have an
            # invalid user_id by comparing it to the PERSON.PIN record
            if len(records) > 1:
                pp_rec = cfccldap.person_pin.get(person_id)
                # temporary array to hold good records
                rec_hold = []
                for lr in records:
                    cn = lr.get_attr_values('cn')[0]
                    logger.info("Checking %s in PERSON.PIN...", cn)
                    if cn != pp_rec.user_id:
                        dn = lr.get_dn()
                        if not dry_run:
                            ldap_session.delete(dn)
                            logger.info("Automatically removed record %s", dn)
                        else:
                            logger.warning("Automatically removing %s", dn)
                    else:
                        rec_hold.append(lr)
                # keep only the records that passed the check
                records = rec_hold[:]
            if len(records) > 1:
                rec_len = len(records) - 1
                print("\n" + "-" * 75 + "\n")
                for i, lr in enumerate(records):
                    if lr.has_attribute('shadowLastChange'):
                        slc = lr.get_attr_values('shadowLastChange')
                    else:
                        slc = None
                    if lr.has_attribute('userPassword'):
                        passwd = lr.get_attr_values('userPassword')
                    else:
                        passwd = None
                    print(i, lr.get_dn(), passwd, slc)
                which = None
                while which is None:
                    usr_ans = input("\nDelete which record (0-%d) [SKIP]: " % rec_len)
                    if usr_ans.strip() == "":
                        result = "skipped"
                        break
                    else:
                        try:
                            which = int(usr_ans)
                            if which < 0 or which > rec_len:
                                print("You must enter a number between 0 and %d" % rec_len)
                                which = None
                        except ValueError:
                            print("You must enter a number")
                if which is not None:
                    result = 'deleted'
                    dn = records[which].get_dn()
                    if not dry_run:
                        ldap_session.delete(dn)
                    else:
                        logger.warning("Deleting %s", dn)
            else:
                result = 'passed'
    return result


if __name__ == "__main__":
    main()

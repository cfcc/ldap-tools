#!/usr/bin/env python3

import argparse
import os
import sys

import pytz
import datetime
import site
from rich import box
from rich.console import Console
from rich.padding import Padding
from rich.table import Table
from rich.text import Text

site.addsitedir("src")

import cfccldap
from cfccldap import config, ldaphelper

CONFIG_FILENAME = "~/.clg_conf"


def convert_timestamp(ad_ts, my_tz='US/Eastern'):
    if ad_ts > 0:
        value = datetime.datetime(1601, 1, 1, tzinfo=datetime.timezone.utc) + datetime.timedelta(seconds=ad_ts / 10000000)
        return value.astimezone(pytz.timezone(my_tz))
    else:
        return None


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--all', help='Show all attributes', action='store_true')
    parser.add_argument("-c", "--config", dest="config_file",
                        default=CONFIG_FILENAME, metavar="PATH",
                        help="Specify the full path to a configuration file")
    parser.add_argument('-E', '--environment', default='prod', metavar='NAME',
                        help="Specify the environment as test or prod (default)")
    parser.add_argument("-e", '--employeeid', help="Find the account by the PERSON ID", action='store_true')
    parser.add_argument('-f', '--fullname', help="Find the account by the first and last name", action='store_true')
    parser.add_argument('-g', '--givenname', help='Find the account by the first name', action='store_true')
    parser.add_argument('-s', '--surname', help='Find the account by the last name', action='store_true')

    parser.add_argument('search_key', nargs='+')

    args = parser.parse_args()

    console = Console()

    if not config.loadConfiguration(os.path.expanduser(args.config_file)):
        console.print(f'[red]Failed to load the configuration file:[/red] {args.config_file}')
        sys.exit(1)

    try:
        my_config = config.get(args.environment)
    except KeyError as exc:
        console.print(f'[red]Invalid environment key specified for -E: {args.environment}[/red]')
        console.print(str(exc))
        sys.exit(1)

    ldap_session = cfccldap.util.LdapSession(my_config['ldap_url'], my_config['ldap_user'], my_config['ldap_password'])
    ldap_session.set_default_domain(my_config['domain'], my_config['user_prefix'], my_config['group_prefix'])

    if args.employeeid:
        query = args.search_key[0]
        key_name = 'employeeNumber'
    elif args.fullname:
        if len(args.search_key) < 2:
            console.print('[red]ERROR: --fullname requires a first and last name.[/red]')
            console.print('[red]       Use --surname or --givenname to search for just one.[/red]')
            sys.exit(1)
        query = "{0}*{1}".format(args.search_key[0], args.search_key[1])
        key_name = 'displayName'
    elif args.surname:
        key_name = 'sn'
        query = args.search_key[0]
    else:
        key_name = 'sAMAccountName'
        query = args.search_key[0]

    result = ldap_session.get_ldap_record(query, key_attr=key_name, return_all=True, attributes=ldaphelper.important_attributes)

    if len(result) == 1:
        rec = result[0]
        is_disabled = bool(int(rec.get_attr_values('userAccountControl')[0]) & 0x0002)
        if is_disabled:
            status_highlight = 'bold magenta'
        else:
            status_highlight = 'green'
        if args.all:
            console.print(rec.pretty_print())
        else:
            table = Table(title=Text(rec.get_dn(), style=status_highlight), box=box.HORIZONTALS)
            table.add_column('Attribute', style='cyan', no_wrap=True)
            table.add_column('Value')
            for line in rec.get_formatted_attributes():
                table.add_row(line[0], Text(line[1], overflow='ellipsis', no_wrap=True))

            if is_disabled:
                table.add_row('Account Status', Text('Disabled', style=status_highlight))
            else:
                table.add_row('Account Status', Text('Normal', style=status_highlight))
            console.print(Padding(table, (1, 0)))
    elif len(result) > 1:
        table = Table(title="Multiple Records Found", box=box.HORIZONTALS)
        table.add_column('Person ID', no_wrap=True)
        table.add_column('distinguishedName')
        for rec in result:
            table.add_row(rec.get_attr_values('employeeNumber')[0], Text(rec.get_dn(),
                                                                         overflow='ellipsis', no_wrap=True))
        console.print(Padding(table, (1, 0)))
    else:
        console.print('Record not found in Active Directory', style='bold red')


if __name__ == '__main__':
    main()

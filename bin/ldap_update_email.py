#!/usr/bin/env python

"""Add email addresses to the accounts that are missing them"""

import logging
from optparse import OptionParser

import intercall

import sys

sys.path.append("/usr/local/lib/python2.6/site-packages/")
#PRODUCTION PATH#
sys.path.append("/opt/ldap-tools/lib/python2.6/site-packages/")
#TESTING PATH#
#sys.path.append("src")

import cfccldap
import cfccldap.config

DEBUG = False
LOG_FILENAME = 'log/ldap_update_email'
DEFAULT_EMAIL_TYPES = ['SE', 'EE'] # ['GW', 'CC', 'W']

def main():
    global DEBUG

    parser = OptionParser(usage="%prog [options]")
    parser.add_option("-d", dest="debug", action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment as 'test' or 'prod' (default)")
    parser.add_option("-e", dest="person_id", metavar="ID",
                      help="Specify a PERSON record ID")
    parser.add_option("-f", "--force", dest="force",
                      action="store_true", default=False,
                      help="Force the update, even on records with an email.")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-s", dest="savedlist_name", metavar="NAME",
                      help="Set a savedlist to use")
    parser.add_option("-t", "--email-type", metavar="CODE",
                      dest="email_type", default=None,
                      help="Use a specific email type (ex: EE, SE)")

    (options, args) = parser.parse_args()

    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        parser.error(str(ex))

    if options.debug:
        DEBUG = True

    cfccldap.initLogging(LOG_FILENAME, full_logging=options.log, debug=options.debug)
    logger1 = logging.getLogger('main')

    if options.savedlist_name is None and options.person_id is None:
        print("Selecting all active records in ORG.ENTITY.")
        usr_ans = input("Do you want to continue (y/N)?")
        if usr_ans.lower() != "y":
            sys.exit(0)

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'])

    if options.savedlist_name is not None:
        msg = "Updating records from savedlist: " + options.savedlist_name
        print(msg)
        logger1.info(msg)
        session = cfccldap.dbutil.getDbSession()
        session.getList(options.savedlist_name, 3)
        for rec_id in session.readNext(3):
            rec = cfccldap.org_entity.get(rec_id)
            doUpdate(rec, options.email_type, options.force, options.debug)
    elif options.person_id is not None:
        msg = "Updating single record: " + str(options.person_id)
        print(msg)
        logger1.info(msg)
        rec = cfccldap.org_entity.get(options.person_id)
        doUpdate(rec, options.email_type, options.force, options.debug)
    else:
        msg = "Updating all records in ORG.ENTITY."
        print(msg)
        logger1.info(msg)
        records = cfccldap.org_entity.select(cfccldap.org_entity.oe_org_entity_env.ne(""))
        for rec in records:
            doUpdate(rec, options.email_type, options.force, options.debug)

    cfccldap.disconnectAll()

def doUpdate(rec, email_type, force, debug):
    """
    Run the email update on the LDAP record.
    
    Try to get the Colleague email, but fall back to a self-generated
    one if we can't find anything.
    
    """
    logger2 = logging.getLogger('main.doUpdate')
    try:
        new_mail = cfccldap.getEmail(rec.record_id, email_type)
        if new_mail is None:
            for t in DEFAULT_EMAIL_TYPES:
                new_mail = cfccldap.getEmail(rec.record_id, t)
                if new_mail is not None:
                    logger2.info("No preferred email, using default type '%s'", t)
                    break
            if new_mail is None:
                logger2.warning("No email found for %s", rec.record_id)
        if not debug:
            cfccldap.util.updateEmail(rec, new_mail, force)
        else:
            print("[DEBUG] updateEmail(%s, %s, %s)" % (rec.record_id, new_mail, force))
    except intercall.IntercallError as val:
        logger2.error("Failed on %s: %s", rec.record_id, val)


if __name__ == "__main__":
    main()

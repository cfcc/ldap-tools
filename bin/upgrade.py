#!/usr/bin/env python
"""An upgrade script for the guest account database."""

import sqlobject
import sys

sys.path.append("/usr/local/lib/python2.7/site-packages/")
#PRODUCTION PATH#
sys.path.append("/opt/ldap-tools/lib/python2.7/site-packages/")
#TESTING PATH#
#sys.path.append("src")

import cfccldap
from cfccldap import config
#import cfccldap.guest_model

CONFIG_FILE = 'etc/clg.conf'

def main():

    print("Loading configuration ...", end=' ')

    if not cfccldap.config.loadConfiguration(CONFIG_FILE):
        parser.error("Failed to load configuration file: %s" % CONFIG_FILE)

    try:
        guest_config = cfccldap.config.get("guests")
    except KeyError as ex:
        parser.error(str(ex))
    print("done")

    print("Starting database upgrade")

    print("Connected to %s" % guest_config['db_uri'])
    connection = sqlobject.connectionForURI(guest_config['db_uri'])
    sqlobject.sqlhub.processConnection = connection

    print("Version x.y - Add a CC address field ...", end=' ')

    class GuestAccount(sqlobject.SQLObject):
        class sqlmeta:
            fromDatabase = True

    if 'ccAddr' not in GuestAccount.sqlmeta.columns:
        GuestAccount.sqlmeta.addColumn(sqlobject.StringCol('cc_addr', default=""), changeSchema=True)
        print("UPDATED")
    else:
        print("not needed")

    print("Upgrade complete")

if __name__ == "__main__":
    sys.exit(main())

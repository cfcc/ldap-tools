#!/usr/bin/env python

"""Verify all users in the Employee OU are current employees.

"""

import logging
import site
from optparse import OptionParser
import os
import sys

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')

#PRODUCTION PATH#
site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
#TESTING PATH#
# site.addsitedir("src")

import ldap
import ldap.modlist
import cfccldap
from cfccldap import config
from cfccldap.progress_bar import ProgressBar
from cfccldap.user import LdapUser

CONFIG_FILENAME = "etc/clg.conf"
DEBUG_LIMIT = 5

EMPLOYEE_GROUP = 'CURRENT_EMPLOYEES'
NEWSUPERIOR = ['students', 'inactive employees']

LOG_PATH = "log"
LOG_FILENAME = "verify_employees"

def main():
    """Parse the command line options"""

    parser = OptionParser()
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment (default='prod')")
    parser.add_option("-f", "--force", dest="force", action="store_true",
                      default=False, help="Force the update")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")
    parser.add_option("-u", "--user", metavar="USERNAME",
                      dest="username", default=None,
                      help="Specify a username to synchronize.")
    parser.add_option("-y", "--fix", dest="fix", action="store_true", default=False, help="Move the record to the student OU")

    (options, args) = parser.parse_args()
    
    if not config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = config.get(options.environment)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    global_config = config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    if not options.quiet:
        print("Connecting to the source environment: %s..." % options.environment)

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'],
                        my_config['db_password'],
                        my_config['ldap_password'])

    cfccldap.util.setDefaultDomain(my_config['domain'],
                                   my_config['user_prefix'],
                                   my_config['group_prefix'])

    #
    # First: make sure the Employees OU only has current employees,
    #        move everyone else to the student OU
    #
    if options.debug:
        debug_cnt = 0
    search_base = "ou=employees," + config.baseDn(my_config)
    if options.username:
        user_records = [cfccldap.util.getLdapRecord(options.username)]
    else:
        user_records = cfccldap.util.selectAllPaged(search_base)
    logger1.info("Selecting users from %s", search_base)
    for ldap_rec in user_records:
        if ldap_rec is not None:
            status = verify(ldap_rec, ['CURRENT_EMPLOYEES', 'TEST_GROUP'])
            if not status:
                if ldap_rec.has_attribute('employeeNumber'):
                    person_id = ldap_rec.get_attr_values('employeeNumber')[0]
                else:
                    person_id = ""
                username = ldap_rec.get_attr_values('cn')[0]
                logger1.warning('User %s (%s) is not a current employee',
                                username,
                                person_id)
                if options.fix:
                    try:
                        new_ou = cfccldap.util.changeOU(ldap_rec, NEWSUPERIOR)
                        msg = "Moved %s to %s" % (username, new_ou)
                        if not options.quiet:
                            print(msg)
                        logger1.info(msg)
                    except (ldap.INSUFFICIENT_ACCESS, ldap.OTHER) as ex:
                        logger1.error("Move failed for %s", username)
                        logger1.debug(ex, exc_info=True)
                if options.debug:
                    debug_cnt += 1
                    if debug_cnt > DEBUG_LIMIT:
                        print("Debug limit reached %d, exiting..." % debug_cnt)
                        break

    #
    # Second: Check that everyone in CURRENT_EMPLOYEES is in the
    #         employees OU, if not move them there
    #
    if options.debug:
        debug_cnt = 0
    logger1.info("Retrieving membership of %s", EMPLOYEE_GROUP)
    emp_group = cfccldap.util.getLdapGroup(EMPLOYEE_GROUP)
    curr_employees = emp_group.get_b_members()
    if options.username:
        # determine if this record is in CURRENT_EMPLOYEES because if
        # not, we'll just skip the whole test by leaving the members
        # list empty
        ldap_rec = cfccldap.util.getLdapRecord(options.username)
        if ldap_rec.get_dn() in curr_employees:
            members = [ldap_rec.get_dn()]
        else:
            members = []
    else:
        members = curr_employees
    for b_dn in members:
        dn = b_dn.decode()
        if 'students' in dn.lower():
            username = cfccldap.util.dn2username(dn)
            ldap_rec = cfccldap.util.getLdapRecord(username)
            person_id = ldap_rec.get_attr_values('employeeNumber')[0]
            correct_ou = cfccldap.getLdapContext(person_id)
            if ldap_rec.get_ou() != correct_ou[0]:
                if options.fix:
                    try:
                        new_ou = cfccldap.util.changeOU(ldap_rec, correct_ou)
                        msg = "Moved %s to %s" % (username, new_ou)
                        if not options.quiet:
                            print(msg)
                        logger1.info(msg)
                    except (ldap.INSUFFICIENT_ACCESS, ldap.OTHER) as ex:
                        logger1.error("Move failed for %s", username)
                        logger1.debug(ex, exc_info=True)
                else:
                    logger1.info("Need to move %s to %s", username, correct_ou)
            else:
                msg = "Employee %s (%s) missing WHERE.USED info" % (username, person_id)
                if not options.quiet:
                    print(msg)
                logger1.warning(msg)
            if options.debug:
                debug_cnt += 1
                if debug_cnt > DEBUG_LIMIT:
                    print("Debug limit reached %d, exiting..." % debug_cnt)
                    break
        
    logger1.info("Checks complete, closing connections...")
    cfccldap.util.close()


def verify(ldap_rec, group_names):
    """return true if the user has the group (or groups) requested"""
    result = False
    logger1 = logging.getLogger('main.verify')
    # convert a single group name to a list
    if not isinstance(group_names, list):
        group_names = [group_names]
    if ldap_rec.has_attribute('memberOf'):
        for group_dn in ldap_rec.get_attr_values('memberOf'):
            name = group_dn[group_dn.find('=') + 1:group_dn.find(',')]
            if name in group_names:
                result = True
                break
    else:
        logger1.error("%s does not have memberOf", ldap_rec.get_attr_values('cn')[0])
    return result


if __name__ == "__main__":
    main()

#!/usr/bin/env python

"""Delete users in Active Directory.

Sometimes Colleague and LDAP get out of sync so I need a script to
delete users that Colleague thinks do not exist yet.

"""

import logging
import os
import site
from optparse import OptionParser
import sys
from pprint import pprint as pp

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')
#PRODUCTION PATH#
site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
#TESTING PATH#
# sys.path.append("src")

import cfccldap
import cfccldap.config
import cfccldap.util
from cfccldap.progress_bar import ProgressBar

CONFIG_FILENAME = "etc/clg.conf"
LOG_PATH = "log"
LOG_FILENAME = "delete_user.log"


def main():
    """Parse the command line options and run the script"""

    parser = OptionParser()
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-e", dest="person_id", metavar="ID",
                      help="Specify a PERSON record ID")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment (default='prod')")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-p", dest="pbar_limit", metavar="NUM",
                      help="Display a progress bar for NUM records")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")
    parser.add_option("-s", dest="savedlist_name", metavar="NAME",
                      help="Set a savedlist to use")
    parser.add_option("-u", "--user", metavar="USERNAME",
                      dest="username", default=None,
                      help="Specify a username to synchronize.")

    (options, args) = parser.parse_args()
    
    if options.savedlist_name is None and options.person_id is None:
        parser.error("You must specify a savedlist or a person ID")

    if not cfccldap.config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = cfccldap.config.get(options.environment)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    global_config = cfccldap.config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    if options.debug:
        pp(global_config)
        pp(my_config)
        input("Press enter to continue...")

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    cfccldap.connectAll(my_config['db_url'], my_config['db_password'])

    ldap_session = cfccldap.util.LdapSession(my_config['ldap_url'], my_config['ldap_user'], my_config['ldap_password'])
    ldap_session.set_default_domain(my_config['domain'], my_config['user_prefix'], my_config['group_prefix'])

    if options.savedlist_name is not None:
        msg = "Deleting records from savedlist: %s" % options.savedlist_name
        logger1.info(msg)
        if not options.quiet:
            print(msg)
        session = cfccldap.dbutil.getDbSession()
        session.getList(options.savedlist_name, 3)
        # initialize the optional progress bar
        prog = None
        pbar_cnt = 0
        oldprog = ''
        if options.pbar_limit is not None:
            try:
                limit = int(options.pbar_limit)
                prog = ProgressBar(0, limit, 74, mode='fixed')
                oldprog = str(prog)
            except ValueError:
                msg = "Progress bar limit must be an integer"
                logger1.warning(msg)
                if not options.quiet:
                    print(msg)
        # go through the savedlist and delete the LDAP records
        for rec_id in session.readNext(3):
            do_delete(rec_id, ldap_session)
            # update the progress bar
            if prog is not None:
                pbar_cnt += 1
                prog.update_amount(pbar_cnt)
                if oldprog != str(prog):
                    print(prog, "\r", end=' ')
                    sys.stdout.flush()
                    oldprog = str(prog)
    elif options.person_id is not None:
        msg = "Deleting single record: %s" % options.person_id
        logger1.info(msg)
        if not options.quiet:
            print(msg)
        do_delete(options.person_id, ldap_session)

    msg = "Processing complete, closing connections..."
    logger1.info(msg)
    if not options.quiet:
        print(msg)
    cfccldap.disconnectAll()


def do_delete(person_id, ldap_session):
    """Delete the record from LDAP"""
    logger1 = logging.getLogger('main.do_delete')

    # get the username from person.pin
    ppin_rec = cfccldap.person_pin.get(person_id)
    if ppin_rec.user_id.strip() == "":
        logger1.warning("No username for %s, skipping...", person_id)
    else:
        ldap_rec = ldap_session.get_ldap_record(ppin_rec.user_id)
        if ldap_rec is not None:
            logger1.debug("Deleting %s, %s from LDAP", person_id, ldap_rec.get_dn())
            ldap_session.delete(ldap_rec.get_dn())
        else:
            logger1.info("LDAP record not found for %s (%s)", ppin_rec.user_id, person_id)


if __name__ == "__main__":
    main()

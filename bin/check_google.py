#!/usr/bin/env python

import gdata.apps.service
import getpass
from optparse import OptionParser

domains = { 'gtest.cfcc.edu': { 'email': 'administrator@gtest.cfcc.edu',
                                'password': None },
            'mail.cfcc.edu': { 'email': 'administrator@mail.cfcc.edu',
                                'password': None },
            }
DEFAULT_DOMAIN = 'mail.cfcc.edu'

def main():
    parser = OptionParser(usage="%prog [options] USER_ID")
    parser.add_option("-d", "--debug", dest="debug",
                      action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-D", "--domain", dest="domain", default=DEFAULT_DOMAIN,
                      help="Specify the domain hosted by Google Apps.")
    (options, args) = parser.parse_args()

    username = None
    if len(args) >= 1:
        username = args[0]
    else:
        parser.error("You must specify a username")

    if domains[options.domain]['password'] is not None:
        domain_password = domains[options.domain]['password']
    else:
        domain_password = getpass.getpass("Enter password for %s: " % domains[options.domain]['email'])

    service = gdata.apps.service.AppsService(email=domains[options.domain]['email'], domain=options.domain, password=domain_password)
    service.ProgrammaticLogin()
    
    result = service.RetrieveUser(username)

    print(result.login)

if __name__ == "__main__":
    main()

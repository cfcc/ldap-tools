#!/usr/bin/env python
"""Validate that advisee groups in Active Directory should be there.

Removes any groups that would or should be empty.

"""

import logging
from optparse import OptionParser
import os
import sys
import site

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')

#PRODUCTION PATH#
# site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
#TESTING PATH#
site.addsitedir("src")

import cfccldap
from cfccldap import config

CONFIG_FILENAME = "etc/clg.conf"
DEBUG_LIMIT = 5

LOG_PATH = "log"
LOG_FILENAME = "verify_advisee_group"


def main():
    parser = OptionParser()

    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file")
    parser.add_option("-D", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment (default='prod')")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")

    (options, args) = parser.parse_args()

    if not config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = config.get(options.environment)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    global_config = config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    if not options.quiet:
        print("Connecting to the source environment: %s..." % options.environment)

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'],
                        my_config['db_password'],
                        my_config['ldap_password'])

    cfccldap.util.setDefaultDomain(my_config['domain'],
                                   my_config['user_prefix'],
                                   my_config['group_prefix'])

    search_base = config.baseDn(my_config, 'group_prefix')
    ldap_filter = '(cn=*_advisees)'
    logger1.info('Selecting groups from %s', search_base)

    group_records = cfccldap.util.selectAllPaged(search_base, ldap_filter)

    for group_rec in group_records:
        if group_rec is not None:
            print(group_rec.get_attr_values('cn'))
            # FIXME: need to compare group membership to what Colleague thinks it should be


if __name__ == '__main__':
    main()

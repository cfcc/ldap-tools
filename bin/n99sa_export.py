#!/usr/bin/env python

"""Export records from the N99.STU.SEC.ATTEND file that are less than 3 years old and have a L code

The file in Colleague records all the attendance, but we only need the last day for this report.  Once that is exported
to a tabbed-separated file, it can be picked up by Kettle and loaded into a SQL database.

Kettle Job: upload_student_sec_attend
Kettle Transform: ImportStudentSecAttend

Informer has a mapping for the colleague_data.n99_stu_sec_attend table.

"""

import datetime
import logging
import site
from optparse import OptionParser
import os.path
from pprint import pprint as pp
import sys

site_packages = os.path.join('lib', 'python' + sys.version[:3], 'site-packages')
#PRODUCTION PATH#
# site.addsitedir(os.path.join('/', 'opt', 'ldap-tools', site_packages))
#TESTING PATH#
site.addsitedir("src")

import cfccldap
from cfccldap import config
from intercall import AND

CONFIG_FILENAME = "etc/clg.conf"

LOG_PATH = "log"
LOG_FILENAME = "n99sa_export.log"


def main():
    """Parse the command line options"""

    parser = OptionParser(usage="n99sa_export.py EXPORT_FILENAME")
    parser.add_option("-c", "--config", dest="config_file",
                      default=CONFIG_FILENAME, metavar="PATH",
                      help="Specify the full path to a configuration file (default=%s)" % CONFIG_FILENAME)
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      default=False, help="Display debugging messages")
    parser.add_option("-E", "--environment", dest="environment",
                      default="prod", metavar="NAME",
                      help="Specify the environment (default='prod')")
    parser.add_option("-l", "--log", dest="log",
                      action="store_true", default=False,
                      help="Create a log file of the records processed")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")

    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error("You must specify an export filename")

    if not config.loadConfiguration(options.config_file):
        parser.error("Failed to load configuration file: %s" % options.config_file)

    try:
        my_config = config.get(options.environment)
    except KeyError as ex:
        my_config = None
        parser.error(str(ex))

    global_config = config.get('global')
    if 'log' in global_config:
        log_fn = os.path.join(global_config['log'], LOG_FILENAME)
    else:
        log_fn = os.path.join(LOG_PATH, LOG_FILENAME)

    cfccldap.initLogging(log_fn,
                         full_logging=options.log,
                         debug=options.debug)
    logger1 = logging.getLogger('main')

    if not options.quiet:
        print("Connecting to the source environment: %s..." % options.environment)

    cfccldap.connectAll(my_config['db_url'],
                        my_config['ldap_url'],
                        my_config['ldap_user'],
                        my_config['db_password'],
                        my_config['ldap_password'])

    stu_sec_attend = cfccldap.stu_sec_attend

    years = 3
    days_per_year = 365.24
    three_years_ago = datetime.date.today() - datetime.timedelta(days=(years*days_per_year))

    if not options.quiet:
        print("Selecting records since %s..." % three_years_ago.strftime("%Y-%m-%d"))
    result = stu_sec_attend.select(AND(stu_sec_attend.n99sa_attend_type.eq(['L', 'EL', 'TL']),
                                       stu_sec_attend.n99sa_attend_date.ge(three_years_ago)))
    field_names = ["n99sa_id",
                   "n99sa_student_id",
                   "n99sa_course_section",
                   "n99sa_course_sec_meeting",
                   "n99sa_student_course_sec",
                   "n99sa_attend_type",
                   "n99sa_attend_date",
                   "n99sa_attend_hours",
                   "n99sa_attend_chngd_date",
                   "n99sa_attend_chngd_value"]

    if not options.quiet:
        print("Writing results to %s..." % args[0])
    cnt = 0
    with open(args[0], 'wb') as fd_out:
        fd_out.write(b"\t".join([f.encode() for f in field_names]) + b"\n")
        for rec in result:
            try:
                if len(rec.n99sa_attend_chngd_date) > 0:
                    chngd_date = rec.n99sa_attend_chngd_date[-1]
                    chngd_value = rec.n99sa_attend_chngd_value[-1]
                else:
                    chngd_date = ""
                    chngd_value = ""
                s_out = "\t".join([rec.record_id,
                                   rec.n99sa_student_id,
                                   rec.n99sa_course_section,
                                   rec.n99sa_course_sec_meeting,
                                   rec.n99sa_student_course_sec,
                                   rec.n99sa_attend_type,
                                   rec.n99sa_attend_date,
                                   rec.n99sa_attend_hours,
                                   chngd_date,
                                   chngd_value])
                fd_out.write(s_out.encode() + b"\n")
                cnt += 1
            except Exception as ex:
                if not options.quiet:
                    print("[ERROR] failed to write record, %s: %s" % (rec.record_id, ex))

    if not options.quiet:
        print("Wrote %d records to the extract file." % cnt)
    cfccldap.util.close()


if __name__ == "__main__":
    main()

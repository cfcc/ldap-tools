#!/bin/bash
# This script allows us to use the virtual environment without having to do extra steps in the crontab.
#
cd /opt/ldap-tools || exit 1
venv/bin/python3 bin/create-guestaccount.py "$@"

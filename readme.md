### Installation

#### OpenCSW

Use `/opt/csw/bin/pkgutil -i` to install the following packages:

 1. python27
 1. py_virtualenv
 1. py_ldap 
 1. py_crypto

Create a virtual environment for Python:

    /opt/csw/bin/virtualenv -p /opt/csw/bin/python2.7 --system-site-packages ~/src/venv-ldap-tools

Activate the virtual environment and install one more package:

    pip install pyyaml

#### Linux

Install development files (example for CentOS):

    sudo yum install python3-devel openldap-devel

Or for Ubuntu:

    sudo apt install gcc

    sudo apt install python3-dev libsasl2-dev libldap2-dev libssl-dev

Or for Debian

    sudo apt install python3-dev freetds-dev libldap2-dev libsasl2-dev
    sudo apt-get install default-libmysqlclient-dev build-essential

Create the destination directory:

    mkdir /opt/ldap-tools
    cd /opt/ldap-tools

Create a virtual environment and activate it:

    python3 -m venv venv
    source venv/bin/activate

Install the required Python packages with Pip (Note that the python-intercall package comes from the local Nexus PyPI
repository on vault.ad.cfcc.edu):

    pip install -r requirements.txt

Then from the source directory install the scripts.

    python setup.py install --prefix=/opt/ldap-tools

The scripts in /opt/ldap-tools/bin will be set to the correct Python executable, so you can then run them without first
activating the virtual environment.

### Configuration

Create the clg.conf file in the local `etc` subdirectory using the clg.conf.example file.

import intercall
import re

RE_WORD = re.compile("[A-Z][a-z]+")

role_from_office_code = {
    'AR-CR': ['AR', 'CS'],
    'Administrator': [],
    'Admissions': ['AD'],
    # These are now assigned through custom LDAP groups in X2LD
    #    'Budget': ['IT'],
    #    'Budget Manager': ['FI'],
    'Bookstore': ['BK'],
    'Average User': ['CN', 'FM', 'PC'],
    'CampusCruiser': [],
    'Continuing Education': ['BS', 'CE'],
    'Equipment and Inventory': ['IN'],
    'Faculty': ['AF',
                'DI',
                'DL',
                'HC',
                'SW'],
    'Finance': ['AP', 'FI', 'SR'],
    'Financial Aid': ['FA'],
    # IT has now been made a group of groups in AD
    #    'IT': ['IT'],
    'Institution Core': [
        'AF',
        'AP',
        'AR',
        'AU',
        'BS',
        'BK',
        'CE',
        'CN',
        'CS',
        'FA',
        'FI',
        'FM',
        'HC',
        'HR',
        'IR',
        'IS',
        'IT',
        'LR',
        'PA',
        'PU',
        'RG',
        'SR',
        'TS'
        ],
    'Institutional Effectiveness': ['FN', 'IR', 'PR', 'RC'],
    'Instructional Operation': [
        'AF',
        'DI',
        'DL',
        'HC',
        'IS',
        'LR',
        'SA',
        'SD',
        'SW'
        ],
    'Limited User': [],
    'Payroll': ['PA'],
    'Personnel': ['HR'],
    'Purchasing': ['PU'],
    'Registrar': ['RG', 'TS'],
    }


def role(office_codes, for_ldap=False):
    """Take a list of office codes and return a list of roles.

    The Average User role is returned as a default.

    for_ldap = Now that the groups can be maintained in LDAP, I'm just
    converting the descriptions to DNs if this flag is true.

    """
    if not for_ldap:
        my_roles = {'Average User': True}
    else:
        my_roles = dict()
    for (role, codes) in list(role_from_office_code.items()):
        for my_code in office_codes:
            if my_code in codes:
                if for_ldap:
                    role = "cn=%s,ou=groups,dc=cfcc,dc=edu" % role
                my_roles[role] = True
    return my_roles


def meta(cls_name, sep="."):
    """Generate a new UniFile class at run-time and return it.

    cls_name - the class name, must be CamelCase so the function can
               convert it to a unidata filename following the
               convention that each word will be uppercase separated
               by the character in the "sep" parameter.  For example:
               StudentAcadCred becomes STUDENT.ACAD.CRED after
               conversion.

    sep - the word separator to use when converting the class name to
          a unidata file name (default is the period (.) but sometimes
          we need to use underscore (_) instead).
    
    """
    # in case I need the reverse
    #cls_name = "".join([w.capitalize() for w in unidata_filename.split(sep)])

    unidata_filename = sep.join([w.upper() for w in RE_WORD.findall(cls_name)])
    
    class cls(intercall.UniFile):
        ic_filename = unidata_filename
        from_database = True

    cls.__name__ = cls_name
    return cls


class User(intercall.UniFile):
    ic_filename = "INF_USER"
    employee_id = intercall.StringCol(0, length=10)
    first_name = intercall.StringCol(1, length=15)
    last_name = intercall.StringCol(2, length=20)
    active = intercall.StringCol(61, length=1)
    last_mod_date = intercall.DateCol(69)
    email = intercall.StringCol(134, length=35)
    username = intercall.StringCol(157, length=20)
    roles = intercall.ForeignKey(166, 'Role', mv=True)


class View(intercall.UniFile):
    ic_filename = "INF_VIEW"
    name = intercall.StringCol(1)
    data_type = intercall.StringCol(2)
    userid = intercall.ForeignKey(3, 'User')
    view_type = intercall.StringCol(4)
    description = intercall.StringCol(5)
    allowed_users = intercall.StringCol(6)
    role_groups = intercall.StringCol(7, mv=True)
    arguments = intercall.StringCol(8)
    cols = intercall.StringCol(9, mv=True)
    col_disp = intercall.StringCol(10)
    col_total = intercall.StringCol(11)
    sort_cols = intercall.StringCol(12)
    sort_dir = intercall.StringCol(13)
    group_cols = intercall.StringCol(14)
    group_dir = intercall.StringCol(15)
    stmt = intercall.StringCol(16)
    create_date = intercall.DateCol(31)
    create_time = intercall.StringCol(32, conv="MTH")
    create_user = intercall.ForeignKey(33, 'User')
    mod_date = intercall.DateCol(34)
    mod_time = intercall.StringCol(35, conv="MTH")
    mod_user = intercall.ForeignKey(36, 'User')
    comments = intercall.StringCol(37)
    category = intercall.ForeignKey(38, 'Category', mv=True)
    category_id = intercall.StringCol(38, mv=True)
    locked_by = intercall.StringCol(43)


class Category(intercall.UniFile):
    ic_filename = "INF_CAT"
    description = intercall.StringCol(1, len=10, control='C')
    parent = intercall.StringCol(2)
    children = intercall.StringCol(3)
    category_type = intercall.StringCol(4)
    long_description = intercall.StringCol(4)


class Role(intercall.UniFile):
    ic_filename = "INF_ROLE"
    from_database = True
    long_desc = intercall.StringCol(1, len=30, dbname="DESCRIPTION")


## The following are all Colleague files ##


class AcadPrograms(intercall.UniFile):
    ic_filename = 'ACAD.PROGRAMS'
    desc = intercall.StringCol(loc=1, len=50, dbname='ACPG.DESC')
    title = intercall.StringCol(loc=2, len=60, dbname='ACPG.TITLE')
    status = intercall.StringCol(loc=11, len=20, dbname='ACPG.STATUS', mv=True)
    status_date = intercall.DateCol(loc=12, len=8, dbname='ACPG.STATUS.DATE', mv=True)
    acpg_depts = intercall.StringCol(loc=17, len=25, dbname='ACPG.DEPTS', mv=True)


class Address(intercall.UniFile):
    ic_filename = 'ADDRESS'
    zip = intercall.StringCol(loc=1, len=10, dbname='ZIP')
    state = intercall.StringCol(loc=2, len=2, dbname='STATE')
    city = intercall.StringCol(loc=3, len=25, dbname='CITY')
    address_lines = intercall.StringCol(loc=5, len=30, dbname='ADDRESS.LINES', mv=True)


class Applications(intercall.UniFile):
    ic_filename = 'APPLICATIONS'
    applicant = intercall.StringCol(loc=1, dbname='APPL.APPLICANT')
    status = intercall.StringCol(loc=5, dbname='APPL.STATUS')
    status_date = intercall.StringCol(loc=5, dbname='APPL.STATUS.DATE')
    start_term = intercall.StringCol(loc=9, dbname='APPL.START.TERM')


class Applicants(intercall.UniFile):
    ic_filename = 'APPLICANTS'
    app_applications = intercall.StringCol(loc=18, dbname='APP.APPLICATIONS', mv=True)
    
    x810_app_stu_terms = intercall.ComputedCol("X810.APP.STU.TERMS", len=35, mv=True)
    org_entity = intercall.ForeignKey(0, 'OrgEntity')
    person_pin = intercall.ForeignKey(0, 'PersonPin')
    applications = intercall.ForeignKey(18, 'Applications', mv=True)


class Buildings(intercall.UniFile):
    ic_filename = 'BUILDINGS'
    from_database = True


class CampusOrgs(intercall.UniFile):
    ic_filename = 'CAMPUS.ORGS'
    from_database = True


class CoreValcodes(intercall.UniFile):
    ic_filename = "CORE.VALCODES"
    code = intercall.StringCol(loc=1, len=5, mv=True, immutable=True)
    desc = intercall.StringCol(loc=2, len=25, mv=True, immutable=True)


class CourseSections(intercall.UniFile):
    ic_filename = "COURSE.SECTIONS"
    from_database = True


class CourseSecFaculty(intercall.UniFile):
    ic_filename = "COURSE.SEC.FACULTY"
    from_database = True


class Depts(intercall.UniFile):
    ic_filename = 'DEPTS'
    desc = intercall.StringCol(loc=1, len=30, dbname='DEPTS.DESC')
    division = intercall.StringCol(loc=2, len=35, dbname='DEPTS.DIVISION')
    school = intercall.StringCol(loc=3, len=35, dbname='DEPTS.SCHOOL')
    status = intercall.StringCol(loc=4, len=8, dbname='DEPTS.ACTIVE.FLAG')
    depts_head_id = intercall.StringCol(loc=30, len=40, dbname='DEPTS.HEAD.ID')


class Divisions(intercall.UniFile):
    ic_filename = 'DIVISIONS'
    desc = intercall.StringCol(loc=1, len=35, dbname='DIV.DESC')
    depts = intercall.StringCol(loc=2, len=35, mv=True, dbname='DIV.DEPTS')
    locations = intercall.StringCol(loc=3, len=35, mv=True, dbname='DIV.LOCATIONS')
    head_id = intercall.StringCol(loc=4, dbname="DIV.HEAD")
    school = intercall.StringCol(loc=5, len=35, dbname='DIV.SCHOOL')

    head = intercall.ForeignKey(4, 'Person')


class Faculty(intercall.UniFile):
    ic_filename = "FACULTY"
    from_database = True
    org_entity = intercall.ForeignKey(0, 'OrgEntity')
    person_pin = intercall.ForeignKey(0, 'PersonPin')


class Hrper(intercall.UniFile):
    ic_filename = "HRPER"
    all_statuses = intercall.StringCol(loc=1, len=10, dbname="ALL.STATUSES", mv=True)
    hrp_effect_employ_date = intercall.DateCol(loc=26, dbname="HRP.EFFECT.EMPLOY.DATE")
    hrp_pri_campus_location = intercall.StringCol(loc=41, len=5, dbname="HRP.PRI.CAMPUS.LOCATION")
    hrp_pri_campus_building = intercall.StringCol(loc=42, len=20, dbname="HRP.PRI.CAMPUS.BUILDING")
    hrp_pri_campus_office = intercall.StringCol(loc=43, len=6, dbname="HRP.PRI.CAMPUS.OFFICE")
    hrp_pri_campus_extension = intercall.StringCol(loc=44, len=8, dbname="HRP.PRI.CAMPUS.EXTENSION")
    hrp_pri_campus_fax = intercall.StringCol(loc=45, len=8, dbname="HRP.PRI.CAMPUS.FAX")

    hrp_oth_campus_location = intercall.StringCol(loc=46, len=5, dbname="HRP.OTH.CAMPUS.LOCATION", mv=True)
    hrp_oth_campus_building = intercall.StringCol(loc=47, len=20, dbname="HRP.OTH.CAMPUS.BUILDING", mv=True)
    hrp_oth_campus_office = intercall.StringCol(loc=48, len=6, dbname="HRP.OTH.CAMPUS.OFFICE", mv=True)
    hrp_oth_campus_extension = intercall.StringCol(loc=49, len=8, dbname="HRP.OTH.CAMPUS.EXTENSION", mv=True)
    hrp_oth_campus_fax = intercall.StringCol(loc=50, len=8, dbname="HRP.OTH.CAMPUS.FAX", mv=True)

    hrp_pri_dept = intercall.ComputedCol("HRP.PRI.DEPT", len=5)
    hrp_pri_pos = intercall.ComputedCol("HRP.PRI.POS", len=14)
    # hrp_active_status = intercall.SubroutineCol("CC.GET.ACTIVE.PERSTAT", 0, "")

    org_entity = intercall.ForeignKey(0, 'OrgEntity')
    person_pin = intercall.ForeignKey(0, 'PersonPin')


class Locations(intercall.UniFile):
    ic_filename = 'LOCATIONS'
    from_database = True


class StuSecAttend(intercall.UniFile):
    ic_filename = "N99.STU.SEC.ATTEND"
    n99sa_student_id = intercall.StringCol(loc=1, len=19)
    n99sa_course_section = intercall.StringCol(loc=2, len=19)
    n99sa_course_sec_meeting = intercall.StringCol(loc=3, len=19)
    n99sa_student_course_sec = intercall.StringCol(loc=4, len=19)
    n99sa_attend_type = intercall.StringCol(loc=5, len=2)
    n99sa_attend_date = intercall.DateCol(loc=6)
    n99sa_attend_hours = intercall.NumCol(loc=7)
    n99sa_attend_chngd_date = intercall.DateCol(loc=8, mv=True)
    n99sa_attend_chngd_value = intercall.StringCol(loc=9, len=2, mv=True)


class OrgEntity(intercall.UniFile):
    ic_filename = "ORG.ENTITY"
    oe_last_name = intercall.StringCol(3, len=25, encoding='latin-1')
    oe_first_name = intercall.StringCol(4, len=15, encoding='latin-1')
    oe_org_entity_env = intercall.StringCol(14)
    oee_opers = intercall.ComputedCol("OEE.OPERS", len=20)
    fk_org_entity_env = intercall.ForeignKey(14, "OrgEntityEnv")

    fk_person = intercall.ForeignKey(0, "Person")


class OrgEntityEnv(intercall.UniFile):
    ic_filename = "ORG.ENTITY.ENV"
    oee_username = intercall.StringCol(17, len=20)
    oee_opers = intercall.StringCol(30, len=20)


class OrgLdapGroups(intercall.UniFile):
    ic_filename = "ORG.LDAP.GROUPS"
    from_database = True
    ut_prcs_ctl = intercall.ForeignKey(23, "UtPrcsCtl")
    ut_prcs_gen = intercall.ForeignKey(23, "UtPrcsGen")


class Perpos(intercall.UniFile):
    ic_filename = "PERPOS"

    hrp_id = intercall.StringCol(loc=1, dbname="PERPOS.HRP.ID")
    position_id = intercall.StringCol(loc=2, dbname="PERPOS.POSITION.ID")
    start_date = intercall.DateCol(loc=6, dbname="PERPOS.START.DATE")
    end_date = intercall.DateCol(loc=7, dbname="PERPOS.END.DATE")
    supervisor_hrp_id = intercall.StringCol(loc=21, dbname="PERPOS.SUPERVISOR.HRP.ID")

    pos_rank = intercall.ComputedCol("POS.RANK", len=34)
    perpos_hrly_or_slry = intercall.ComputedCol("PERPOS.HRLY.OR.SLRY")

    fk_hrper = intercall.ForeignKey(1, 'Hrper')
    fk_position = intercall.ForeignKey(2, 'Position')


class Perposwg(intercall.UniFile):
    ic_filename = "PERPOSWG"
    hrp_id = intercall.StringCol(loc=1, dbname="PPWG.HRP.ID")
    position_id = intercall.StringCol(loc=2, dbname="PPWG.POSITION.ID")
    perpos_id = intercall.StringCol(loc=3, dbname="PPWG.PERPOS.ID")
    start_date = intercall.DateCol(loc=7, dbname="PPWG.START.DATE")
    end_date = intercall.DateCol(loc=8, dbname="PPWG.END.DATE")
    earn_type = intercall.StringCol(loc=17, dbname="PPWG.BASE.ET")


class Perstat(intercall.UniFile):
    ic_filename = "PERSTAT"
    from_database = True
    hrp_id = intercall.StringCol(loc=1, dbname="PERSTAT.HRP.ID")
    primary_pos_id = intercall.StringCol(loc=2, dbname="PERSTAT.PRIMARY.POS.ID")
    end_date = intercall.DateCol(loc=3, dbname="PERSTAT.END.DATE")
    end_date_reason = intercall.StringCol(loc=4, dbname="PERSTAT.END.REASON")
    status = intercall.StringCol(loc=5, dbname="PERSTAT.STATUS")


class Person(intercall.UniFile):
    ic_filename = "PERSON"
    last_name = intercall.StringCol(loc=1, len=25, immutable=True)
    first_name = intercall.StringCol(loc=3, len=15, immutable=True)
    middle_name = intercall.StringCol(loc=4, len=15, immutable=True)
    preferred_address = intercall.StringCol(loc=6, len=10, immutable=True)
    birth_date = intercall.DateCol(loc=14, immutable=True)
    nickname = intercall.StringCol(loc=22, immutable=True)
    preferred_name = intercall.StringCol(loc=15, immutable=True)
    person_user4 = intercall.StringCol(loc=98, immutable=True)
    person_email_types = intercall.StringCol(loc=67, len=3, mv=True, immutable=True)
    person_email_addresses = intercall.StringCol(loc=68, len=50, mv=True, immutable=True)
    person_preferred_email = intercall.StringCol(loc=219, len=1, mv=True, immutable=True)
    name_history_last_name = intercall.StringCol(loc=44, immutable=True)
    name_history_first_name = intercall.StringCol(loc=45, immutable=True)
    name_history_middle_name = intercall.StringCol(loc=46, immutable=True)
    where_used = intercall.StringCol(loc=31, mv=True, immutable=True)

    fk_pref_addr = intercall.ForeignKey(6, 'Address')
    org_entity = intercall.ForeignKey(0, 'OrgEntity')
    person_pin = intercall.ForeignKey(0, 'PersonPin')


class PersonPin(intercall.UniFile):
    ic_filename = "PERSON.PIN"
    person_id = intercall.StringCol(loc=0, len=7, dbname='@ID')
    user_id = intercall.StringCol(loc=8, len=60, dbname="PERSON.PIN.USER.ID", control='X')


class Position(intercall.UniFile):
    ic_filename = "POSITION"
    title = intercall.StringCol(loc=1, len=60, dbname="POS.TITLE")
    authorized_date = intercall.DateCol(loc=2, dbname="POS.AUTHORIZED.DATE")
    end_date = intercall.DateCol(loc=3, dbname="POS.END.DATE")
    pos_class = intercall.StringCol(loc=4, len=30)
    pos_rank = intercall.StringCol(loc=5, len=34)
    short_title = intercall.StringCol(loc=10, len=28, dbname="POS.SHORT.TITLE")
    in_use = intercall.StringCol(loc=18, len=14, dbname="POSITION.IN.USE")
    supervisor_pos_id = intercall.StringCol(loc=34, len=14, dbname="POS.SUPERVISOR.POS.ID")


class Schools(intercall.UniFile):
    ic_filename = 'SCHOOLS'
    desc = intercall.StringCol(loc=1, len=30, dbname='SCHOOLS.DESC')
    head_id = intercall.StringCol(loc=2, len=40, dbname='SCHOOLS.HEAD.ID')
    
    head = intercall.ForeignKey(2, 'Person')
    

class Staff(intercall.UniFile):
    ic_filename = "STAFF"
    from_database = True
    person = intercall.ForeignKey(0, 'Person')
    org_entity = intercall.ForeignKey(0, 'OrgEntity')
    person_pin = intercall.ForeignKey(0, 'PersonPin')


class Students(intercall.UniFile):
    ic_filename = "STUDENTS"
    stu_acad_levels = intercall.StringCol(loc=19, len=35, mv=True, dbname='STU.ACAD.LEVELS')
    stu_acad_programs = intercall.StringCol(loc=22, len=20, mv=True, dbname='STU.ACAD.PROGRAMS')
    stu_terms = intercall.StringCol(loc=23, len=35, mv=True, dbname='STU.TERMS')
    stu_active_programs = intercall.ComputedCol('STU.ACTIVE.PROGRAMS', len=20, mv=True)
    person = intercall.ForeignKey(0, 'Person')
    org_entity = intercall.ForeignKey(0, 'OrgEntity')
    person_pin = intercall.ForeignKey(0, 'PersonPin')


class StudentAdvisement(intercall.UniFile):
    ic_filename = "STUDENT.ADVISEMENT"
    student = intercall.StringCol(loc=1, dbname="STAD.STUDENT")
    faculty = intercall.StringCol(loc=2, dbname="STAD.FACULTY")
    start_date = intercall.DateCol(loc=3, dbname="STAD.START.DATE")
    end_date = intercall.DateCol(loc=4, dbname="STAD.END.DATE")

    fk_students = intercall.ForeignKey(1, 'Students')
    fk_person = intercall.ForeignKey(2, 'Person')
    fk_x810_id_card = intercall.ForeignKey(1, 'X810IdCard')


class StudentCourseSec(intercall.UniFile):
    ic_filename = "STUDENT.COURSE.SEC"
    from_database = True


class Subjects(intercall.UniFile):
    ic_filename = "SUBJECTS"
    from_database = True


class Terms(intercall.UniFile):
    ic_filename = "TERMS"
    from_database = True


class UtSeclass(intercall.UniFile):
    ic_filename = 'UT.SECLASS'
    sys_class_description = intercall.StringCol(loc=1, len=70, dbname='SYS.CLASS.DESCRIPTION')


class UtOpers(intercall.UniFile):
    ic_filename = 'UT.OPERS'
    sys_user_classes = intercall.StringCol(loc=4, len=20, mv=True, dbname='SYS.USER.CLASSES')
    sys_person_id = intercall.StringCol(loc=15, len=10, dbname='SYS.PERSON.ID')

    org_entity = intercall.ForeignKey(0, 'OrgEntity')
    person_pin = intercall.ForeignKey(15, 'PersonPin')


class PrcsCtl(intercall.UniFile):
    """Process Control file for each application.

    This file is APPL based, so you must specify the application at
    runtime.

    PrcsCtl(db_sess, "CORE") -> UniFile Object

    """
    from_database = True

    def __init__(self, session, appl):
        self.ic_filename = "%s.PRCS.CTL" % appl
        super(PrcsCtl, self).__init__(session)


class PrcsGen(intercall.UniFile):
    """Process Gen file for each application.

    This file is APPL based, so you must specify the application at
    runtime.

    PrcsGen(db_sess, "CORE") -> UniFile Object

    """
    from_database = True
    
    def __init__(self, session, appl):
        self.ic_filename = "%s.PRCS.GEN" % appl
        super(PrcsGen, self).__init__(session)


class Valcodes(intercall.UniFile):
    """
    This can represent any of the valcode tables in Colleague.  The
    actual table must be specified at runtime.

    Valcodes(db_sess, "CORE.VALCODES") -> UniFile Object
    
    """
    from_database = True

    def __init__(self, session, filename):
        self.ic_filename = filename
        super(Valcodes, self).__init__(session)


class X810IdCard(intercall.UniFile):
    ic_filename = 'X810.ID.CARD'
    last = intercall.StringCol(loc=1, len=25, dbname='X810.ID.LAST')
    first = intercall.StringCol(loc=2, len=25, dbname='X810.ID.FIRST')
    type = intercall.StringCol(loc=13, len=8, dbname='X810.ID.TYPE')
    dept = intercall.StringCol(loc=14, len=10, dbname='X810.ID.DEPT')
    active_date = intercall.DateCol(loc=15, dbname='X810.ID.ACTIVE.DATE')
    expire_date = intercall.DateCol(loc=16, dbname='X810.ID.EXPIRE.DATE')
    ldap_prog = intercall.StringCol(loc=26, dbname='X810.ID.LDAP.PROG', mv=True)
    ldap_pos = intercall.StringCol(loc=27, dbname='X810.ID.LDAP.POS', mv=True)
    ldap_dept = intercall.StringCol(loc=28, dbname='X810.ID.LDAP.DEPT', mv=True)
    ldap_div = intercall.StringCol(loc=29, dbname='X810.ID.LDAP.DIV', mv=True)
    ldap_school = intercall.StringCol(loc=30, dbname='X810.ID.LDAP.SCHOOL', mv=True)
    ldap_groups = intercall.StringCol(loc=31, dbname='X810.ID.LDAP.GROUPS', mv=True)
    location = intercall.StringCol(loc=40, dbname='X810.ID.LOCATION')
    pos_status = intercall.StringCol(loc=41, dbname='X810.ID.POS.STATUS')

    org_entity = intercall.ForeignKey(0, 'OrgEntity')
    person_pin = intercall.ForeignKey(0, 'PersonPin')

__all__ = ['User', 'View', 'Category', 'Role', 'Person', 'PersonPin', 'Staff', 'role']

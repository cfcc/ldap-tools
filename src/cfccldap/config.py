"""Configuration Module"""

from Crypto.Cipher import AES
import base64
from pathlib import Path
import shutil
import yaml

APP_SECRET = b'2277f796ace699c6'

ldap_user = None
#
# IMPORTANT, do not change these default values, instead override them in your local configuration file
#
__config = {
    'global': {
        'remove_max_percentage': 1,
        'rules': None,
        'rules_uri': None
    },
    'prod': {
        'label': 'Production AD',
        'db_url': None,
        'ldap_url': None,
        'ldap_user': None,
        'domain': None,
        'user_prefix': None,
        'group_prefix': None,
        'key_attr': None,
        },
    'test': {
        'label': 'Test AD',
        'db_url': None,
        'ldap_url': None,
        'ldap_user': None,
        'domain': None,
        'user_prefix': None,
        'group_prefix': None,
        'key_attr': None,
        },
    'guests': {
        'mail_domain': "",
        'server_domain': "",
        'guest_ou': ['guest'],
        # the algorithm can be 'random' or 'wordlist'
        'password_algorithm': 'random',
        # the next option is only for random passwords
        'password_length': 10,
        # the next 4 options are for wordlist passwords
        'password_word_count': 3,
        'password_min_len': 3,
        'password_max_len': 5,
        'password_word_file': "/usr/share/dict/words",
        'bad_words_file': "etc/bad-words.txt",
        # list of the email addresses of the system admins
        'notification': [],
        'db_uri': "sqlite:/:memory:"
        }
    }


def get(which='prod'):
    """Return the relevant configuration settings as a dictionary

    :param which: specify the environment as a string (default=prod)

    """

    if which in __config:
        return __config[which]
    else:
        raise KeyError("Valid keys are: " + ", ".join(sorted(__config.keys())))


def getEnv():
    """Return a copy of the key list"""
    return sorted(__config.keys())[:]


def loadConfiguration(config_fn, decrypt_passwords=True):
    """return True if when config values are loaded from the file specified.

    Default configuration settings will be overridden by what is in
    the file and any passwords will be decrypted.

    Extra environments will be created from scratch by the way the
    YAML import works, but this will be noted with a warning message
    in the log.

    The following is a sample configuration file.  

    %YAML 1.2
    ---
    log: coll_ldap_groups.log
    test1:
      db_url: unidata://datatel@shamash/datatel2/coll18/test1/apphome
      db_password: '{AES}dB4tH77fWjIoEpkETZtn8g=='
      ldap_url: ldap://garnet.dtadmin.edu/
      ldap_user: uid=dmiadmin,ou=admins,dc=cfcc,dc=edu
      ldap_password: '{AES}0C/sGpf0nKn7Ceasf2qkNw=='
    prod:
      db_url: unidata://datatel@shamash/datatel/coll18/production/apphome
      db_password: '{AES}u/x9IyGl0a68cgchAIRbbw=='
      ldap_url: ldaps://enlil.cfcc.edu/
      ldap_user: uid=dmiadmin,ou=admins,dc=cfcc,dc=edu
      ldap_password: '{AES}0C/sGpf0nKn7Ceasf2qkNw=='
    ...

    Even though the passwords are encrypted, the file should not be
    world readable.
    
    """
    success = False
    try:
        fd = open(config_fn)
        config = yaml.safe_load(fd)
        fd.close()
        # if we have a file loaded, then decrypt passwords and merge with
        # module configuration dictionary
        for key in list(config.keys()):
            if isinstance(config[key], dict):
                for k in ('ldap_password', 'db_password'):
                    if k in config[key]:
                        if config[key][k] is not None:
                            # print(f"config[{key}][{k}] =", config[key][k])
                            if isinstance(config[key][k], list):
                                if config[key][k][0] == b'{AES}':
                                    # config[key][k] = getPassword(config[key][k], APP_SECRET)
                                    if decrypt_passwords:
                                        config[key][k] = get_password_v2(config[key][k], APP_SECRET)
                                        # print(f"config[{key}][{k}] =", config[key][k])
                if key in __config:
                    __config[key].update(config[key])
                else:
                    # print "[WARNING] Adding environment settings for %s" % key
                    __config[key] = config[key].copy()
            else:
                # these are single configuration strings and get added
                # silently to the configuration dictionary as "global"
                # settings
                __config['global'][key] = config[key]
        success = True
    except IOError as val:
        print("Error opening config file '%s' (%s)" % (config_fn, val))
    except UnicodeDecodeError as ex:
        print(f"Error decoding a password: {ex}")
    return success


def save_configuration(config_fn):
    p_orig = Path(config_fn)
    p_bkup = p_orig.with_name(f"{p_orig.name}~")
    shutil.copyfile(p_orig, p_bkup)
    with open(p_orig, 'w') as fd:
        fd.write(yaml.dump(__config))


def encode_password_v2(plaintext, key=APP_SECRET, header='{AES}'):
    """Return a list with a base64 encoded password."""
    cipher = AES.new(base64.b64encode(key), AES.MODE_EAX)
    ciphertext, tag = cipher.encrypt_and_digest(plaintext.encode())
    data_out = [base64.b64encode(d) for d in (cipher.nonce, tag, ciphertext)]
    data_out.insert(0, header.encode('utf-8'))
    return data_out


def get_password_v2(data_in, key):
    """Return plaintext password that was saved earlier."""
    nonce = base64.b64decode(data_in[1])
    tag = base64.b64decode(data_in[2])
    ciphertext = base64.b64decode(data_in[3])

    cipher = AES.new(base64.b64encode(key), AES.MODE_EAX, nonce)
    plaintext = cipher.decrypt_and_verify(ciphertext, tag)
    return plaintext.decode()


def encodePassword(plaintext, key=APP_SECRET, header='{AES}'):
    """Return a base64 encoded string.

    The plaintext is encrypted with the given key and converted to
    base64.  The header is an option string that will be prepended to
    the base64 string before it is returned.
    
    """
    c = AES.new(base64.b64encode(key))
    padding_len = 16 - (len(plaintext) % 16)
    padded_text = plaintext.encode() + b'\x00' * padding_len
    ciphertext = c.encrypt(padded_text)
    bytes_out = header.encode() + base64.b64encode(ciphertext)
    return bytes_out.decode()


def getPassword(encoded_str, key, header='{AES}'):
    """Return the plaintext result of the encoded/encrypted string.

    The header is stripped from the string before base64 decoding.

    """
    ciphertext = base64.b64decode(encoded_str[len(header):].encode())
    c = AES.new(base64.b64encode(key), AES.MODE_EAX)
    plaintext = c.decrypt(ciphertext)
    return plaintext[:plaintext.find(b'\x00')].decode()


def baseDn(my_config, which='user_prefix'):
    """Return a string with the base DN

    which - set to 'user_prefix' (default) or 'group_prefix'
    """
    return my_config[which] + "," + my_config['domain']


def update_password(env_name, svc_name, new_passwd):
    if svc_name == 'd':
        key = 'db_password'
    elif svc_name == 'l':
        key = 'ldap_password'
    else:
        key = None
    __config[env_name][key] = encode_password_v2(new_passwd)

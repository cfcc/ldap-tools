"""LDAP Group for future students (i.e. applicants)

Member records are from the ORG.ENTITY file

 * ORG.ENTITY = WEBSTUDENT
 * STU.TERMS = ""

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE


class FutureStudents(LdapGroup, Plugin):
    def selectRecords(self):
        """
        Records from ORG.ENTITY where:

        * STU.TERMS is empty
        * OEE.OPERS equals WEBSTUDENT

        """
        logger1 = logging.getLogger('main.FutureStudents')
        db = self.getDbSession()
        if self.debug:
            logger1.info("Selecting records from ORG.ENTITY.ENV")
        result = db.execute("SELECT ORG.ENTITY.ENV WITH OEE.OPERS = 'WEBSTUDENT'")
        if self.debug:
            logger1.info("Result = %s", result)
        result = db.execute("SELECT ORG.ENTITY.ENV SAVING UNIQUE OEE.RESOURCE REQUIRE.SELECT")
        if self.debug:
            logger1.info("Result = %s", result)
            logger1.info("Selecting required records from APPLICANTS")
        applicants = unidata_model.Applicants(getDbSession())
        if self.debug:
            logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
            result = applicants.select(applicants.x810_app_stu_terms.eq("") + ' SAMPLE ' + str(SAMPLE_SIZE))
        else:
            result = applicants.select(applicants.x810_app_stu_terms.eq(""))
        return result

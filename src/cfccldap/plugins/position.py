"""LDAP Group Factory for Position groups

All records in POSITION with IN.USE flag that is "Y" are used to build groups.

Member records are from X810.ID.CARD where:

 * LDAP.POSITION is "position code"

"""
import logging
import sys

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE

from intercall import IntercallError


class PositionRules(LdapGroupFactory, Plugin):
    """
    Generate groups based on the HR positions in Colleague.
    """
    def getRules(self):
        """
        Build groups based on their employment position in Colleague.
        """
        pos_rules = []
        position = unidata_model.Position(getDbSession())

        result = position.select(position.in_use.eq('Y'))
        for rec in result:
            try:
                pos_rules.append(PositionGroup(group_name=rec.record_id,
                                               desc=rec.title))
            except IntercallError as e:
                if e.error_code == 30001:
                    print("Record ID %s not found in POSITION" % rec.record_id)
                else:
                    print(str(e))
                    sys.exit(0)
        return pos_rules


class PositionGroup(LdapGroup):
    """
    Create a group with the usernames of employees with this position.
    """
    def selectRecords(self):
        result = None
        logger1 = logging.getLogger('main.PositionGroup')
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        if self.debug:
            logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
            result = x810_id_card.select(x810_id_card.ldap_pos.eq(self.group_name) + ' SAMPLE ' + str(SAMPLE_SIZE))
        else:
            result = x810_id_card.select(x810_id_card.ldap_pos.eq(self.group_name))
        return result

"""LDAP Group Factory for the Divisons

A group is built for every record in DIVISONS

Member records are from X810.ID.CARD where:

 * Ldap Division equals "division ID"

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE
from intercall import IntercallError


class DivisionRules(LdapGroupFactory, Plugin):
    def getRules(self):
        """
        Add groups based on the Division
        """
        div_rules = []
        divisions = unidata_model.Divisions(getDbSession())

        result = divisions.select()
        for rec in result:
            try:
                div_rules.append(DivisionGroup(group_name=rec.record_id, desc=rec.desc))
            except IntercallError as e:
                if e.error_code == 30001:
                    print("Record ID %s not found in DIVISIONS" % rec.record_id)
                else:
                    print(str(e))
        return div_rules
    

class DivisionGroup(LdapGroup):
    """
    Dynamic group based on the IDs from the DIVISIONS file.

    Build a group based on membership in a division from X810.ID.CARD.

    """

    def selectOwners(self):
        """Owner of the division group is the Division Head."""
        owners = []
        divisions = unidata_model.Divisions(getDbSession())
        person = unidata_model.Person(getDbSession())
        div = divisions.get(self.group_name)
        if div.head_id != "":
            rec = person.get(div.head_id)
            if rec is not None:
                owners.append(rec)
        return owners
    
    def selectRecords(self):
        """
        Records from X810.ID.CARD where:

        * Ldap Division equals "division ID"
        
        """
        logger1 = logging.getLogger('main.DivisionGroup')
        logger1.info("Selecting records matching %s from X810.ID.CARD", self.group_name)
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        if self.debug:
            logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
            result = x810_id_card.select(x810_id_card.ldap_div.eq(self.group_name) + ' SAMPLE ' + str(SAMPLE_SIZE))
        else:
            result = x810_id_card.select(x810_id_card.ldap_div.eq(self.group_name))
        return result

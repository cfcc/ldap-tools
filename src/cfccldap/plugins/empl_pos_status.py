"""LDAP groups broken out as FT and PT Faculty (or Staff)

Records must meet the following criteria:

from PERSTAT where:

 * The status matches either FT... or PT...
 * End date is empty or in the future

from PERPOS where:

 * Rank equals "INST" (or "" for Staff)

from X810.ID.CARD where:

 * ID.TYPE equals "EMPLOYEE"
 * ID.DEPT is not "UCW"

"""
import datetime

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE

from intercall import AND, OR, SAVING


class EmployeeByPosStatus(LdapGroup, Plugin):
    """All the current faculty or staff with the given position status code.

    Position status will begin with either 'F' for full time or 'P' for part time.

    The value for rank is based on the group name containing FACULTY

    """

    def __init__(self, *args, **kwds):
        """Initialize the class and set the full-time or part-time codes."""
        self.full_time = False
        if kwds['key'] == 'P':
            self.key = 'PT...'
            self.earn_type = ['PT',  # Part-Time Teaching Pay
                              'CE',  # Con Ed P/T Teaching Pay
                              'RP',  # Regular Part-Time
                              ]
        elif kwds['key'] == 'F':
            self.full_time = True
            self.key = 'FT...'
            self.earn_type = ['TP',  # Teaching Pay
                              'OL',  # Overload
                              'NI',  # Non-Instructional Pay
                              ]
        else:
            raise Exception('EmployeeByPosStatus key must be P or F and not "%s"', kwds['key'])
        if 'FACULTY' in kwds['group_name']:
            self.pos_rank = 'INST'
        else:
            self.pos_rank = ''
        self.card_type = 'EMPLOYEE'
        self._exclude_dept = 'UCW'
        super(EmployeeByPosStatus, self).__init__(**kwds)

    def selectRecords(self):
        # logger1 = logging.getLogger('main.FacultyByPosStatus')
        perpos = unidata_model.Perpos(getDbSession())
        perstat = unidata_model.Perstat(getDbSession())
        perposwg = unidata_model.Perposwg(getDbSession())
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        
        today = datetime.date.today()

        # PERPOS.START.DATE
        # PERPOS.END.DATE

        # I'm selecting full time based on primary position, and part time based on individual contract status
        if self.full_time:
            perstat.selectOnly(
                SAVING(
                    AND(
                        OR(perstat.perstat_end_date.eq(""),
                           perstat.perstat_end_date.gt(today)),
                        perstat.perstat_status.like(self.key)
                    ),
                    perstat.perstat_primary_perpos_id,
                    unique=True
                )
            )
        else:
            perposwg.selectOnly(
                SAVING(
                    AND(
                        OR(
                            perposwg.end_date.eq(""),
                            perposwg.end_date.gt(today)
                        ),
                        perposwg.earn_type.eq(self.earn_type)
                    ),
                    perposwg.perpos_id,
                    unique=True
                )
            )
        # Now select the faculty or staff based on the rank recorded in the position (INST or blank)
        perpos.selectOnly(
            SAVING(
                perpos.pos_rank.eq(self.pos_rank),
                perpos.hrp_id,
                unique=True
            ),
            require_select=True
        )
        if self.debug:
            use_sample = SAMPLE_SIZE
        else:
            use_sample = None
        result = x810_id_card.select(
            AND(
                x810_id_card.type.eq(self.card_type),
                x810_id_card.dept.ne(self._exclude_dept)
            ),
            require_select=True,
            sample=use_sample
        )
        return result

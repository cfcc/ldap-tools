"""LDAP Group Factory for the Schools

A group is built for all the records in SCHOOLS

Member records are from X810.ID.CARD where:

 * LDAP.SCHOOL equals "school code"

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE
from intercall import IntercallError


class SchoolRules(LdapGroupFactory, Plugin):
    def getRules(self):
        """
        Add a group based on the school in Colleague.
        """
        school_rules = []
        schools = unidata_model.Schools(getDbSession())
        result = schools.select()
        for rec in result:
            try:
                school_rules.append(SchoolGroup(group_name=rec.record_id, desc=rec.desc))
            except IntercallError as e:
                if e.error_code == 30001:
                    print("Record ID %s not found in SCHOOLS" % rec.record_id)
                else:
                    raise e
        return school_rules


class SchoolGroup(LdapGroup):
    """
    Dynamic group based on the Schools in Colleague.
    """
    def selectOwners(self):
        """The owner of a school is a Dean"""
        owners = []
        schools = unidata_model.Schools(getDbSession())
        school = schools.get(self.group_name)
        person = unidata_model.Person(getDbSession())
        if school.head_id != "":
            rec = person.get(school.head_id)
            if rec is not None:
                owners.append(rec)
        return owners
    
    def selectRecords(self):
        logger1 = logging.getLogger('main.SchoolGroup')
        result = None
        logger1.info("Selecting records matching %s from X810.ID.CARD", self.group_name)
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        if self.debug:
            logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
            result = x810_id_card.select(x810_id_card.ldap_school.eq(self.group_name) + ' SAMPLE ' + str(SAMPLE_SIZE))
        else:
            result = x810_id_card.select(x810_id_card.ldap_school.eq(self.group_name))
        return result
    

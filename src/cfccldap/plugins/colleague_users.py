"""LDAP group for all Colleague Users

Records from STAFF where:

 * Status is "current"
 * Login ID is not empty

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE

from intercall import AND


class ColleagueUsers(LdapGroup, Plugin):
    """
    Select all current employees with an active, unlocked, login on shamash.

    SELECT STAFF WITH STAFF.STATUS = 'C' AND WITH STAFF.LOGIN.ID NE ''
    """
    def selectRecords(self):
        staff = unidata_model.Staff(getDbSession())
        result = staff.select(AND(staff.staff_status.eq('C'),
                                  staff.staff_login_id.ne("")))
        return result

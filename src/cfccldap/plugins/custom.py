"""LDAP Group Factory for all the custom LDAP groups

The group names are taken from X810.LDAP.GROUPS in CORE.VALCODES

Member records are from X810.ID.CARD where:

 * LDAP.GROUPS equals "custom group ID"

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroupFactory, LdapGroup
from cfccldap.util import SAMPLE_SIZE


class CustomGroupRules(LdapGroupFactory, Plugin):
    """Add group rules based on the X810.LDAP.GROUPS values."""
    def getRules(self):
        """Return a list of custom ldap group objects"""
        x810_rules = []
        core_valcodes = unidata_model.Valcodes(getDbSession(), 'CORE.VALCODES')

        rec = core_valcodes.get('X810.LDAP.GROUPS')
        for i in range(len(rec.val_minimum_input_string)):
            x810_rules.append(CustomLdapGroups(group_name=rec.val_minimum_input_string[i],
                                               desc=rec.val_external_representation[i]))

        return x810_rules


class CustomLdapGroups(LdapGroup):
    """
    Dynamic group based on the the group codes defined in valcode
    table X810.LDAP.GROUPS in CORE.VALCODES.
    
    """
    def selectRecords(self):
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        result = x810_id_card.select(x810_id_card.ldap_groups.eq(self.group_name))
        return result

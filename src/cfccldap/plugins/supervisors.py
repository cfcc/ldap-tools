"""A group that contains everyone who has a supervisor position.

This is a two step process:

 1. Select all the unique supervisor positions POS.SUPERVISOR.POS.ID listed in POSITION for positions marked with POSITION.IN.USE = "Y"

 1. For each of the positions in the previous list SELECT PERPOS WITH PERPOS.POSITION.ID = 'id' and save the HRPER.ID

"""
import datetime
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup

from intercall import AND, OR, SAVING


class Supervisors(LdapGroup, Plugin):

    def selectRecords(self):
        logger1 = logging.getLogger('main.Supervisors')
        perpos = unidata_model.Perpos(getDbSession())
        position = unidata_model.Position(getDbSession())
        x810_id_card = unidata_model.X810IdCard(getDbSession())

        TODAY = datetime.date.today()

        # Limit the supervisors to people who have active person positions under them
        perpos.selectOnly(
            SAVING(
                OR(perpos.end_date.eq(""), perpos.end_date.gt(TODAY)),
                perpos.position_id,
                unique=True
            )
        )
        # Still check to make sure those positions are also marked active
        position.selectOnly(
            SAVING(
                position.in_use.eq('Y'),
                position.supervisor_pos_id,
                unique=True
            ),
            require_select=True
        )
        # re-select the Position file to get the final list of active Supervisor positions
        position_records = position.select(position.in_use.eq('Y'), require_select=True)

        supervisor_hrper_ids = dict()
        supervisor_records = list()
        position_cnt = 0
        supervisor_cnt = 0
        for pos_rec in position_records:
            position_cnt += 1
            result = perpos.select(AND(perpos.position_id.eq(pos_rec.record_id),
                                       OR(perpos.end_date.eq(""),
                                          perpos.end_date.gt(TODAY))))
            # result = perpos.select(perpos.position_id.eq(pos_rec.record_id))
            for rec in result:
                # logger1.info("Found %s in position %s", rec.hrp_id, pos_rec.record_id)
                supervisor_hrper_ids[rec.hrp_id] = 1

        # Some supervisors are specified in the alternate field so select those and add them to the list of supervisors
        perpos_with_alt_supervisors = perpos.select(
            AND(
                OR(perpos.end_date.eq(""), perpos.end_date.gt(TODAY)),
                perpos.supervisor_hrp_id.ne("")
            )
        )
        for perpos_rec in perpos_with_alt_supervisors:
            supervisor_hrper_ids[perpos_rec.supervisor_hrp_id] = 1

        # Now that we have a dict of unique PERSON IDs, we can build the list...
        for hrp_id in sorted(supervisor_hrper_ids.keys()):
            supervisor_records.append(x810_id_card.get(hrp_id))
            supervisor_cnt += 1

        logger1.info("Found %d supervisors out of %d positions checked.", supervisor_cnt, position_cnt)
        return supervisor_records

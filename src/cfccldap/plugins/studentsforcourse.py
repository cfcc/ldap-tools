"""LDAP group for people registered for classes of a given subject (ex: DMS, MAT).

Unique STUDENT IDs STUDENT.COURSE.SEC where

 * COURSE.SECTIONS Term equals 'TERM' and
 * COURSE.SECTIONS Sec Course Subject equals 'SUBJECT_CODE' and
 * COURSE.SECTIONS Current Status equals 'A','N'

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE

from intercall import AND, SAVING


class StudentsForCourse(LdapGroup, Plugin):
    """Build a group for all students taking a given course this semester."""
    def __init__(self, *args, **kwds):
        """Create the class and populate the group name and description.

        Store the term for later.

        Keywords:

         * key - the term from which to select students
         * owners - a list of person IDs for those who moderate the group
         * start_date - an optional date to limit classes selected

        """
        self.term = kwds['key']
        if 'owners' in kwds:
            self.owner_ids = kwds['owners']
        else:
            self.owner_ids = []

        if 'start_date' in kwds:
            self.start_date = kwds['start_date']
        else:
            self.start_date = None

        if 'crs_subject' in kwds:
            self.crs_subject = kwds['crs_subject']
        else:
            self.crs_subject = 'DMS'

        if 'crs_status' in kwds:
            self.crs_status = kwds['crs_status']
        else:
            self.crs_status = ['A', 'N']

        super(StudentsForCourse, self).__init__(**kwds)
        
    def selectOwners(self):
        owners = []
        person = unidata_model.Person(getDbSession())
        for person_id in self.owners:
            rec = person.get(person_id)
            if rec is not None:
                owners.append(rec)
        return owners

    def selectRecords(self):
        logger1 = logging.getLogger('main.StudentsForCourse')
        person = unidata_model.Person(getDbSession())
        students = unidata_model.Students(getDbSession())
        course_sections = unidata_model.CourseSections(getDbSession())
        student_course_sec = unidata_model.StudentCourseSec(getDbSession())
        db = self.getDbSession()

        if self.start_date is not None:
            query = AND(
                course_sections.sec_term.eq(self.term),
                course_sections.sec_subject.eq(self.crs_subject),
                course_sections.sec_current_status.eq(self.crs_status),
                course_sections.sec_start_date.ge(self.start_date)
            )
        else:
            query = AND(
                course_sections.sec_term.eq(self.term),
                course_sections.sec_subject.eq(self.crs_subject),
                course_sections.sec_current_status.eq(self.crs_status)
            )
        # Note: SEC.STUDENTS actually points to STUDENT.COURSE.SEC
        course_sections.selectOnly(SAVING(query, course_sections.sec_students, unique=True))

        # select only the students who haven't dropped the class
        student_course_sec.selectOnly(
            SAVING(student_course_sec.stc_status.ne('X'), student_course_sec.scs_student, unique=True),
            require_select=True
        )

        result = students.select(students.stu_acad_levels.ne(""), require_select=True)
        # Add the faculty "owners" to the group
        for person_id in self.owners:
            result.append(person.get(person_id))

        return result

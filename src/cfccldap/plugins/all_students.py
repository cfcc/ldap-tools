"""LDAP Group for All Students

Member records from STUDENTS where:

 * OEE.OPERS equals WEBSTUDENT

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE


class AllStudents(LdapGroup, Plugin):
    """
    Select all ORG.ENTITY.ENV records that have the opers class WEBSTUDENT.

    SELECT ORG.ENTITY.ENV WITH OEE.OPERS = 'WEBSTUDENT' SAVING UNIQUE OEE.RESOURCE
   
    """
    def __init__(self, *args, **kwds):
        super(AllStudents, self).__init__(**kwds)
        self.defer_connection = True

    def selectRecords(self):
        """
        Records from STUDENTS where:

        * OEE.OPERS is WEBSTUDENT
        
        """
        logger1 = logging.getLogger('main.AllStudents')
        db = self.getDbSession()
        query = "SELECT ORG.ENTITY.ENV WITH OEE.OPERS = 'WEBSTUDENT'"
        if self.debug:
            logger1.info("Selecting %s records from ORG.ENTITY.ENV", SAMPLE_SIZE)
            query +=  ' SAMPLE ' + str(SAMPLE_SIZE)
        result = db.execute(query)
        if self.debug:
            logger1.info("Result = %s", result)
        result = db.execute("SELECT ORG.ENTITY.ENV SAVING UNIQUE OEE.RESOURCE REQUIRE.SELECT")
        if self.debug:
            logger1.info("Result = %s", result)
            logger1.info("Selecting required records from STUDENTS")
        students = unidata_model.Students(getDbSession())
        result = students.select(students.stu_acad_levels.ne(""))
        return result

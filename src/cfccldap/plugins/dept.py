"""LDAP Group Factory for Departments

A group is built for all active records in DEPTS that have people assigned.

 * DEPTS.ACTIVE.FLAG equals "A"

Member records are from X810.ID.CARD where:

 * LDAP.DEPT equals "dept ID"

"""
import logging

from intercall import IntercallError

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE


class DeptRules(LdapGroupFactory, Plugin):
    def getRules(self):
        """
        Add groups based on department IDs.
        """
        dept_rules = []
        depts = unidata_model.Depts(getDbSession())

        result = depts.select()
        for rec in result:
            try:
                dept_rules.append(DeptGroup(group_name=rec.record_id, desc=rec.desc, active=rec.status))
            except IntercallError as e:
                if e.error_code == 30001:
                    print("Record ID %s not found in DEPTS" % rec.record_id)
                else:
                    print(str(e))
        return dept_rules


class DeptGroup(LdapGroup):
    """
    Dynamic group based on IDs in the Departments file.

    Build a group based on membership in a dept from X810.ID.CARD.

    """
    def __init__(self, *args, **kwds):
        super(DeptGroup, self).__init__(**kwds)
        if 'active' in kwds and kwds['active'] == 'A':
            self.is_active = True
        else:
            self.is_active = False
    
    def selectRecords(self):
        logger1 = logging.getLogger('main.DeptGroup')
        if self.is_active:
            logger1.info("Selecting records matching %s from X810.ID.CARD", self.group_name)
            x810_id_card = unidata_model.X810IdCard(getDbSession())
            if self.debug:
                logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
                result = x810_id_card.select(x810_id_card.ldap_dept.eq(self.group_name) + ' SAMPLE ' + str(SAMPLE_SIZE))
            else:
                result = x810_id_card.select(x810_id_card.ldap_dept.eq(self.group_name))
        else:
            result = []
            logger1.info("Inactive Department %s, returning empty result", self.group_name)
        return result

"""LDAP Group for all Students Taking Online Classes

Member records are from STUDENT.COURSE.SEC where:

 * STC.TERM is the current term
 * STC.SECTION.NO begins with I or HY

Unique student IDs are saved from SCS.STUDENT

Records are selected again from STUDENTS where:

 * STU.ACAD.LEVELS is not empty

"""
import datetime
import logging

from intercall.main import SelectError

from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE
from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession

from intercall import AND, SAVING, IntercallError

SECTION_PREFIXES = ["I...", "HY..."]


class OnlineStudentsGroup(LdapGroup, Plugin):

    """Initialize the class and set the term lead time."""
    def __init__(self, *args, **kwds):
        logger1 = logging.getLogger('main.OnlineStudentsGroup')
        self.lead_time = 1
        if 'lead_time' in kwds:
            try:
                self.lead_time = int(kwds['lead_time'])
            except ValueError:
                logger1.error("Invalid lead_time in configuration file: %s.  Defaulting to 1 day", self.lead_time)

        super(OnlineStudentsGroup, self).__init__(**kwds)

    def selectRecords(self):
        """
        Return a list with the students taking Internet or Hybrid classes.
        """
        logger1 = logging.getLogger('main.OnlineStudentsGroup')
        result = []

        # FIXME: exception generated when terms are not found, for example with this select
        #
        # :SELECT TERMS WITH (TERM.START.DATE LE '01/28/2020' AND TERM.END.DATE GE '01/14/2020') TO 0
        #
        # 4 records selected to list 0.
        #
        # >SELECT STUDENTS WITH STU.ACAD.LEVELS NE '' TO 0 REQUIRE.SELECT
        #
        # No data retrieved from current (S)SELECT statement.
        # The following record ids do not exist:
        # 2020SP
        # 2020CE1
        # 2020CE2
        # 2020CE3
        # :EOF)Enter h for help, <CR> for next page
        #
        # The return code from Intercall is now (-1, 0)
        try:
            # padding the term start date so we'll have the group
            # loaded ahead of the term beginning.
            term_start = datetime.date.today() + datetime.timedelta(days=self.lead_time)
            term_end = datetime.date.today()
            terms = unidata_model.Terms(getDbSession())
            result = terms.select(
                AND(terms.term_start_date.le(term_start),
                    terms.term_end_date.ge(term_end)))
            if result is not None and len(result) > 0:
                current_terms = [r.record_id for r in result]
            else:
                current_terms = None
            logger1.info("Current term for this group is %s", current_terms)
            if current_terms is not None:
                student_course_sec = unidata_model.StudentCourseSec(getDbSession())
                # the student IDs are in SCS.STUDENT
                query = "SELECT STUDENT.COURSE.SEC " + SAVING(
                    AND(student_course_sec.stc_term.eq(current_terms),
                        student_course_sec.stc_section_no.like(SECTION_PREFIXES)),
                    student_course_sec.scs_student,
                    unique=True)
                if self.debug:
                    query += " SAMPLE " + str(SAMPLE_SIZE)
                    logger1.info("Query = %s", query)
                db = self.getDbSession()
                result = db.execute(query)
                if self.debug:
                    logger1.info("Result = %s", result)

                students = unidata_model.Students(getDbSession())
                result = students.select(students.stu_acad_levels.ne(""),
                                         require_select=True)
        except SelectError as ex:
            logger1.error('Selecting students failed for ONLINE_STUDENTS: %s', str(ex))
            result = []
        return result

    def selectOwners(self):
        """Return the DL staff as owners"""
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        result = x810_id_card.select(x810_id_card.ldap_dept.eq("UDL"))
        return result

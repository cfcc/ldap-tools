"""LDAP group for the current Faculty

All records from FACULTY and...

Records from X810.ID.CARD where:

 * ID.TYPE equals EMPLOYEE

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE


class CurrentFaculty(LdapGroup, Plugin):
    """
    Build a group for all the current faculty.

    """
    def selectRecords(self):
        logger1 = logging.getLogger('main.Faculty')
        faculty = unidata_model.Faculty(getDbSession())
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        db = self.getDbSession()
        result = None
        if self.debug:
            logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
            sample_size = SAMPLE_SIZE
        else:
            sample_size = None
        faculty.selectOnly(sample=sample_size)
        #faculty.selectOnly(faculty.fac_advise_flag.eq('Y'), sample=sample_size)
        result = x810_id_card.select(x810_id_card.type.eq('EMPLOYEE'), require_select=True)
        return result

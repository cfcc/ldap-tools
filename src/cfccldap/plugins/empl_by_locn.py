"""LDAP Group for employees at a Colleague Location.

Member records are from HRPER where:

 * HRP.PRI.CAMPUS.LOCATION is equal to the given code
 * X810.ID.CARD.TYPE is equal to EMPLOYEE

Groups are named according to the convention LOCN_CODE, so the Wilmington
Campus will be LOCN_WILM, etc.

"""
import datetime
import logging

from intercall import SelectError

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE

class LocationRules(LdapGroupFactory, Plugin):
    """
    Group factory to all active locations, such as:

    * WILM
    * NORTH
    * BURG
    * SURF
    
    """
    def getRules(self):
        """Add location groups based on the employee's office location"""
        logger1 = logging.getLogger('main.LocationRules')
        locn_rules = []

        locations = unidata_model.Locations(getDbSession())

        result = locations.select()
        for rec in result:
            g_name = "LOCN_%s" % rec.record_id
            g_desc = "Colleague Group: %s" % rec.loc_desc
            locn_rules.append(EmplByLocnGroup(group_name=g_name,
                                              desc=g_desc,
                                              key=rec.record_id))
        return locn_rules


class EmplByLocnGroup(LdapGroup):
    """Create a LDAP group with all the employees in a given location.

    Only active employees (in X810.ID.CARD) are listed.
    """

    def __init__(self, *args, **kwds):
        """Initialize the class and set the term lead time."""
        self.location = kwds['key']
        self.card_type = 'EMPLOYEE'
        super(EmplByLocnGroup, self).__init__(**kwds)

    def selectRecords(self):
        """Return a list of employees who have an office in the location."""
        logger1 = logging.getLogger('main.EmplByLocnGroup')
        result = []

        hrper = unidata_model.Hrper(getDbSession())
        x810_id_card = unidata_model.X810IdCard(getDbSession())

        hrper.selectOnly(hrper.hrp_pri_campus_location.eq(self.location))

        try:
            result = x810_id_card.select(x810_id_card.type.eq(self.card_type), require_select=True)
        except SelectError:
            pass

        return result

"""Group Factory to produce Advisee groups for each Advisor

Faculty ID's are selected from FACULTY where:

 * FAC.ADVISEES is not empty

Member records from STUDENTS where:

 * STUDENT.ADVISEMENT matches the FACULTY ID
 * STUDENT.ADVISEMENT END.DATE is empty
 * X810.ID.TYPE is not 'EXPIRED'

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE

from intercall import IntercallError, AND


class AdvisorRules(LdapGroupFactory, Plugin):
    """Advisor rule factory"""
    def getRules(self):
        """
        Build groups for each advisor with the advisees as members.
        """
        advisor_rules = []
        faculty = unidata_model.Faculty(getDbSession())
    
        result = faculty.select(faculty.fac_advisees.ne(""))
        for rec in result:
            advisor_rules.append(AdviseeGroup(key=rec.record_id))
        return advisor_rules


class AdviseeGroup(LdapGroup, Plugin):
    """
    The group name is based on the instructor's LDAP UID
    """
    def __init__(self, *args, **kwds):
        self.faculty = kwds['key']
        group_name = ""
        desc = ""
        org_entity = unidata_model.OrgEntity(getDbSession())
        logger1 = logging.getLogger('main.AdviseeGroup')
        try:
            rec = org_entity.get(self.faculty)
            username = rec.fk_org_entity_env.oee_username
            if username != "":
                group_name = "%s_advisees" % username
                desc = "Faculty Advisee Group"
            else:
                self.faculty = None
        except IntercallError:
            logger1.debug("No username found for faculty %s", self.faculty)
            group_name = ""
            desc = ""
            self.faculty = None
        # update the keyword list before calling the parent class
        kwds['group_name'] = group_name
        kwds['desc'] = desc
        super(AdviseeGroup, self).__init__(**kwds)

    def selectOwners(self):
        owners = []
        person = unidata_model.Person(getDbSession())
        rec = person.get(self.faculty)
        if rec is not None:
            owners.append(rec)
        return owners
                
    def selectRecords(self):
        result = []
        if self.faculty is not None:
            person = unidata_model.Person(getDbSession())
            student_advisement = unidata_model.StudentAdvisement(getDbSession())
            students = unidata_model.Students(getDbSession())

            records = student_advisement.select(AND(
                    student_advisement.faculty.eq(self.faculty),
                    student_advisement.end_date.eq("")))
            for rec in records:
                try:
                    if rec.fk_x810_id_card.type != "EXPIRED":
                        result.append(students.get(rec.student))
                except IntercallError:
                    pass
            # if students/advisees have been found, then add the faculty member to complete the group
            if len(result) > 0:
                result.insert(0, person.get(self.faculty))
        return result

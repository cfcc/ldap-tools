"""LDAP Group for returning students.

Member records are from APPLICANTS where:

 * OEE.OPERS is WEBSTUDENT
 * the X810.ID.CARD record exists
 * APP.APPLICATIONS is not empty

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE
from cfccldap import dbutil


class ReturningStudents(LdapGroup, Plugin):
    """
    Select returning students from the ORG.ENTITY and APPLICANTS file

    ORG.ENTITY = WEBSTUDENT
    no X810.ID.CARD record
    
    """
    def __init__(self, *args, **kwds):
        super(ReturningStudents, self).__init__(**kwds)
        self.current_terms = dbutil.getCurrentTerms()
        
    def selectRecords(self):
        logger1 = logging.getLogger('main.RecentStudents')
        db = self.getDbSession()
        applicants = unidata_model.Applicants(getDbSession())
        if self.debug:
            logger1.info("Selecting records from ORG.ENTITY.ENV")
        result = db.execute("SELECT ORG.ENTITY.ENV WITH OEE.OPERS = 'WEBSTUDENT' SAVING UNIQUE OEE.RESOURCE TO 2")
        if self.debug:
            logger1.info("Result = %s", result)
        result = db.execute("SELECT X810.ID.CARD TO 3")
        if self.debug:
            logger1.info("Result = %s", result)
        result = db.execute("MERGE.LIST 2 DIFF 3 TO 4")
        if self.debug:
            logger1.info("Result = %s", result)
        result = db.execute("SAVE.LIST RECENT.STUDENTS.JF FROM 4")
        if self.debug:
            logger1.info("Result = %s", result)
        result = db.execute("GET.LIST RECENT.STUDENTS.JF")
        if self.debug:
            logger1.info("Result = %s", result)

        if self.debug:
            result = applicants.select(applicants.app_applications.ne('') + ' SAMPLE ' + str(SAMPLE_SIZE))
        else:
            result = applicants.select(applicants.app_applications.ne(''))
        return result


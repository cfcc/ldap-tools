"""LDAP Group Factory for Campus Organizations

A group is built for each record that has the function code 'AD'.

Member records are from CAMPUS.ORGS where:

 * All person ID's from the X810.CURRENT.MEMBERS field

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE
from intercall import IntercallError


class CampusOrgRules(LdapGroupFactory, Plugin):
    """
    Add group rules based on CAMPUS.ORGS function code of 'AD'
    """
    def getRules(self):
        co_rules = []
        campus_orgs = unidata_model.CampusOrgs(getDbSession())

        result = campus_orgs.select(campus_orgs.cmp_functions.eq('AD'))
        for rec in result:
            co_rules.append(CampusOrgs(key=rec.record_id))
        return co_rules


class CampusOrgs(LdapGroup, Plugin):
    """Select a campus organziation and return the current members."""
    def __init__(self, *args, **kwds):
        """LDAP group based on a record from CAMPUS.ORGS.
        Description is retrieved from the Colleague table.

        key - the campus organization code
        """
        self.key = kwds['key']
        cmorg = unidata_model.CampusOrgs(getDbSession())
        try:
            self.cmo_rec = cmorg.get(self.key)
            desc = self.cmo_rec.cmp_desc
            group_name = self.key
        except IntercallError:
            self.cmo_rec = None
            desc = ""
            group_name = None
        # update the keyword list before calling the parent class
        kwds['group_name'] = group_name
        kwds['desc'] = desc
        super(CampusOrgs, self).__init__(**kwds)

    def selectRecords(self):
        active_members = []
        if self.cmo_rec is not None:
            logger1 = logging.getLogger('main.CampusOrgs')
            person = unidata_model.Person(getDbSession())
            for per_id in self.cmo_rec.x810_current_members:
                per_rec = person.get(per_id)
                active_members.append(per_rec)
        return active_members

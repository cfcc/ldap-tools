"""Verify OU Plugin for Current CU Students

Records from STUDENTS where:

 * STU.ACAD.LEVELS is 'CU'

and the ID is contained in records from X810.ID.CARD where:

 * ID.TYPE equals STUDENT

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE


class CurrentCuStudents(LdapGroup, Plugin):
    """Return a list of current curriculum faculty.
    
    """
    CARD_TYPE = 'STUDENT'

    def __init__(self, *args, **kwds):
        self.general_ou = kwds['general_ou']
        self.priv_ou = kwds['priv_ou']
        if 'any_source_ou' in kwds:
            self.any_source_ou = kwds['any_source_ou']
        else:
            self.any_source_ou = False
        super(CurrentCuStudents, self).__init__(**kwds)

    def selectRecords(self):
        logger1 = logging.getLogger('main.CurrentCuStudents')
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        students = unidata_model.Students(getDbSession())
        if self.debug:
            logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
            sample_size = SAMPLE_SIZE
        else:
            sample_size = None
        x810_id_card.selectOnly(x810_id_card.type.eq(self.CARD_TYPE), sample=sample_size)
        result = students.select(students.stu_acad_levels.eq('CU'), require_select=True)
        return result

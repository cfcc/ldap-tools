# python module: CFCCLdap Plugins
#
# Modular System for Colleague LDAP Tools
# ---------------------------------------
# 
# Each group is a plugin (defined by being a child of the Plugin
# class) and also inherits from one of the two LDAP Group classes.
# 
# LdapGroup - Single group based on a selection
#
# LdapGroupFactory - Runs a separate selection to generate multiple
#                    LdapGroup objects.  For example groups based on
#                    departments.
# 
# Each plugin file must contain at least one class that inherits from
# one of the two parent classes above.  They'll also define if they
# should run during the day.  Long running selections should turn on
# the deferLdapConnection flag.
# 
# Instead of specifying an alternate rule file, there will be two options:
# plugin-dir=<PATH> and run-plugin=<NAME>
# 
# Ideas:
# 
# http://aroberge.blogspot.com/2008/12/plugins-part-1-application.html
# 
# http://stackoverflow.com/questions/3659773/python-plugin-system-howto
# 
# http://www.doughellmann.com/PyMOTW/abc/

"""

=== Schedule ===

Groups are processed in three different Maestro jobs:

 * Ldap``Update -- Runs every hour from 7:08 AM to 5:08 PM

  * Includes only plugins not marked ''after_hours''

  * SQL: {{{WHERE disabled = false AND run_alone = false}}}

  * Equivalent rule file: {{{/opt/ldap-tools/etc/rules.yaml}}}

 * Ldap``Update``Adv -- Runs daily at 7:19 PM

  * Update the Advisor groups only

  * SQL: {{{WHERE id = 51 AND disabled = false}}}

  * Equivalent rule file: {{{/opt/ldap-tools/etc/rules-advisor.yaml}}}

 * Ldap``Update``All -- Runs daily at 8:18 PM

  * Includes all plugins except those marked ''disabled''

  * Equivalent rule file: {{{/opt/ldap-tools/etc/rules.yaml}}}

 * Ldap``Update``Stud -- Runs daily at 2:57 AM

  * Includes a single plugin: ''all_students''

  * SQL: {{{WHERE id = 16 AND disabled = false}}}

  * Equivalent rule file: {{{/opt/ldap-tools/etc/rules-allstud.yaml}}}

=== Membership ===

Membership in the groups can be modified by adding them to one of the following groups:

 * No Changes ({{{cn=No Changes,cn=users,dc=ad,dc=cfcc,dc=edu}}})

 * TEST_GROUP ({{{cn=TEST_GROUP,ou=groups,dc=ad,dc=cfcc,dc=edu}}})

To block someone from being automatically added or removed from a group, add them to the
one called 'No Changes'.  The test group should only be used for true test accounts.

"""

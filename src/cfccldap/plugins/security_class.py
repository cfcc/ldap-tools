"""LDAP Group for the Colleague UT Security Classes

Member records are from UT.OPERS where:

 * SYS.USER.CLASSES equal "security class name"

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE
from intercall import IntercallError
from intercall.ic_constants import B_VM


class ColSecurityClass(LdapGroup, Plugin):
    """
    LDAP groups based on Colleague UT Security Classes
    """
    
    def __init__(self, *args, **kwds):
        """
        The group name and description are taken from the colleague record.

        The group is not created if the ut_seclass record is not found.
        """
        self.security_class = kwds['key']
        group_name = self.security_class.replace('.', '_')
        ut_seclass = unidata_model.UtSeclass(getDbSession())

        try:
            rec = ut_seclass.get(self.security_class)
            desc = rec.sys_class_description[:rec.sys_class_description.find("\n")]
        except IntercallError:
            desc = ""
            self.security_class = None
        # update the keyword list before calling the parent class
        kwds['group_name'] = group_name
        kwds['desc'] = desc
        super(ColSecurityClass, self).__init__(**kwds)
        
    def selectRecords(self):
        result = []
        ut_opers = unidata_model.UtOpers(getDbSession())
        person = unidata_model.Person(getDbSession())
        if self.security_class is not None:
            opers = ut_opers.select(ut_opers.sys_user_classes.eq(self.security_class))
            result = [person.get(rec.sys_person_id) for rec in opers]
        return result


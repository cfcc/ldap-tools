"""LDAP Group for Individual Classes

A group is built for each subject in the config file.

Member records are from STUDENT.COURSE.SEC where:

  * STC.TERM is the current term
  * STC.ACAD.LEVEL is "CU"
  * STC.SUBJECT is the requested subject code

 Optional: you can specify a list of course numbers to limit which classes are included in the group, which adds the following:

  * STC.NUMBER is the requested number(s)

Unique student IDs are saved from SCS.STUDENT

Records are selected again from STUDENTS where:

 * STU.ACAD.LEVELS is not empty

"""
import datetime
import logging

from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE
from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession

from intercall import AND, IntercallError, SelectError

GROUP_NAME_PREFIX = "Course_"


class ClassGroup(LdapGroup, Plugin):

    def __init__(self, *args, **kwds):
        """
        Create the class and populate the group name and description.
        
        The group name is based on the key for the class.

        Keywords:

         * key - as a String: the course subject (ex: 'ENG')
         * key - as a List: the course subject and number (ex: ['ENG', '111'])
         * course_number - optional, string or list of course numbers that
                           will be included in the group (i.e. include 210 and
                           211 in LEX, group will be called Course_LEX)

        """
        self.course_number = None
        self.section_number = None

        if isinstance(kwds['key'], list):
            self.subject = kwds['key'][0]
            self.course_number = kwds['key'][1]
            group_name = GROUP_NAME_PREFIX + "".join(kwds['key'])
        else:
            self.subject = kwds['key']
            group_name = GROUP_NAME_PREFIX + str(self.subject)

        try:
            subjects = unidata_model.Subjects(getDbSession())
            result = subjects.get(self.subject)
            desc = result.subj_desc
        except IntercallError:
            desc = "Subject '%s' unknown" % (self.subject)
            self.subject = None

        # Note if this is present, it overrides setting the course number above
        if 'course_number' in kwds:
            self.course_number = kwds['course_number']

        if 'section_number' in kwds:
            self.section_number = kwds['section_number']

        kwds['group_name'] = group_name
        # These three are for the verify_current_cu.py script
        if 'general_ou' in kwds:
            self.general_ou = kwds['general_ou']
        if 'priv_ou' in kwds:
            self.priv_ou = kwds['priv_ou']
        if 'any_source_ou' in kwds:
            self.any_source_ou = kwds['any_source_ou']
        else:
            self.any_source_ou = False

        super(ClassGroup, self).__init__(**kwds)

    def selectRecords(self):
        """
        Return a list with the students taking this subject.
        """
        logger1 = logging.getLogger('main.ClassGroup')
        records_out = []

        if self.subject is not None:
            # padding the term dates so we'll have the group loaded
            # the day before and keep it one day extra
            term_start = datetime.date.today() + datetime.timedelta(days=1)
            term_end = datetime.date.today() + datetime.timedelta(days=-1)
            terms = unidata_model.Terms(getDbSession())
            result = terms.select(
                AND(terms.term_start_date.le(term_start),
                    terms.term_end_date.ge(term_end),
                    terms.term_acad_levels.eq("CU")))
            if result is not None and len(result) > 0:
                current_term = result[0].record_id
            else:
                current_term = None
            logger1.info("Current term for this group is %s", current_term)
            if current_term is not None:
                logger1.info("Selecting records matching %s from STUDENT.COURSE.SEC", self.subject)
                student_course_sec = unidata_model.StudentCourseSec(getDbSession())
                # the student IDs are in SCS.STUDENT
                query = "SELECT STUDENT.COURSE.SEC WITH STC.TERM EQ '%s' AND WITH STC.ACAD.LEVEL EQ 'CU' AND WITH STC.SUBJECT EQ '%s'" % (current_term, self.subject)
                if self.debug:
                    logger1.info("Query = %s", query)
                db = self.getDbSession()
                result = db.execute(query)
                if self.debug:
                    logger1.info("Result = %s", result)

                if self.course_number is not None:
                    if isinstance(self.course_number, list):
                        course_number = "', '".join(self.course_number)
                    else:
                        course_number = self.course_number
                    logger1.info("Filtering records by course numbers %s from STUDENT.COURSE.SEC", course_number)
                    query = "SELECT STUDENT.COURSE.SEC WITH X.SCS.SEC.COURSE.NO EQ '%s' REQUIRE.SELECT" % (course_number)
                    if self.debug:
                        logger1.info("Query = %s", query)
                    result = db.execute(query)
                    if self.debug:
                        logger1.info("Result = %s", result)

                if self.section_number is not None:
                    if isinstance(self.section_number, list):
                        section_number = "', '".join(self.section_number)
                    else:
                        section_number = self.section_number
                    logger1.info("Filtering records by section numbers %s from STUDENT.COURSE.SEC", section_number)
                    query = "SELECT STUDENT.COURSE.SEC WITH X.SCS.SEC.NO EQ '%s' REQUIRE.SELECT" % (section_number)
                    if self.debug:
                        logger1.info("Query = %s", query)
                    result = db.execute(query)
                    if self.debug:
                        logger1.info("Result = %s", result)

                query = "SELECT STUDENT.COURSE.SEC WITH X.SCS.STC.CURRENT.STATUS = 'A' 'N' SAVING UNIQUE SCS.STUDENT REQUIRE.SELECT"
                result = db.execute(query)
                if self.debug:
                    logger1.info("Query = %s", query)
                if self.debug:
                    logger1.info("Result = %s", result)
                # the db.execute stored the result in the active saved list
                students = unidata_model.Students(getDbSession())
                try:
                    records_out = students.select(students.stu_acad_levels.ne(""), require_select=True)
                except SelectError:
                    logger1.warning("no students selected for " + self.subject)
        return records_out

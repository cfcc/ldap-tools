import abc
import logging
import os
import sys

PLUGINS = dict()
_modules = list()


class Plugin(object, metaclass=abc.ABCMeta):
    """Base class for all plugins"""


def initPlugins(external_path=None):
    """Simple plugin initializer"""
    findPlugins(external_path)
    registerPlugins()


def findPlugins(external_path=None):
    """Find all files in the plugin directory and import them.

    external_path -- search this path for plugins as well

    """
    global _modules
    _modules = []
    pcount = 0
    logger1 = logging.getLogger('main.findPlugins')
    if external_path is not None and os.path.exists(external_path):
        plugin_dir = external_path
    else:
        plugin_dir = os.path.dirname(os.path.realpath(__file__))
    plugin_files = [x[:-3] for x in os.listdir(plugin_dir) if x.endswith(".py")]
    sys.path.insert(0, plugin_dir)
    for plugin in plugin_files:
        try:
            logger1.debug("Importing plugin %s...", plugin)
            _modules.append(__import__(plugin))
            pcount += 1
        except Exception as ex:
            logger1.error("Failed to load plugin %s", plugin, exc_info=ex)
    logger1.info("Loaded %d plugins", pcount)


def registerPlugins():
    """Register all class based plugins.

    Uses the fact that a class knows all of its subclasses to
    automatically initialize the relevant plugins
    """
    for plugin in Plugin.__subclasses__():
        PLUGINS[plugin.__name__] = plugin


def getModules():
    """Return a copy of the module list"""
    return _modules[:]


def getPlugins():
    """Return a reference to the plugin hash"""
    return PLUGINS
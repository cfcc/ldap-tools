"""LDAP Group for employees in a given building.

Member records are from HRPER where:

 * HRP.PRI.CAMPUS.BUILDING is equal to the given building code
 * X810.ID.CARD.TYPE is equal to EMPLOYEE

Groups are named according to the convention BLDG_CODE, so building
"A" will be BLDG_A, etc.

Only buildings on CFCC campuses are selected in the group factory.

"""
import datetime
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE

from intercall import AND, OR, SAVING, IntercallError, SelectError


class BuildingRules(LdapGroupFactory, Plugin):
    """
    Group factory to select buildings in each campus:

    * WILM
    * NORTH
    * BURG
    
    """
    def getRules(self):
        """Add building groups based on the employee's office location"""
        logger1 = logging.getLogger('main.BuildingRules')
        bldg_rules = []

        buildings = unidata_model.Buildings(getDbSession())

        result = buildings.select(
            OR(
                buildings.bldg_location.eq("WILM"),
                buildings.bldg_location.eq("NORTH"),
                buildings.bldg_location.eq("BURG")
            )
        )
        for rec in result:
            g_name = "BLDG_%s" % (rec.record_id)
            g_desc = "Colleague Group: %s" % (rec.desc)
            bldg_rules.append(EmplByBldgGroup(group_name = g_name,
                                              desc = g_desc,
                                              key = rec.record_id))
        return bldg_rules


class EmplByBldgGroup(LdapGroup):
    """Create a LDAP group with all the employees in a given building.

    Only active employees (in X810.ID.CARD) are listed.
    """

    def __init__(self, *args, **kwds):
        """Initialize the class and set the term lead time."""
        self.building = kwds['key']
        self.card_type = 'EMPLOYEE'
        super(EmplByBldgGroup, self).__init__(**kwds)

    def selectRecords(self):
        """Return a list of employees who have an office in the building."""
        logger1 = logging.getLogger('main.EmplByBldgGroup')
        result = []

        hrper = unidata_model.Hrper(getDbSession())
        x810_id_card = unidata_model.X810IdCard(getDbSession())

        hrper.selectOnly(hrper.hrp_pri_campus_building.eq(self.building))

        try:
            result = x810_id_card.select(x810_id_card.type.eq(self.card_type), require_select=True)
        except SelectError:
            logger1.warning("No records selected for building " + self.building)

        return result

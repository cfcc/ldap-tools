"""LDAP Group Factory for groups based on Office Codes

These are used to build Informer groups.

All the Office Codes are selected to build groups

Member records are from STAFF where:

 * STAFF.OFFICE.CODE is "code"
 * STAFF.STATUS is current

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE
from cfccldap.unidata_model import role_from_office_code
from intercall import AND


class OfficeCodeRules(LdapGroupFactory, Plugin):
    """Add group rules based on STAFF office codes."""
    def getRules(self):
        oc_rules = []
        for role_name in list(role_from_office_code.keys()):
            oc_rules.append(OfficeCodeGroups(group_name=role_name,
                                             desc="Informer Group",
                                             key=role_from_office_code[role_name]))
        return oc_rules


class OfficeCodeGroups(LdapGroup):
    """Select members from STAFF based on office codes."""
    def __init__(self, *args, **kwds):
        super(OfficeCodeGroups, self).__init__(**kwds)
        self.office_codes = kwds['key']

    def selectRecords(self):
        staff = unidata_model.Staff(getDbSession())
        logger1 = logging.getLogger('main.OfficeCodeGroups')
        if len(self.office_codes) > 0 and self.office_codes[0] != "":
            query = AND(staff.staff_office_code.eq(self.office_codes),
                        staff.staff_status.eq("C"))
            if self.debug:
                logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
                query += ' SAMPLE ' + str(SAMPLE_SIZE)
            result = staff.select(query)
        else:
            result = []
        return result


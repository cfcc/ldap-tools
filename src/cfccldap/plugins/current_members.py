"""LDAP group for Current list of Students or Employees

Records from X810.ID.CARD where:

 * ID.TYPE equals STUDENT (or EMPLOYEE)

Optional keywords:

 * owners - a list of person IDs for email group owners

 * exclude - Exclude non-employees (status == NONEMP), student employees (dept == USIN), and college work-study students (dept == UCW)

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE
from intercall import AND


class CurrentMembers(LdapGroup, Plugin):
    """
    Build a group with everyone with the same ID card type (STUDENT or EMPLOYEE).
    
    """
    def __init__(self, *args, **kwds):
        self._exclude_dept = ['UCW', 'USIN']
        self._exlcude_pos = ['NONEMPPEERMENT']
        self._exclude_status = 'NONEMP'

        self.card_type = kwds['key']

        if 'exclude' in kwds:
            self.do_exclude = True
        else:
            self.do_exclude = False

        super(CurrentMembers, self).__init__(**kwds)

    def selectRecords(self):
        logger1 = logging.getLogger('main.CurrentMembers')
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        result = None
        depts_to_exclude = [x810_id_card.dept.ne(d) for d in self._exclude_dept]
        if self.debug:
            logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
            if self.do_exclude:
                query = AND(x810_id_card.type.eq(self.card_type),
                            *depts_to_exclude,
                            x810_id_card.pos_status.ne(self._exclude_status),
                            x810_id_card.ldap_pos.ne(self._exlcude_pos)) + ' SAMPLE ' + str(SAMPLE_SIZE)
            else:
                query = x810_id_card.type.eq(self.card_type)
            logger1.info("QUERY: [%s]", str(query))
            result = x810_id_card.select(query)
        else:
            if self.do_exclude:
                result = x810_id_card.select(AND(x810_id_card.type.eq(self.card_type),
                                                 *depts_to_exclude,
                                                 x810_id_card.pos_status.ne(self._exclude_status),
                                                 x810_id_card.ldap_pos.ne(self._exlcude_pos)))
            else:
                result = x810_id_card.select(x810_id_card.type.eq(self.card_type))
        return result

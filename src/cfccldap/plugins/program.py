"""LDAP Group Factory for all Academic Programs

Groups are built for all records in ACAD.PROGRAMS that have STATUS of 'A'.  The group name is the same as the program
code.  Description is set to the title of the program.

Member records are from X810.ID.CARD where:

 * LDAP.PROG equals "Academic Program ID"

The "Owner" of the group is the "Dean" of the department as specified on the DEPT form in Colleague.  For example, for
the program "Construction Management Technology, Project Management Certificate (C35190A)" the department would be
"3519" and so one would need to add a "Dean" to it for the owner field to be filled in.

"""
import logging

from intercall import IntercallError, AND, SelectError

from cfccldap import unidata_model, dbutil
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup, LdapGroupFactory
from cfccldap.util import SAMPLE_SIZE
from cfccldap.unidata_model import X810IdCard, AcadPrograms, Person, Depts, Students


class ProgRules(LdapGroupFactory, Plugin):
    def getRules(self):
        """Add group rules based on Academic Program IDs."""
        prog_rules = []
        acad_programs = unidata_model.AcadPrograms(dbutil.getDbSession())

        result = acad_programs.select(acad_programs.status.eq('A'))
        for rec in result:
            try:
                prog_rules.append(ProgramGroup(group_name=rec.record_id, desc=rec.title))
            except IntercallError as e:
                if e.error_code == 30001:
                    print("Record ID %s not found in ACAD.PROGAMS" % rec.record_id)
                else:
                    print(str(e))
        return prog_rules


class ProgramGroup(LdapGroup, Plugin):
    """
    Group based on IDs in the ACAD.PROGRAMS file.
    
    Group membership is based on the Program field in X810.ID.CARD.

    ACAD.PROGRAMS Title.......................................................
 
    C25400        Real Estate
    C55270        Esthetics Instructor
    BSP           Basic Skills Program

    """
    def __init__(self, *args, **kwds):
        super(ProgramGroup, self).__init__(**kwds)
        if 'current_terms' in kwds:
            self.current_terms = kwds['current_terms']
        else:
            self.current_terms = dbutil.getCurrentTerms(lead_time=14)

    def selectRecords(self):
        logger1 = logging.getLogger('main.ProgramGroup')
        try:
            students = Students(dbutil.getDbSession())
            x810_id_card = X810IdCard(dbutil.getDbSession())
            students.selectOnly(AND(students.stu_active_programs.eq(self.group_name),
                                    students.stu_terms.eq(self.current_terms)))
            if self.debug:
                logger1.info("Selecting sample of %d records.", SAMPLE_SIZE)
                # result = x810_id_card.select(x810_id_card.ldap_prog.eq(self.group_name) + ' SAMPLE ' + str(SAMPLE_SIZE))
                result = x810_id_card.select(x810_id_card.type.ne('EXPIRED') + ' SAMPLE ' + str(SAMPLE_SIZE),
                                             require_select=True)
            else:
                # result = x810_id_card.select(x810_id_card.ldap_prog.eq(self.group_name))
                result = x810_id_card.select(x810_id_card.type.ne('EXPIRED'), require_select=True)
        except SelectError as ex:
            result = []
            logger1.error('Selecting students failed for %s: %s', self.group_name, str(ex))
        return result

    def selectOwners(self):
        """Return a list of owner distinguished names"""
        owners = []
        person = Person(dbutil.getDbSession())
        acad_programs = AcadPrograms(dbutil.getDbSession())
        depts = Depts(dbutil.getDbSession())

        acpg_rec = acad_programs.get(self.group_name)
        for dept_id in acpg_rec.acpg_depts:
            rec = depts.get(dept_id)
            owner = person.get(rec.depts_head_id)
            if owner is not None:
                owners.append(owner)
        return owners

"""LDAP Group for former students.

Member records are from X810.ID.CARD where:

 * OEE.OPERS is WEBSTUDENT
 * X810.ID.TYPE is either 'EXPIRED' or empty

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE
from intercall import OR


class RecentStudents(LdapGroup, Plugin):
    """
    Select former students from the ORG.ENTITY file

    ORG.ENTITY = WEBSTUDENT
    X810.ID.TYPE = EXPIRED or ""
    
    """
    def __init__(self, **kwds):
        """When the plugin is used by verify_current_cu, then set the OU's"""
        if 'general_ou' in kwds:
            self.general_ou = kwds['general_ou']
        if 'priv_ou' in kwds:
            self.priv_ou = kwds['priv_ou']
        if 'any_source_ou' in kwds:
            self.any_source_ou = kwds['any_source_ou']
        else:
            self.any_source_ou = False

        super(RecentStudents, self).__init__(**kwds)

    def selectRecords(self):
        logger1 = logging.getLogger('main.RecentStudents')
        db = self.getDbSession()
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        query = "SELECT ORG.ENTITY.ENV WITH OEE.OPERS = 'WEBSTUDENT'"
        if self.debug:
            logger1.info("Selecting %s records from ORG.ENTITY.ENV", SAMPLE_SIZE)
            query +=  ' SAMPLE ' + str(SAMPLE_SIZE)
        result = db.execute(query)
        if self.debug:
            logger1.info("Result = %s", result)
        result = db.execute("SELECT ORG.ENTITY.ENV SAVING UNIQUE OEE.RESOURCE REQUIRE.SELECT")
        if self.debug:
            logger1.info("Result = %s", result)
        result = x810_id_card.select(OR(x810_id_card.type.eq('EXPIRED'),
                                        x810_id_card.type.eq('')))
        return result

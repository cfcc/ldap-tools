"""Verify OU Plugin for Current CU Students or Faculty

Records from FACULTY where:

 * FAC.COURSE.SECTIONS is not empty

and the ID is contained in records from COURSE.SEC.FACULTY where:

 * CSF.FACULTY is not blank

and the ID is contained in records from COURSE.SECTIONS where:

 * SEC.END.DATE is within the past 6 months
 * SEC.ACAD.LEVEL is CU

"""
import logging

from cfccldap import unidata_model
from cfccldap.dbutil import getDbSession
from cfccldap.plugins.base import Plugin
from cfccldap.group import LdapGroup
from cfccldap.util import SAMPLE_SIZE
import datetime
import intercall
from intercall import SAVING, AND


class CurrentCuFaculty(LdapGroup, Plugin):
    """Return a list of current curriculum faculty.
    
    """
    CARD_TYPE = 'EMPLOYEE'

    def __init__(self, *args, **kwds):
        self.general_ou = kwds['general_ou']
        self.priv_ou = kwds['priv_ou']
        if 'any_source_ou' in kwds:
            self.any_source_ou = kwds['any_source_ou']
        else:
            self.any_source_ou = False
        super(CurrentCuFaculty, self).__init__(**kwds)

    def selectRecords(self):
        logger1 = logging.getLogger('main.CurrentCuFaculty')
        course_sections = unidata_model.CourseSections(getDbSession())
        course_sec_faculty = unidata_model.CourseSecFaculty(getDbSession())
        x810_id_card = unidata_model.X810IdCard(getDbSession())
        faculty = unidata_model.Faculty(getDbSession())

        last_six_months = datetime.date.today() - datetime.timedelta(days=180)
        query_date = last_six_months.strftime("%m/%d/%Y")

        if self.debug:
            sample_size = SAMPLE_SIZE
        else:
            sample_size = None
        course_sections.selectOnly(SAVING(AND(course_sections.sec_end_date.ge(query_date),
                                              course_sections.sec_acad_level.eq('CU')),
                                          course_sections.sec_faculty),
                                   sample=sample_size)
        course_sec_faculty.selectOnly(SAVING(course_sec_faculty.csf_faculty.ne(""),
                                             course_sec_faculty.csf_faculty,
                                             unique=True),
                                      require_select=True)
        x810_id_card.selectOnly(x810_id_card.type.eq(self.CARD_TYPE), require_select=True)
        result = faculty.select(faculty.fac_course_sections.ne(""),
                                require_select=True)
        return result

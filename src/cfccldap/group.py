"""This module has code relating to group creation/population"""

import abc
import logging
from os.path import expanduser
import yaml

from intercall.main import IntercallError
from sqlobject import AND, connectionForURI, sqlhub

from . import util
from . import unidata_model
from .plugins.base import PLUGINS
from .rules_model import Rules

_db_session = None


def setDbSession(sess):
    global _db_session
    _db_session = sess


def getCollStatus(person_id):
    """Return the dn of a group if the user is considered active."""
    x810_id_card = unidata_model.X810IdCard(_db_session)

    group_membership = None
    logger1 = logging.getLogger('main.group.getCollStatus')
    rec = x810_id_card.get(person_id)
    logger1.debug("x810_id_card.type = %s", rec.type)
    if rec.type in util.GROUP_CARD_MAP:
        group_membership = "cn=%s,%s" % (util.GROUP_CARD_MAP[rec.type],
                                         util.GROUP_BASE)
    return group_membership


class LdapGroupBase(object, metaclass=abc.ABCMeta):
    """Base class for the LdapGroup and LdapGroupFactory plugin classes"""

    def __init__(self, *args, **kwds):
        """
        Keywords:

         after_hours - instruct the loader to discard this rule unless
                       the after-hours flag has been set

         disabled - this rule has been turned off in the config file

         allow_empty - create the group even if there are no members
                       and do not remove the group once membership
                       drops back to zero

         defer_connection - the LDAP connection will be closed after
                            reading the LDAP group information and
                            then re-opened when the program is ready
                            to send changes

         limit_removal - if true, then the processor will check to
                         see if the number of users removed from this
                         group exceeds the percentage set in the global
                         config file
        """
        self.debug = False
        self.defer_connection = False
        self.disabled = False
        self.is_factory = False
        self.run_after_hours = False
        self.allow_empty = False
        self.limit_removal = False

        if 'after_hours' in kwds:
            self.run_after_hours = kwds['after_hours']
        if 'disabled' in kwds:
            self.disabled = kwds['disabled']
        if 'defer_connection' in kwds:
            self.defer_connection = kwds['defer_connection']
        if 'allow_empty' in kwds:
            self.allow_empty = kwds['allow_empty']
        if 'limit_removal' in kwds:
            self.limit_removal = kwds['limit_removal']
        if 'ldap_session' in kwds:
            self._ldap_session = kwds['ldap_session']
        else:
            self._ldap_session = None

    def setDebug(self, value=True):
        self.debug = value

    def getDbSession(self):
        """Return the global session for plugin use"""
        return _db_session

    def setLdapSession(self, ldap_session):
        self._ldap_session = ldap_session

    def getTable(self, name, codetable=None):
        """See if we have the table available and return it"""
        raise DeprecationWarning('This function no longer works: use "unidata_model.TableName(getDbSession())" instead')
        # logger1 = logging.getLogger('main.group.getTable')
        # cmd = "obj = unidata_model.%s(_db_session" % (name)
        # if codetable is not None:
        #     cmd += ", '%s'" % codetable
        # cmd += ")"
        # try:
        #     # logger1.info(cmd)
        #     exec(cmd, globals())
        #     assert(obj is not None)
        # except NameError as ex:
        #     # This just means we've never accessed the table before,
        #     # so we can go ahead and create a new object.
        #     cls = unidata_model.meta(name)
        #     if codetable is not None:
        #         obj = cls(_db_session, codetable)
        #     else:
        #         obj = cls(_db_session)
        # except IOError as ex:
        #     # Sometimes I'm getting errors that say the file does not
        #     # exist and I'm assuming for now that it's because of a
        #     # failed connection.
        #     logger1.error("IOError during getTable(%s)", name)
        #     logger1.error(ex, exc_info=True)
        #     logger1.info('Sleeping for 3 minutes to wait for error to clear.')
        #     sleep(180)
        #     # this time we won't catch the error if it fails
        #     exec(cmd)
        # return obj


class LdapGroupFactory(LdapGroupBase, metaclass=abc.ABCMeta):
    """Generates multiple LdapGroup objects based on an list of records"""

    def __init__(self, *args, **kwds):
        """Previously group rule factories only ran after hours, now
        this must be specified as a parameter in the rule file.

        """
        super(LdapGroupFactory, self).__init__(**kwds)
        self.is_factory = True
        #self.run_after_hours = True

    @abc.abstractmethod
    def getRules(self):
        """Returns a list of LdapGroup objects"""
        pass


class LdapGroup(LdapGroupBase):
    """Base class for the LDAP group plugins.

    Boilerplate:

        class SampleGroup(LdapGroup, Plugin):
            def selectRecords(self):
                person_pin = unidata_model.PersonPin(getDbSession())
                person_pin.select(...)

    Default keywords:

     * group_name - the AD name of the group (ex: CURRENT_EMPLOYEES)

     * desc - the AD description for the group (ex: All employees with an active position)

     * info - an optional longer description of the group, not sent to Google Apps

     * owners - a list of person ID's that become the owners in the Google Apps group

    """

    def __init__(self, *args, **kwds):
        super(LdapGroup, self).__init__(**kwds)
        if 'group_name' in kwds:
            self.group_name = kwds['group_name']
        else:
            self.group_name = None
        if 'desc' in kwds:
            self.description = kwds['desc']
        else:
            self.description = ""
        if 'info' in kwds:
            self.info = kwds['info']
        else:
            self.info = ""
        if 'owners' in kwds:
            self.owner_person_ids = kwds['owners']
        else:
            self.owner_person_ids = None
        self.member_count = 0
        self.members = []
        self.owners = []

    def group_info(self):
        """Display the class name and group name when printed as a string"""
        return "%s (%s)" % (self.__class__.__name__, self.getGroupName())

    def createGroup(self):
        """Return the LDAP attributes required to create this group"""
        # this is needed later to generate the members' DN
        members = [m.encode() for m in self.getMembers()]
        owners = [m.encode() for m in self.getOwners()]
        if len(members) > 0 or self.allow_empty:
            rec = [
                ('objectClass', [b'top', b'group']),
                ('cn', [self.group_name.encode()]),
                ('samaccountname', [self.group_name.encode()]),
            ]
            if len(members) > 0:
                rec.append(('member', members))
            if len(owners) > 0:
                rec.append(('secretary', owners))
            if self.description.strip() != "":
                # the description will go in two places for now,
                # displayName is used by Google Apps
                rec.append(('description', [self.description.encode()]))
                rec.append(('displayName', [self.description.encode()]))
            if self.info.strip() != "":
                # the info field is used for longer comments and will
                # not be sent to Google Apps
                rec.append(('info', [self.info.encode()]))
        else:
            rec = None
        return rec

    def getMembers(self, force_update=False):
        """Returns a list of member DN's

        force_update -- used to control caching, normally the function only connects to colleague
                        once to run the Unidata selection, after that the same Python list is
                        returned.  Set this to true to have the function connect to Colleague
                        each time the function is called.
        """
        if len(self.members) == 0 or force_update:
            self.members = []
            for rec in self.selectRecords():
                dn = self.getMemberDN(rec)
                if dn is not None:
                    self.members.append(dn)
            owners = self.getOwners()
            if len(owners) > 0:
                for owner_dn in owners:
                    if owner_dn not in owners:
                        self.members.append(owner_dn)
            self.member_count = len(self.members)
        return self.members

    def getOwners(self, force_update=False):
        """Return a list of owner DN's.

        Also called by getMembers so the owner of the group also
        appears in the membership list, something that Google Groups
        needs.

        force_update -- set to True to disable caching, just like getMembers

        """
        if len(self.owners) == 0 or force_update:
            self.owners = []
            for rec in self.selectOwners():
                dn = self.getMemberDN(rec)
                if dn is not None:
                    self.owners.append(dn)
        return self.owners

    def selectOwners(self):
        """Override this to select or populate the list of owner PERSON IDs."""
        owners = []
        if self.owner_person_ids is not None:
            person = unidata_model.Person(_db_session)
            for person_id in self.owner_person_ids:
                rec = person.get(person_id)
                if rec is not None:
                    owners.append(rec)
        return owners

    def selectRecords(self):
        """Override to select the member records from Colleague.

        IMPORTANT: the record must have a link to PERSON.PIN!

        """
        raise NotImplementedError

    def getGroupName(self):
        return self.group_name

    def getMemberDN(self, rec):
        """Return the member's DN based on the PERSON.PIN record."""
        logger1 = logging.getLogger('main.group.getMemberDN')
        member_dn = None
        try:
            # When I used the foreign key, I would occasionally have a problem with the record being unavailable,
            # but as a TRANS column, it looked like the data was just blank instead.  Now I'm accessing the record
            # directly and handling a missing record differently.
            # FIXME: Even though this is a foreign key, this needs to throw an exception if the record can't be read!
            person_id = rec.record_id
            person_pin = unidata_model.PersonPin(_db_session)

            rec = person_pin.get(person_id)
            user_id = rec.user_id
        except (AttributeError, IntercallError) as ex:
            logger1.error("PERSON.PIN record not found for %s: %s", rec.record_id, ex)
            raise
        if user_id is not None and user_id.strip() != '':
            try:
                # FIXME: what should this be callingT
                ldap_rec = self._ldap_session.get_ldap_record(user_id, key_attr="cn")
                if ldap_rec is not None:
                    member_dn = ldap_rec.dn
                    # context = util.getLdapContext(rec.record_id)
                    # member_dn = 'uid=%s,ou=%s,dc=cfcc,dc=edu' % (user_id, context)
                else:
                    logger1.warning("LDAP record not found for %s", rec.record_id)
            except util.DuplicateRecordError as ex:
                logger1.warning("Duplicate record: %s", ex)
        else:
            logger1.warning("No username for ID %s in PERSON.PIN", rec.record_id)
        return member_dn


def processRules(config, use_after_hours_rules):
    logger1 = logging.getLogger('main.group.processRules')
    rules = []
    for rec in config:
        try:
            this_rule = PLUGINS[rec['class_name']](**rec)
            if not this_rule.disabled and (not this_rule.run_after_hours or use_after_hours_rules):
                if this_rule.is_factory:
                    rules.extend(this_rule.getRules())
                    logger1.debug("Added rule factory %s (after hours=%s)", this_rule, this_rule.run_after_hours)
                else:
                    rules.append(this_rule)
                    logger1.debug("Added single rule %s (after hours=%s)", this_rule, this_rule.run_after_hours)
            else:
                logger1.debug("Skipped rule %s", this_rule)
        except KeyError as ex:
            logger1.error("Unable to process rec %s", rec, exc_info=ex)
    return rules


def loadRules(filename, use_after_hours_rules=False):
    """Load a YAML file containing the rules for creating groups.

    Rules that refer to missing plugins will generate an error
    message, but the process with continue.

    Rules marked after_hours will not be loaded unless the flag to
    use_after_hours_rules is set to True.

    """
    fd = open(expanduser(filename))
    config = yaml.safe_load(fd)
    fd.close()
    return processRules(config, use_after_hours_rules)


def loadRulesFromDatabase(db_uri, use_after_hour_rules=False, sql_rule_id=None, include_run_alone=False):
    """Load the rule configuration from a database.

    This function uses SQLObject to connect to a SQL database and load the rule configs.

    """
    logger1 = logging.getLogger('main.group.loadRulesFromDatabase')
    connection = connectionForURI(db_uri)
    sqlhub.processConnection = connection
    if sql_rule_id is not None:
        query = AND(Rules.q.id == sql_rule_id, Rules.q.disabled == False)
    else:
        if include_run_alone:
            query = Rules.q.disabled == False
        else:
            query = AND(Rules.q.disabled == False, Rules.q.runAlone == False)
    config = [r.get_as_dict() for r in Rules.select(query)]
    logger1.debug("config = %s", config)
    return processRules(config, use_after_hour_rules)

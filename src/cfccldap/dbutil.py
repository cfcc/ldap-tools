"""Retrieve information from Colleague

"""
import datetime
import intercall

# private session connection objects
_db_session = None


def connect(db_url, db_password = None):
    global _db_session

    _db_session = intercall.Session(url=db_url)
    _db_session.connect(password = db_password)


def isOpen():
    result = False
    if _db_session is not None:
        result = _db_session.isOpen()
    return result


def getDbSession():
    """Return a reference the the UniData session object"""
    return _db_session


def getCurrentTerms(today=None, lead_time=0):
    """Return a list of current term codes"""

    if today is None:
        today = datetime.date.today()
    term_end = today.strftime("%m/%d/%Y")
    if lead_time > 0:
        today = today + datetime.timedelta(days=lead_time)
    term_start = today.strftime("%m/%d/%Y")

    query = "SELECT TERMS WITH TERM.START.DATE LE '%s' AND WITH TERM.END.DATE GE '%s'" % (term_start, term_end)
    _db_session.execute(query)
    curr_terms = [t.decode() for t in _db_session.readNext()]
    return curr_terms


def selectAllStaff():
    """Create an active savedlist of all the staff records"""
    sl_name = "LDAP.UPDATE.STAFF"
    _db_session.execute("SELECT ORG.ENTITY.ENV WITH OEE.OPERS = 'WEBFACSTAFF'")
    _db_session.execute("SELECT ORG.ENTITY.ENV SAVING OEE.RESOURCE REQUIRE.SELECT")
    _db_session.execute("SAVE.LIST %s" % sl_name)
    return sl_name

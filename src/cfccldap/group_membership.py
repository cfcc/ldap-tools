import logging
from . import util, RemoveMaxPercentExceededError
from pprint import pprint

from .util import LdapSession


def update_membership(ldap_group, rule, no_changes_members, max_percentage_to_remove=0.75, is_quiet=False, is_debug=False, ldap_connection=None):
    """Returns a tuple of two lists with members to add and remove.

    IMPORTANT: This function may close the LDAP connection for a large rule.
    """
    logger1 = logging.getLogger('update_membership')

    ldap_members = ldap_group.get_members()
    coll_group = rule.getMembers()

    msg = "Current membership for %s is %d" % (rule.getGroupName(), len(ldap_members))
    logger1.info(msg)

    # Rules that create large groups can have a flag to close the connection while processing the changes
    if rule.defer_connection and ldap_connection is not None:
        ldap_connection.close()

    # find the record ID's to add to the group
    unique_dn = dict()
    for member_dn in coll_group:
        if member_dn not in ldap_members and member_dn not in no_changes_members:
            unique_dn[member_dn] = 1
    members_to_add = sorted(unique_dn.keys())
    msg = "Adding %d members to %s" % (len(members_to_add),
                                       rule.getGroupName())
    logger1.info(msg)
    if not is_quiet:
        print(msg)
    if is_debug:
        print("[DEBUG] Members to add:")
        pprint(sorted(members_to_add))

    # find the record ID's to remove from the group
    unique_dn = dict()
    for member_dn in ldap_members:
        if member_dn not in coll_group and member_dn not in no_changes_members:
            unique_dn[member_dn] = 1
    members_to_remove = sorted(unique_dn.keys())
    msg = "Removing %d members from %s" % (len(members_to_remove), rule.getGroupName())
    logger1.info(msg)
    if not is_quiet:
        print(msg)
    if is_debug:
        print("[DEBUG] Members to remove:")
        pprint(sorted(members_to_remove))
    if rule.limit_removal:
        adj_membership = len(ldap_members) + len(members_to_add)
        if adj_membership > 0:
            percent_removed = len(members_to_remove) / adj_membership
        else:
            percent_removed = 0
        if percent_removed > max_percentage_to_remove:
            raise RemoveMaxPercentExceededError(percent_removed)

    # re-open a closed connection
    if rule.defer_connection and ldap_connection is not None:
        ldap_connection.connect()

    return members_to_add, members_to_remove


def update_owners(ldap_group, rule, is_quiet=False, is_debug=False):
    """Return a tuple with two lists of owners to add and owners to remove."""
    logger1 = logging.getLogger('update_owners')

    ldap_owners = ldap_group.get_owners()
    coll_owners = rule.getOwners()

    if len(coll_owners) > 0:
        unique_dn = dict()
        if ldap_owners is not None:
            for owner_dn in coll_owners:
                if owner_dn not in ldap_owners:
                    unique_dn[owner_dn] = 1
            owners_to_add = sorted(unique_dn.keys())
        else:
            owners_to_add = coll_owners
        msg = "Adding %d owners to %s" % (len(owners_to_add),
                                          rule.getGroupName())
        logger1.info(msg)
        if not is_quiet:
            print(msg)
        if is_debug:
            print("[DEBUG] Owners to add:")
            pprint(sorted(owners_to_add))
    else:
        owners_to_add = []

    if ldap_owners is not None:
        unique_dn = dict()
        for owner_dn in ldap_owners:
            if owner_dn not in coll_owners:
                unique_dn[owner_dn] = 1
        owners_to_remove = sorted(unique_dn.keys())
        msg = "Removing %d owners from %s" % (len(owners_to_remove),
                                              rule.getGroupName())
        logger1.info(msg)
        if not is_quiet:
            print(msg)
        if is_debug:
            print("[DEBUG] Owners to remove:")
            pprint(sorted(owners_to_remove))
    else:
        owners_to_remove = []

    return owners_to_add, owners_to_remove


def get_no_changes(ldap_session):
    """Return a list of accounts that should not be updated by this program.

    Retrieve the membership of these groups to use later in preventing their addition to or removal from other groups
    """
    logger1 = logging.getLogger('update_owners')
    assert(isinstance(ldap_session, LdapSession))

    no_changes_group = ldap_session.get_ldap_group('No Changes', alt_group_ou='cn=users')
    logger1.info("Group 'No Changes' has %s member(s)", len(no_changes_group.get_members()))

    test_group = ldap_session.get_ldap_group('TEST_GROUP')

    no_changes_group.add_members(test_group.get_members())
    no_changes_members = no_changes_group.get_members()

    logger1.info("Group 'No Changes' plus 'TEST_GROUP' has %s member(s)", len(no_changes_members))
    return no_changes_members

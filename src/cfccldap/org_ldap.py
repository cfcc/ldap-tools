"""
Build a LDAP group definition in Colleague.

This class is as generic as possible so it can be used with Depts,
Advisors, Programs, etc.

Populates ORG.LDAP.GROUPS, UT.PRCS.CTL, and UT.PRCS.GEN

"""
import sys
sys.path.append("/usr/local/lib/python2.6/site-packages/")

import intercall

org_ldap_groups = None
ut_prcs_ctl = None
ut_prcs_gen = None

class OrgLdap:

    def __init__(self, group_name, group_desc, addtl_selection=[]):
        """
        Additional selection criteria can be added using a list of tuples:
        
        [("AND", "X810.LDAP.ID.NOT.EXPIRED"),
         ("OR", "X810.LDAP.HAS.USERNAME")]

        These will be converted to the Colleague format when creating
        the record.
        
        """
        self.group_name = group_name
        self.group_desc = group_desc
        self.selection_name = "X810.LDAP.%s" % (self.group_name)
        self.datestamp = datetime.date.today().strftime("%m/%d/%Y") 

        self.connector = []
        self.selection_criteria = [self.selection.name]
        
        if isinstance(addtl_selection, list):
            if len(addtl_selection) > 0:
                self.connector = [i[0] for i in addtl_selection]
                self.connector.append("")
                self.selection_criteria.extend([i[1] for i in addtl_selection])
        else:
            raise TypeError("addtl_selection must be a list of tuples")

    def buildGroup(self, force = False):
        """Return true if the group record was created"""
        result = False
        if not force:
            can_create = False
            try:
                rec = org_ldap_groups.get(dept_name, must_exist=True)
            except IntercallError:
                can_create = True
        else:
            can_create = True

        # we only create the group record if it didn't already exist
        if can_create:
            datestamp = datetime.date.today().strftime("%m/%d/%Y")
            org_ldap_groups.set(
                dept_name,
                ldpg_title = self.group_name,
                ldpg_group_name = self.group_name,
                ldpg_group_desc = self.group_desc,
                ldpg_group_scope = "GLOBAL",
                ldpg_group_type = "SECURITY",
                ldpg_group_domain = "LDAP",
                ldpg_group_context = "ou=groups",
                org_ldap_groups_adddate = self.datestamp,
                org_ldap_groups_addopr = "JFRIANT",
                ldpg_sel_order = self.sel_order,
                ldpg_sel_selection_criteria = self.selection_criteria,
                ldpg_sel_connector = self.connector,
                ldpg_appl = "UT"
                )
            result = True
        return result

    def buildPrcsCtl(self):
        ut_prcs_ctl.set(
            record_id,
            process_description = "LDAP Group",
            process_execution_type = "J",
            process_to_execute = "JSBU02 %s" % (self.selection_name),
            process_class = "S",
            prcs_ctl_submit_loc = ""
            )

    def buildPrcsGen(self):

        ut_prcs_gen.set(
            self.selection_name,
            pgen_description = "LDAP Group",
            pgen_class = "S",
            pgen_secure_flag = "Y",
            prcs_gen_add_date = self.datestamp,
            prcs_gen_add_operator = "JFRIANT",
            ls_fname = "X810.ID.CARD",
            ls_filevar = "",
            ls_select_connective = "WITH",
            ls_select_field = "X810.ID.LDAP.DEPT",
            ls_select_rel_opcode = "EQ",
            ls_select_value = "'%s'" % (self.group_name),
            ls_range_select_value = "")


"""Defines an object that represents a LDAP user"""

from .filetimes import dt_to_filetime

DOMAIN_NAME = "ad.cfcc.edu"
BASE_DN = "dc=ad,dc=cfcc,dc=edu"
GROUP_DN = "ou=groups"


class LdapUser:
    def __init__(self, org_entity_rec=None, base_dn=BASE_DN, username=None, server_domain_name=DOMAIN_NAME):
        """Create a blank object

        org_entity_rec is for passing in a record from ORG.ENTITY,
                       which then this constructor will call another
                       function to load the name and biographic
                       information from Colleague.

        base_dn will default to the one specified in this script, but
                should be populated from the config module.

        username can be used to specify a name to put in the required
                 name fields if they don't get populated from the OE
                 record

        """
        self.required_fields = [
            'company',
            'displayName',
            'givenName',
            'mail',
            'postalCode',
            'sn',
            'title',
            'departmentNumber',
            # 'userAccountControl'
            ]
        
        self.depts_desc = ""
        # self.employeeType = ""
        self._disp_name = ""
        self.account_expires = None
        self.birthday = ""
        self.company = "Cape Fear Community College"
        self.description = None
        self.email = ""
        self.first = ""
        self.group_id = ""
        self.home_directory = ""
        self.last = ""
        self.middle = ""
        self.office = ""
        self.person_id = ""
        self.phone = ""
        self.title = ""
        self.username = None
        self.zip = ""
        self.other_office = ""
        self.other_phone = ""

        self.object_classes = ['top', 'person', 'organizationalPerson', 'user']
        self.user_account_control = '514'  # Normal User (512) | Disabled (2)

        self.key_attr = "CN"
        self.context = "UNKNOWN"
        self.domain = base_dn
        self.server_domain_name = server_domain_name

        if org_entity_rec is not None:
            self.importOrgEntity(org_entity_rec)
        elif username is not None:
            # populate the required name fields
            self.username = username
            self.displayName = username
            self.givenName = username
            self.sn = username

    def asDict(self, no_blanks=False):
        ldap_rec = {
            'cn': self.username.encode(),
            'company': self.company.encode(),
            'description': self.getDesc().encode(),
            'displayName': self.getDispName().encode(),
            'employeeNumber': self.person_id.encode(),
            'employeeID': self.person_id.encode(),
            'gidNumber': self.getGID().encode(),
            'givenName': self.first.encode(),
            'loginShell': '/bin/bash'.encode(),
            'objectClass': [o.encode() for o in self.object_classes],
            'physicalDeliveryOfficeName': self.office.encode(),
            'postalCode': self.zip.encode(),
            'roomNumber': self.office.encode(),
            'sAMAccountName': self.getsAMAccountName().encode(),
            'sn': self.last.encode(),
            'telephoneNumber': self.phone.encode(),
            'title': self.title.encode(),
            'uidNumber': self.getUID().encode(),
            'unixHomeDirectory': self.getHomeDir().encode(),
            'userAccountControl': self.getUAC().encode(),
            'userPrincipalName': self.getUPN().encode(),
            'departmentNumber': self.depts_desc.encode(),
            # 'employeeType': self.pos_type,
            }
        if self.email is not None:
            ldap_rec['mail'] = self.email.encode()
        else:
            ldap_rec['mail'] = None

        # optional values that should not be set if empty
        if self.account_expires is not None:
            value = dt_to_filetime(self.account_expires)
            ldap_rec['accountExpires'] = str(value)

        # LDAP modlists no longer handle blank values well, so we can opt to remove them from the record
        if no_blanks:
            new_ldap_rec = dict()
            for k in ldap_rec.keys():
                if ldap_rec[k] != b'':
                    new_ldap_rec[k] = ldap_rec[k]
            return new_ldap_rec
        else:
            return ldap_rec

    def requiredFields(self, selected_fields=None):
        d = self.asDict()
        fields = dict()
        if selected_fields is None:
            selected_fields = self.required_fields
        for k in selected_fields:
            fields[k] = d[k]
        return fields

    def missingRequiredFields(self, ldap_record):
        """Return a list of fields that are missing or do not match Colleague."""
        missing = []
        d = self.asDict()
        for k in self.required_fields:
            try:
                if d[k] is not None:
                    current_value = d[k].decode()
                else:
                    current_value = None
                if current_value != ldap_record.get_attr_values(k)[0]:
                    missing.append(k)
            except IndexError:
                missing.append(k)
            except KeyError:
                # we can ignore some missing fields, i.e. title not in a student record
                if k in d and d[k] != b'':
                    missing.append(k)
        return missing

    def importOrgEntity(self, rec):
        """Import the user information from ORG.ENTITY"""

        self.person_id = rec.record_id
        self.username = rec.fk_org_entity_env.oee_username
        
        self.first = rec.oe_first_name
        self.middle = rec.fk_person.middle_name
        self.last = rec.oe_last_name
        
        self._disp_name = rec.fk_person.preferred_name

        self.birthday = rec.fk_person.birth_date
        self.preferred_address = rec.fk_person.preferred_address

    def importOffice(self, hrper_rec):
        """Import the office information from HRPER"""
        if hrper_rec is not None:
            if hrper_rec.hrp_pri_campus_building.strip() != "" and hrper_rec.hrp_pri_campus_office.strip() != "":
                self.office = "%s-%s" % (hrper_rec.hrp_pri_campus_building,
                                         hrper_rec.hrp_pri_campus_office)
            self.phone = hrper_rec.hrp_pri_campus_extension
            if len(hrper_rec.hrp_oth_campus_building) > 0 and len(hrper_rec.hrp_pri_campus_office) > 0:
                if hrper_rec.hrp_oth_campus_building[0].strip() != "" and hrper_rec.hrp_oth_campus_office[0].strip() != "":
                    self.other_office = "%s-%s" % (hrper_rec.hrp_oth_campus_building[0],
                                                   hrper_rec.hrp_oth_campus_office[0])
            if len(hrper_rec.hrp_oth_campus_extension) > 0:
                self.other_phone = hrper_rec.hrp_oth_campus_extension[0]
        
    def getFullName(self):
        full_name = self.first
        if self.middle.strip() != "":
            full_name += " " + self.middle
        full_name += " " + self.last
        return full_name

    def getDispName(self):
        if self._disp_name.strip() != "":
            name_out = self._disp_name
        else:
            name_out = self.getFullName()
        return name_out

    def getCommonName(self):
        return "%s %s" % (self.first, self.last)
    
    def getDn(self):
        if isinstance(self.context, list):
            context = ",".join(["ou=%s" % i for i in self.context])
        else:
            context = "ou=%s" % self.context
        dn_out = "%s=%s,%s,%s" % (self.key_attr,
                                  self.username,
                                  context,
                                  self.domain)
        return dn_out

    def getHomeDir(self):
        return "/home/" + self.username

    def getDesc(self):
        if self.description is not None:
            return self.description
        else:
            return "User Account"

    def getsAMAccountName(self):
        if len(self.username) > 20:
            return self.username[:17] + self.username[-3:]
        else:
            return self.username
    
    def getUPN(self):
        return self.username + "@" + self.server_domain_name

    def getUID(self):
        if self.person_id is not None and self.person_id != "":
            return "%d" % int(self.person_id)
        else:
            return ""

    def getGID(self):
        return '1000'

    def setUAC(self, new_value):
        """The LDAP attribute for the userAccountControl is a string"""
        self.user_account_control = new_value

    def getUAC(self):
        return self.user_account_control

"""Update the LDAP tree with information from Colleague.

This module holds all the utility functions that used to be separately
defined in ldap_update_email and ldap_update_staff.

"""
from pprint import pprint
import ldap
import ldap.modlist
from ldap.controls import SimplePagedResultsControl
from . import ldaphelper
import logging
import logging.handlers

from .user import LdapUser

# constants
LDAP_ONE_REC = 0
LDAP_ALL_REC = 1
ASYNC_TIMEOUT = 240
PAGE_SIZE = 1000
SL_NUMBER = 3
DOMAIN_NAME = "dc=ad,dc=cfcc,dc=edu"
USER_PREFIX = "ou=cfcc users"
GROUP_PREFIX = "ou=groups"
SEARCH_BASE = USER_PREFIX + "," + DOMAIN_NAME
GROUP_BASE = GROUP_PREFIX + "," + DOMAIN_NAME
DEFAULT_CONTEXT = ['general', 'students']
CONTEXT_PRIORITY = ['FACULTY', "EMPLOYES", 'STUDENTS', 'APPLICANTS']
WHERE_USED_CONTEXTS = {"FACULTY": ["faculty", "employees"],
                       "EMPLOYES": ["staff", "employees"],
                       "STUDENTS": ["general", "students"],
                       "APPLICANTS": ["general","students"]}
GROUP_CARD_MAP = {
    'CURRENT_EMPLOYEES': 'EMPLOYEE',
    'CURRENT_STUDENTS': 'STUDENT',
    # and in reverse:
    'EMPLOYEE': 'CURRENT_EMPLOYEES',
    'STUDENT': 'CURRENT_STUDENTS'
    }
DN_EXCEPTIONS = ['uid=placeholder,ou=students,ou=cfcc users,dc=cfcc,dc=cfcc,dc=edu']
SAMPLE_SIZE = 50
DEFAULT_EMAIL_DOMAIN = "@mail.cfcc.edu"
DEFAULT_OWNER_ATTR = 'secretary'

# AD userAccountControl Flags
NORMAL_ACCOUNT = 0x0200  # 512
PASSWORD_EXPIRED = 0x800000
DONT_EXPIRE_PASSWORD = 0x10000

DEBUG = False


class DuplicateRecordError(Exception):
    pass


class LdapReturnCodeError(Exception):
    pass


class LdapSession:
    domain_name = DOMAIN_NAME
    user_prefix = USER_PREFIX
    group_prefix = GROUP_PREFIX
    
    def __init__(self, ldap_url, priv_dn, ldap_password, defer_connection=False):
        self.url = ldap_url
        self.priv_dn = priv_dn
        self.password = ldap_password
        self._connection = None
        self.domain_upn = None
        
        if not defer_connection:
            self.connect()

    def connect(self):
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        ldap.set_option(ldap.OPT_REFERRALS, 0)
        self._connection = ldap.initialize(self.url)
        # Use this with the debug level option above to show the ldap
        # library debug messages:
        # this_session = ldap.initialize(ldap_url, trace_level=2)
        self._connection.protocol_version = 3
        self._connection.simple_bind_s(self.priv_dn, self.password)

    def close(self):
        self._connection.unbind()

    def is_open(self):
        return self._connection is not None
    
    def get_search_base(self):
        return self.user_prefix + "," + self.domain_name
    
    def get_group_base(self, alt_prefix=None):
        if alt_prefix is not None:
            return alt_prefix + "," + self.domain_name
        else:
            return self.group_prefix + "," + self.domain_name

    def set_default_domain(self, domain, user_prefix=None, group_prefix=None):
        """Set up alternate DN instead of what is hard-coded in the module.
    
        domain = the base domain name, ex: dc=ad,dc=cfcc,dc=edu
    
        user_prefix = the user container(s), ex: ou=cfcc users
    
        group_prefix = the group container(s), ex: ou=groups
    
        """
        self.domain_name = domain
        self.domain_upn = '.'.join([dc.split('=')[1] for dc in domain.split(',')])
        if user_prefix is not None:
            self.user_prefix = user_prefix
        if group_prefix is not None:
            self.group_prefix = group_prefix

    def get_nis_domain(self, alt_domain=None):    
        if alt_domain is None:
            alt_domain = self.domain_name
        return alt_domain.split(',')[0].split('=')[1]

    def get_server_domain_name(self):
        return self.domain_upn

    def search(self, search_filter, attrs=None):
        """A generator that selects records and returns LDAPHelper objects
    
        search_filter - a string with a valid search query
    
        attrs - specify a list of attributes to return (defaults to
                employeeNumber and userPassword if left to None)
    
        """
        # use default list if nothing was specified
        if attrs is None:
            attrs = ['employeeNumber', 'userPassword']
    
        logger1 = logging.getLogger('main.search')
        rid = self._connection.search(self.get_search_base(),
                                      ldap.SCOPE_SUBTREE,
                                      search_filter,
                                      attrs)
        # start generator
        try:
            raw_res = (None, None)
            while raw_res[0] != ldap.RES_SEARCH_RESULT:
                raw_res = self._connection.result(rid, LDAP_ONE_REC, ASYNC_TIMEOUT)
                result_set = ldaphelper.get_search_results(raw_res)
                if len(result_set) == 1:
                    for rec_out in result_set:
                        yield rec_out
        except ldap.TIMEOUT as ex:
            self._connection.abandon(rid)
            logger1.critical("The server took too long to respond.\n%s", ex)
        except ldap.SIZELIMIT_EXCEEDED as ex:
            self._connection.abandon(rid)
            logger1.critical("Search result size limit exceeded for query: %s", search_filter)

    def select_all(self, attrs=None):
        return self.search('objectClass=person', attrs)

    def select_all_paged(self, search_filter=r'(objectClass=person)', alt_search_dn=None):
        """Used for a large selection.  Handles the paging between result sets.
    
        From <http://www.novell.com/coolsolutions/tip/18274.html>
    
        search_filter - specify a search filter other than objectClass=person

        alt_search_dn - specify a search base manually
    
        IMPORTANT: unlike selectAll above, Active Directory will not take
        a custom attribute list when returning paged results, it gives an
        extension unavailable error if you specify one.
    
        Python 2.4 fixes from: http://mattfahrner.com/2014/03/09/using-paged-controls-with-python-and-ldap/
    
        """
        lc = _create_controls(PAGE_SIZE)

        if alt_search_dn is None:
            search_base = self.get_search_base()
        else:
            search_base = alt_search_dn
    
        logger1 = logging.getLogger('main.selectAllPaged')
        pages = 0
        while True:
            msgid = self._connection.search_ext(search_base,
                                                ldap.SCOPE_SUBTREE,
                                                search_filter,
                                                serverctrls=[lc])
            pages += 1
            try:
                logger1.debug("Getting page %d", pages)
                rtype, rdata, rmsgid, serverctrls = self._connection.result3(msgid)
                logger1.debug("%d results (rtype=%s, rmsgid=%s, serverctrls=%s)", len(rdata), rtype, rmsgid, serverctrls)
            except ldap.NO_SUCH_OBJECT:
                logger1.error("LDAP no such object for %s", self.get_search_base())
                break
    
            # generate LDAPSearchResult object and return one at a time
            result_set = ldaphelper.get_search_results(rdata)
            for rec_out in result_set:
                yield rec_out
    
            pctrls = _get_pctrls(serverctrls)
            if not pctrls:
                logger1.error("Server ignores RFC 2696 control.")
                break
            if not _set_cookie(lc, pctrls, PAGE_SIZE):
                break

    def get_employee_number(self, key, key_attr='uid'):
        """Return the employee number listed in the LDAP record"""
        result_id = self._connection.search(self.get_search_base(),
                                            ldap.SCOPE_SUBTREE,
                                            "%s=%s" % (key_attr, key),
                                            ['employeeNumber'])
        raw_res = self._connection.result(result_id, LDAP_ALL_REC)
        result_set = ldaphelper.get_search_results(raw_res)
        return result_set[0].get_attr_values('employeeNumber')[0]

    def get_ldap_record(self, user_id, return_all=False, key_attr='cn', attributes=None):
        """Return the ldap record for the given username.
    
        return_all -- if false (default) raise an exception when the
                      search returns more than one record, otherwise set
                      to true to return multiple records.
    
        key_attr -- specify an alternate attribute for the search criteria.
    
        alt_session -- use this if you already have an LDAP session open,
                       otherwise defaults to the modules global session.
        attributes -- list of attributes to return.  Default is '*'.
    
        """
        logger1 = logging.getLogger('getLdapRecord')
        rec_out = None

        if attributes is None:
            attributes = ('*',)
    
        ldap_filter = "%s=%s" % (key_attr, user_id)
        logger1.debug("search(%s, %s, %s, %r)", self.get_search_base(), ldap.SCOPE_SUBTREE, ldap_filter, attributes)
        # if we're not returning every record AD gives us, then we'll need
        # to make sure a few fields are in place so we can do some error
        # checking
        if not return_all:
            if isinstance(attributes, list):
                if key_attr not in attributes:
                    attributes.append(key_attr)
                if 'employeeNumber' not in attributes:
                    attributes.append('employeeNumber')
        # run the LDAP search
        result_id = self._connection.search(self.get_search_base(),
                                            ldap.SCOPE_SUBTREE,
                                            ldap_filter,
                                            attributes)
        raw_res = self._connection.result(result_id, LDAP_ALL_REC)
        result_set = ldaphelper.get_search_results(raw_res)
        if not return_all:
            if len(result_set) == 1:
                rec_out = result_set[0]
            elif len(result_set) > 1:
                # first verify that these results are valid and not extra
                # entries tacked on by ActiveDirectory
                employee_number = None
                actual_errors = 0
                for rec in result_set:
                    try:
                        rec_id = rec.get_attr_values(key_attr)[0]
                        employee_number = rec.get_attr_values('employeeNumber')[0]
                        actual_errors += 1
                        logger1.critical("Record %s has duplicate employeeNumber %s", rec_id, employee_number)
                    except AttributeError:
                        if rec.dn != "":
                            logger1.critical("Additional record: %s", rec.dn)
                            actual_errors += 1
                        else:
                            logger1.warning("Additional record: %s", rec.attrs)
                    except KeyError:
                        if rec.dn != "":
                            logger1.critical("Additional record: %s", rec.dn)
                            actual_errors += 1
                        else:
                            logger1.warning("Additional record: %s", rec.attrs)
                if actual_errors > 1:
                    raise DuplicateRecordError("[FATAL] Multiple LDAP records found for %s" % employee_number)
                else:
                    rec_out = result_set[0]
        else:
            rec_out = result_set[:]
        return rec_out

    def get_ldap_group(self, group_cn, alt_group_ou=None, range_lower=None, owner_attr=DEFAULT_OWNER_ATTR):
        """
        Return an ActiveDirectoryGroup object with attributes for the group dn,
        the array of the group members, and an optional array of group owners.
    
        Because Active Directory has a 1500 value limit on the member
        attribute we have to do some additional processing to determine if
        there are additional records to retrieve.
    
        if range_lower is set then we continue to run recursive calls
        until the end of the group has been reached.
    
        :rtype : ActiveDirectoryGroup
        """
        this_group = ActiveDirectoryGroup()
    
        logger1 = logging.getLogger('getLdapGroup')
        logger1.debug("base_dn=%s, group_cn=%s", self.get_group_base(alt_group_ou), group_cn)
    
        if range_lower is not None:
            attr_request = "member;range=%d-*" % range_lower
        else:
            attr_request = '*'
    
        result_id = self._connection.search(self.get_group_base(),
                                            ldap.SCOPE_SUBTREE,
                                            "cn=%s" % group_cn,
                                            [attr_request])
        raw_res = self._connection.result(result_id, LDAP_ALL_REC)
        result_set = ldaphelper.get_search_results(raw_res)
    
        if len(result_set) > 0:
            rec_out = result_set[0]
    
            this_group.set_dn(rec_out.dn)
    
            if rec_out.has_attribute(owner_attr):
                this_group.add_owners(rec_out.get_attr_values(owner_attr))
    
            if rec_out.has_attribute('member'):
                this_group.add_members(rec_out.get_attr_values('member'))
            if this_group.empty():
                member_key = None
                for k in rec_out.get_attr_names():
                    if k.startswith("member;range"):
                        member_key = k
                        break
                if member_key is not None:
                    # print '[DEBUG] found %s attribute' % member_key
                    # request the next group from active directory
                    # and append it to our list
                    upper = member_key[member_key.index('-') + 1:]
                    if upper != '*':
                        next_range = int(upper) + 1
                        # print '[DEBUG] requesting records %d to *' % next_range
                        result = self.get_ldap_group(group_cn, alt_group_ou, next_range)
                        this_group.add_members(result.get_members())
                    # now append our partial list
                    this_group.add_members(rec_out.get_attr_values(member_key))
    
        return this_group

    def change_ou(self, ldap_rec, correct_ou):
        """return the new OU on a successful rename

        Generates a new DN and renames the LDAP record to change the OU.

        ldap_rec   - a ldaphelper.LDAPSearchResult object
        correct_ou - a list of OU's under the base DN, ex: ['staff', 'employees']

        """
        logger1 = logging.getLogger('changeOU')
        cn_only = "CN=%s" % ldap_rec.get_attr_values('cn')[0]
        s_ou = ",".join(["ou=%s" % n for n in correct_ou])
        new_superior = "%s,%s" % (s_ou, self.get_search_base())
        logger1.debug("Moving %s to %s", cn_only, new_superior)
        self._connection.rename_s(ldap_rec.get_dn(), cn_only, new_superior)
        return new_superior

    def delete(self, dn):
        """Delete a record from the LDAP tree"""
        logger1 = logging.getLogger('delete')
        rid = self._connection.delete_s(dn)
        try:
            result = self._connection.result(rid)
        except TypeError as ex:
            result = None
            logger1.debug("Unable to retrieve LDAP result for %s: %s", dn, ex)
        user = dn[:dn.find("dc=") - 1]
        logger1.info("Deleted %s...result=%s", user, result)

    def update_email(self, rec, new_mail, force_update=False, key_attr='uid'):
        """Write the address to the LDAP record"""
        logger1 = logging.getLogger('updateEmail')

        user_rec = LdapUser(rec)
        if user_rec.username is not None:
            search_filter = "%s=%s" % (key_attr, user_rec.username)
        else:
            search_filter = "employeeNumber=%s" % rec.record_id

        scope = ldap.SCOPE_SUBTREE
        retrieve_attributes = ['uid', 'mail']
        result_set = []
        attr = None

        try:
            result_id = self._connection.search(self.get_search_base(),
                                                scope,
                                                search_filter,
                                                retrieve_attributes)
            while 1:
                result_type, result_data = self._connection.result(result_id, LDAP_ONE_REC)
                # print "DEBUG: %s, %s" % (result_type, result_data)
                if not result_data:
                    break
                else:
                    if result_type == ldap.RES_SEARCH_ENTRY:
                        result_set.append(result_data)
            # see if we have an entry to update
            if len(result_set) > 0:
                # if we don't have a username yet, see if ldap knows it
                if user_rec.username is None:
                    for entry in result_set:
                        attr = entry[0][1]
                        if 'uid' in attr:
                            user_rec.username = attr['uid'][0]
                        else:
                            raise Exception("[FATAL] No username for %s" % rec.record_id)
                if new_mail is None:
                    new_mail = user_rec.username + DEFAULT_EMAIL_DOMAIN

                for entry in result_set:
                    dn = entry[0][0]
                    attr = entry[0][1]
                    ldap_action = None
                    if 'mail' in attr:
                        old_mail = attr['mail'][0]
                        if attr['mail'][0] != new_mail or force_update:
                            ldap_action = ldap.MOD_REPLACE
                        else:
                            logger1.info("No changes required for %s", user_rec.username)
                    else:
                        old_mail = ""
                        logger1.info("Adding %s to %s", new_mail, user_rec.username)
                        ldap_action = ldap.MOD_ADD
                    # now perform the action, if any
                    if ldap_action is not None:
                        modlist = [
                            (ldap_action, 'mail', new_mail),
                            (ldap.MOD_REPLACE, 'sn', user_rec.last),
                            (ldap.MOD_REPLACE, 'givenName', user_rec.first),
                            (ldap.MOD_REPLACE, 'cn', user_rec.getFullName()),
                            (ldap.MOD_REPLACE, 'displayName', user_rec.getDispName())]
                        self._connection.modify(dn, modlist)
                        logger1.info("Updating %s from %s to %s",
                                     user_rec.username,
                                     old_mail,
                                     new_mail)
                        logger1.info("Modlist for %s: %s", user_rec.username, modlist)
            else:
                logger1.warning("No results for %s ", user_rec.username)
        except ldap.LDAPError as error_message:
            logger1.error(error_message, exc_info=True)
        except IndexError as val:
            logger1.error("Index error for %s, %s, %s", user_rec.username, val, result_set)
        except KeyError as val:
            logger1.error("Key error for %s: %s, %s", user_rec.username, val, attr)

    def update_office_codes(self, username, office_codes, key_attr='uid'):
        """Search for the dn and update the office codes if necessary"""
        # global _ldap_session
        scope = ldap.SCOPE_SUBTREE
        search_filter = "%s=%s" % (key_attr, username)
        retrieve_attributes = ['employeeType']
        result_set = []
        attr = None
        timeout = 0

        logger1 = logging.getLogger('updateCodes')

        try:
            result_id = self._connection.search(self.get_search_base(), scope, search_filter, retrieve_attributes)
            while 1:
                result_type, result_data = self._connection.result(result_id, timeout)
                if not result_data:
                    break
                else:
                    if result_type == ldap.RES_SEARCH_ENTRY:
                        result_set.append(result_data)
            # see if we have an entry to update
            if len(result_set) > 0:
                for entry in result_set:
                    dn = entry[0][0]
                    attr = entry[0][1]
                    # only make a change if there is a difference
                    old_codes = []
                    if 'employeeType' in attr:
                        if attr['employeeType'] != office_codes:
                            logger1.info("Updating %s from %s to %s" % (username,
                                                                        old_codes,
                                                                        office_codes))
                            modlist = [(ldap.MOD_REPLACE, 'employeeType', office_codes)]
                            self._connection.modify(dn, modlist)
                        else:
                            logger1.debug("%s is up to date" % username)
                    elif len(office_codes) > 0:
                        logger1.info("Adding %s to %s" % (office_codes, username))
                        modlist = [(ldap.MOD_ADD, 'employeeType', office_codes)]
                        self._connection.modify(dn, modlist)
                    else:
                        logger1.debug("Nothing to do for " + username)
            else:
                logger1.warning("No results for " + username)
        except ldap.LDAPError as error_message:
            logger1.error(error_message, exc_info=True)
        except IndexError as val:
            logger1.error("Index error for %s, %s, %s", username, val, result_set)
        except KeyError as val:
            logger1.error("Key error for %s: %s, %s", username, val, attr)

    def update_group_members(self, group_dn, add_list, del_list, owner_add_list=None, owner_del_list=None,
                             owner_attr=DEFAULT_OWNER_ATTR):
        """Add and remove values from the member attribute of the group.

        group_dn   - the group's distinguished name (DN)

        add_list   - list of member DN's to add

        del_list   - list of member DN's to remove

        owner_list - list of DN's who are considered the owner of the
                     group, not updated if empty

        owner_attr - the attribute where the owners are stored (could be
                     'managedBy', 'secretary', etc.)

        :rtype : None
        """
        if owner_add_list is None:
            owner_add_list = []
        logger1 = logging.getLogger('updateGroupMembersAD')
        try:
            if len(add_list) > 0:
                modlist_add = [(ldap.MOD_ADD, 'member', i.encode()) for i in add_list]
                self._connection.modify_s(group_dn, modlist_add)
        except ldap.LDAPError as error_message:
            logger1.error("Unable to add %s to %s", repr(add_list), group_dn)
            logger1.error(error_message, exc_info=True)

        try:
            if len(del_list) > 0:
                if len(del_list) > 5000:
                    total = len(del_list)
                    remainder = total % 5000
                    slices = int(total / 5000)
                    steps = [(i * 5000, ((i + 1) * 5000) - 1) for i in range(0, slices)]
                    steps.append((slices * 5000, slices * 5000 + remainder))
                    for n in steps:
                        logger1.info("Removing %d to %d from %s", n[0], n[1], group_dn)
                        modlist_del = [(ldap.MOD_DELETE, 'member', i.encode()) for i in del_list[n[0]: n[1]]]
                        self._connection.modify_s(group_dn, modlist_del)
                else:
                    modlist_del = [(ldap.MOD_DELETE, 'member', i.encode()) for i in del_list]
                    self._connection.modify_s(group_dn, modlist_del)
        except ldap.LDAPError as error_message:
            logger1.error("Unable to delete %s from %s", repr(del_list), group_dn)
            logger1.error(error_message, exc_info=True)

        try:
            if owner_add_list is not None and len(owner_add_list) > 0:
                modlist_owner = [(ldap.MOD_ADD, owner_attr, i.encode()) for i in owner_add_list]
                self._connection.modify_s(group_dn, modlist_owner)
        except ldap.LDAPError as error_message:
            logger1.error("owner %s could not be saved to %s", repr(owner_add_list), repr(owner_attr))
            logger1.error(error_message, exc_info=True)

        try:
            if owner_del_list is not None and len(owner_del_list) > 0:
                modlist_owner = [(ldap.MOD_DELETE, owner_attr, i.encode()) for i in owner_del_list]
                self._connection.modify_s(group_dn, modlist_owner)
        except ldap.LDAPError as error_message:
            logger1.error("owner %s could not be saved to %s", repr(owner_add_list), repr(owner_attr))
            logger1.error(error_message, exc_info=True)

    def get_user_groups(self, username, key_attr='uid'):
        """Return a list of groups from the user's memberOf attribute."""
        logger1 = logging.getLogger("getUserGroups")
        prev_groups = []
        result_set = []
        try:
            # first get the user's dn
            search_filter = "%s=%s" % (key_attr, username)
            result_id = self._connection.search(self.get_search_base(), ldap.SCOPE_SUBTREE, search_filter, ['memberOf'])
            raw_res = self._connection.result(result_id, LDAP_ALL_REC)
            result_set = ldaphelper.get_search_results(raw_res)
            if len(result_set) > 0:
                # user_dn = result_set[0].get_dn().lower()
                if result_set[0].has_attribute('memberOf'):
                    prev_groups = [s.lower() for s in result_set[0].get_attr_values('memberOf')]
            else:
                logger1.error("LDAP record not found for %s", username)
        except ldap.LDAPError as error_message:
            logger1.error(error_message, exc_info=True)
        except IndexError as val:
            logger1.error("Index error for %s, %s, %s", username, val, result_set[0])
        return prev_groups

    def add_group(self, rule):
        """Return true if a new group was added based on the Colleague rule"""
        was_created = False
        logger1 = logging.getLogger('main.addGroup')

        new_rec = rule.createGroup()
        if new_rec is not None:
            group_dn = 'cn=%s,%s' % (rule.group_name, self.get_group_base())
            # print("[DEBUG] addGroup()", group_dn)
            # pprint(new_rec)
            result = self._connection.add_s(group_dn, new_rec)
            if not result:
                logger1.warning("LDAP command returned: %s", result)
            logger1.info("Created new group %s with %d members", rule.group_name, rule.member_count)
            was_created = True
        else:
            logger1.info("Skipped group %s, membership is 0", rule.group_name)
        return was_created

    def set_ldap_password(self, dn, new_pass):
        """Write a new password to the AD Server.

        Raises LDAPError if the change fails.

        """
        # 'changetype': 'modify'
        # replace: 'unicodePwd': ENCODED_PASSWD),

        new_password = ('"%s"' % new_pass).encode("utf-16-le")
        modlist = [(ldap.MOD_REPLACE, 'unicodePwd', new_password),
                   (ldap.MOD_REPLACE, 'userAccountControl', b'512')]
        self._connection.modify_s(dn, modlist)

    def enable_account(self, dn):
        """Enable the Active Directory account by setting the user control."""
        # changetype: modify
        # replace: userAccountControl
        # userAccountControl: 512
        logger1 = logging.getLogger('main.enableADAccount')
        try:
            modlist = [(ldap.MOD_REPLACE, 'userAccountControl', str(NORMAL_ACCOUNT).encode())]
            self._connection.modify_s(dn, modlist)
        except ldap.LDAPError as error_message:
            logger1.warning("Cannot enable account: %s", dn)
            logger1.error(error_message, exc_info=True)

    def set_password_to_not_expire(self, dn):
        """Update the user account control field so that the password will not expire."""
        logger1 = logging.getLogger('main.setPasswordtoNotExpire')
        try:
            modlist = [(ldap.MOD_REPLACE, 'userAccountControl', str(NORMAL_ACCOUNT & DONT_EXPIRE_PASSWORD).encode())]
            self._connection.modify_s(dn, modlist)
        except ldap.LDAPError as error_message:
            logger1.warning("Cannot set password expiration field: %s", dn)
            logger1.error(error_message, exc_info=True)

    def create_user(self, dn, attributes):
        """Create a new user record for the given DN.

        createUser(string, dict) -> None

        I normally use this function along with the LdapUser class.  For example:

          user_rec = LdapUser(base_dn="ou=cfcc users,dc=ad,dc=cfcc,dc=edu", username="guest.user")
          create_user(user_rec.getDn(), user_rec.asDict())

        raises ldap.LDAPError if the user cannot be created.
        raises LdapReturnCodeError if the result is not ldap.RES_ADD

        """
        mod_list = ldap.modlist.addModlist(attributes)
        result = self._connection.add_s(dn, mod_list)

        if not (isinstance(result, tuple) and result[0] == ldap.RES_ADD):
            raise LdapReturnCodeError(result[0])

    def modify_user(self, dn, attributes, old_attrs=None, force_mod=False):
        """Update all attributes in a user record in Active directory.

        Passing an empty dictionary for the old values means everything
        will be updated.

        """
        EXCLUDED_FIELDS = ["cn",
                           "userPrincipalName",
                           "userAccountControl",
                           "objectClass",
                           "sAMAccountName"]
        mod_list = []

        logger1 = logging.getLogger('main.modifyUser')
        if old_attrs is None:
            old_attrs = dict()
            for k in list(attributes.keys()):
                old_attrs[k] = b""
        for k in list(attributes.keys()):
            logger1.debug("Key %s, attribute value %s", k, attributes[k])
            if k not in EXCLUDED_FIELDS:
                if attributes[k] is None or attributes[k].strip() in [b"", b"-"]:
                    is_blank = True
                else:
                    is_blank = False
                if k in old_attrs:
                    if is_blank:
                        action = ldap.MOD_DELETE
                        attributes[k] = None
                    else:
                        action = ldap.MOD_REPLACE
                else:
                    if is_blank:
                        continue
                    else:
                        action = ldap.MOD_ADD
                m = (action, k, attributes[k])
                logger1.debug("modlist item: %r", m)
                mod_list.append(m)
        try:
            result = self._connection.modify_s(dn, mod_list)
            logger1.info("LDAP result = %s", result)
        except:
            logger1.error("modlist problem for %s", dn)
            pprint(mod_list)
            raise

        return True


def dn2username(dn_in, key="cn"):
    """Return the username portion of the distinguished name.

    dn_in - string with the distinguished name

    key   - name of the field that holds the username (default='cn')

    """
    start_str = "%s=" % key
    cn_beg = dn_in.find(start_str) + 4
    cn_end = dn_in.find(",", cn_beg)
    return dn_in[cn_beg:cn_end].lower()


def _create_controls(pagesize):
    if ldap.__version__.startswith('2.3'):
        lc = SimplePagedResultsControl(ldap.LDAP_CONTROL_PAGE_OID,
                                       True,
                                       (pagesize, ''))
    else:
        lc = SimplePagedResultsControl(True, pagesize, cookie='')
    return lc


def _get_pctrls(serverctrls):
    if ldap.__version__.startswith('2.3'):
        server_control = [c for c in serverctrls
                if c.controlType == ldap.LDAP_CONTROL_PAGE_OID]
    else:
        server_control = [c for c in serverctrls
                if c.controlType == SimplePagedResultsControl.controlType]
    return server_control


def _set_cookie(lc_object, pctrls, pagesize):
    if ldap.__version__.startswith('2.3'):
        est, cookie = pctrls[0].controlValue
        lc_object.controlValue = (pagesize, cookie)
    else:
        cookie = pctrls[0].cookie
        lc_object.cookie = cookie
    return cookie


class ActiveDirectoryGroup:
    def __init__(self):
        self._dn = None
        self._b_members = []
        self._b_owners = None

    def set_dn(self, new_dn):
        self._dn = new_dn

    def get_dn(self):
        return self._dn

    def exists(self):
        return self._dn is not None

    def add_b_members(self, b_new_members):
        self._b_members.extend(b_new_members)

    def add_members(self, new_members):
        self._b_members.extend([m.encode() for m in new_members])

    def get_b_members(self):
        return self._b_members

    def get_members(self):
        if self._b_members is not None:
            return [m.decode() for m in self._b_members]
        else:
            return None

    def empty(self):
        return len(self._b_members) == 0

    def add_owners(self, new_owners):
        if self._b_owners is None:
            self._b_owners = []
        self._b_owners.extend([o.encode() for o in new_owners])

    def get_b_owners(self):
        return self._b_owners

    def get_owners(self):
        if self._b_owners is not None:
            return [o.decode() for o in self._b_owners]
        else:
            return None

#!/usr/bin/env python
# Generate random passwords based on the dictionary method suggested by XKCD.
#
# Code ideas from http://www.thialfihar.org/projects/password_generator/

import random
import re

DEFAULT_DICTIONARY = "/usr/share/dict/words"


def randomWords(num, min_len=5, max_len=9, dictionary=None, bad_words=None):
    """Return a list of words selected randomly from a file.

    Word list defaults to the dictionary in /usr/share/dict/words which
    should work on most Linux distributions.

    Debian/Ubuntu can install alternate dictionaries, for example:

    # apt-get install wbritish

    """
    if dictionary is None:
        dictionary = DEFAULT_DICTIONARY
    r = random.SystemRandom()  # i.e. preferably not pseudo-random
    with open(dictionary, "r") as f:
        count = 0
        chosen = []
        for i in range(num):
            chosen.append("")
        prog = re.compile("^[a-z]{%d,%d}$" % (min_len, max_len))  # reasonable length, no proper nouns
        if (f):
            for word in f:
                if prog.match(word):
                    if word not in bad_words:
                        for i in range(num):  # generate all words in one pass thru file
                            if r.randint(0, count) == 0:
                                chosen[i] = word.strip()
                        count += 1
                    else:
                        print("Skipped a bad word: " + word)
    return (chosen)


def loadBadWords(filename):
    """Return a list of words that should not be used in a password."""
    wordlist = []
    try:
        with open(filename) as fd:
            for line in fd:
                s = line.strip()
                if s != "":
                    wordlist.append(s)
    except IOError as ex:
        print("Error loading word list from " + str(filename) + ", " + str(ex))
    return wordlist


def loadBadWords2(filename):
    """Return a dict of words that should not be used in a password."""
    wordlist = dict()
    try:
        with open(filename) as fd:
            for line in fd:
                s = line.strip()
                if s != "":
                    wordlist[s] = 1
    except IOError as ex:
        print("Error loading word list from " + str(filename) + ", " + str(ex))
    return wordlist


def genPassword(num=4, min_len=5, max_len=9, spacers='~*.-=+:', bad_words=dict(), dictionary=None, digitsAtEnd=0):
    """Generate a password of multiple words separated by a random character.

    Using the arguments you can customize the number of words, length of
    the words, type of spacers (can use None), and add optional numbers
    at the end.

    One of the words will be capitalized.

    """
    # select a spacer from the list
    if spacers is not None and len(spacers) > 0:
        spacer = random.choice(spacers)
    else:
        spacer = ""
    # get the random words
    words = randomWords(num, min_len, max_len, bad_words=bad_words, dictionary=dictionary)
    # capitalize one of the words
    which = random.randint(0, len(words) - 1)
    words[which] = words[which].capitalize()
    # merge the words
    plaintext = (spacer.join(words))
    # add digits (if any)
    for i in range(digitsAtEnd):
        plaintext += "%d" % random.randint(0, 9)
    return plaintext


def simplePassword(min_len=3, max_len=9, bad_words=dict(), dictionary=None):
    """Return a simple two-word, one-number password."""
    finished = False
    while not finished:
        words = randomWords(2, min_len, max_len, bad_words=bad_words, dictionary=dictionary)
        plaintext = "%s%s%d" % (words[0],
                                words[1].capitalize(),
                                random.randint(10, 99))
        if len(plaintext) >= 8:
            finished = True
    return plaintext

#
# See test/t_passwdgen.py for password generation tests
#

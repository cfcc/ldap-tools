"""Password generator module for cfccldap"""

import random
import string
import platform

if platform.system() == "SunOS":
    WORDS = "/usr/share/lib/dict/words"
else:
    WORDS = "/usr/share/dict/words"

NON_RANDOM_WORD_LIST = ['Bold', 'Cfcc', 'College', 'Guest', 'Invite', 'School', 'Study']

VALID_CHARS = (string.ascii_uppercase, string.punctuation, string.digits)

MAX_PASSWD_GEN_ATTEMPTS = 10


class PasswordGenError(Exception):
    def __init__(self, value, attempts=MAX_PASSWD_GEN_ATTEMPTS):
        self.value = value
        self.attempts = attempts

    def __str__(self):
        return "Unable to generate a password after %d attempts for %s" % (self.attempts, self.value)


def randomPassword(length=30, valid_chars=VALID_CHARS):
    """Return a random string to use as a password."""
    password = []
    for group in valid_chars:
        password += random.sample(group, 3)
    all_chars = []
    for i in range(len(valid_chars)):
        all_chars += valid_chars[i]
    password += random.sample(all_chars, length - len(password))
    random.shuffle(password)
    return ''.join(password)


def markovPassword(length=30, required_chars=VALID_CHARS):
    """Return a semi-random, pronouncible password"""
    pw_out = []
    onepass = Password()
    word_len = (length - len(required_chars)) / 2
    for group in required_chars:
        pw_out += random.sample(group, 1)
    for i in range(2):
        onepass.renew(word_len)
        pw_out += [str(onepass)]
    random.shuffle(pw_out)
    return ''.join(pw_out)


def adPassword(length=15):
    """Return a password that meets our complexity requirements.

    The characters O,0,1,I are removed to reduce confusion when
    reading the password.

    """
    valid_chars = (string.ascii_uppercase.translate({'O': None, 'I': None}),
                   string.digits.translate({'0': None, '1': None}))
    return markovPassword(length, valid_chars)


def nonRandomPassword(first_name, last_name, digitsAtEnd=4):
    """Return a password based on one word and some random digits."""
    finished = False
    attempt = 0
    plaintext = ""
    while not finished:
        plaintext = random.choice(NON_RANDOM_WORD_LIST)
        if first_name == "" or first_name.lower() not in plaintext.lower():
            if last_name == "" or last_name.lower() not in plaintext.lower():
                finished = True
        attempt += 1
        if attempt > MAX_PASSWD_GEN_ATTEMPTS:
            user_name = "%s %s" % (first_name, last_name)
            raise PasswordGenError(user_name, attempt)

    for i in range(digitsAtEnd):
        plaintext += "%d" % random.randint(0, 9)
    return plaintext


class Password(object):
    """
    This algorithm is from the Python Cookbook by Alex Martelli, Anna Ravenscroft and David Ascher.
    """
    def __init__(self):
        self.data = open(WORDS).read().lower()
        self.chars = []

    def renew(self, n, maxmem=3):
        """
        Accumulate into self.chars 'n' random characters, with a
        maximum-memory 'history' of 'maxmem' characters back.
        """
        if len(self.chars) > 0:
            self.chars = []
        for i in range(int(n)):
            # randomly "rotate" self.data
            randspot = random.randrange(len(self.data))
            self.data = self.data[randspot:] + self.data[:randspot]
            # get the n-gram
            where = -1
            # start by trying to locate the last maxmem characters in
            # self.chars.  If i<maxmem, we actually only get the last
            # i, i.e., all of self.chars -- but that's OK: slicing is
            # quite tolarant in this way, and it fits the algorithm
            locate = ''.join(self.chars[-maxmem:])
            while where < 0 and locate:
                # locate the n-gram in the data
                where = self.data.find(locate)
                # back off to a shorter n-gram if necessary
                locate = locate[1:]
                # if where = -1 and locate = '', we just pick
                # self.data[0] -- it's a random item within self.data,
                # thanks to the rotation
            c = self.data[where + len(locate) + 1]
            # we only want lowercase letters, so, if we picked
            # another kind of character, we just choose a random
            # letter instead
            if not c.islower(): c = random.choice(string.ascii_lowercase)
            # and finally we record the character into self.chars
            self.chars.append(c)
    
    def __str__(self):
        return ''.join(self.chars)


if __name__ == '__main__':
    """Usage: passgen.py [passwords [length [memory]]]"""
    import sys
    if len(sys.argv) > 1: dopass = int(sys.argv[1])
    else: dopass = 8
    if len(sys.argv) > 2: length = int(sys.argv[2])
    else: length = 10
    if len(sys.argv) > 3: memory = int(sys.argv[3])
    else: memory = 3
    onepass = Password()
    for i in range(dopass):
        onepass.renew(length, memory)
        print(onepass)
    print(adPassword(length))

"""LDAP helper module gives a nicer result from searches.

The original code came from the Python LDAP Applications article
(http://www.packtpub.com/article/python-ldap-applications-ldap-opearations)
written by Matt Butcher.

"""
import datetime

import ldif
from io import StringIO

import pytz
from ldap.cidict import cidict

important_attributes = ['cn',
                        'sAMAccountName',
                        'employeeNumber',
                        'givenName',
                        'sn',
                        'displayName',
                        'postalCode',
                        'whenCreated',
                        'whenChanged',
                        'memberOf',
                        'userAccountControl',
                        'badPwdCount',
                        'badPasswordTime',
                        'lastLogoff',
                        'lastLogon',
                        'pwdLastSet',
                        'accountExpires',
                        'logonCount',
                        'description',
                        'division',
                        'userPrincipalName',
                        'lockoutTime',
                        'lastLogonTimestamp',
                        'mail',
                        ]


def get_search_results(results):
    """Given a set of results, return a list of LDAPSearchResult
    objects.
    """
    res = []

    if type(results) == tuple and len(results) == 2 :
        (code, arr) = results
    elif type(results) == list:
        arr = results

    if len(results) == 0:
        return res

    for item in arr:
        res.append( LDAPSearchResult(item) )

    return res


class LDAPSearchResult:
    """A class to model LDAP results.
    """

    dn = ''

    def __init__(self, entry_tuple):
        """Create a new LDAPSearchResult object."""
        (dn, attrs) = entry_tuple
        if dn:
            self.dn = dn
            self.attrs = cidict(attrs)
        else:
            self.dn = ''
            self.attrs = cidict([])

    def __str__(self):
        return self.pretty_print()

    def get_attributes(self):
        """Get a dictionary of all attributes.
        get_attributes()->{'name1':['value1','value2',...],
                           'name2: [value1...]}
        """
        return self.attrs

    def set_attributes(self, attr_dict):
        """Set the list of attributes for this record.

        The format of the dictionary should be string key, list of
        string alues. e.g. {'cn': ['M Butcher','Matt Butcher']}

        set_attributes(attr_dictionary)
        """

        self.attrs = cidict(attr_dict)

    def has_attribute(self, attr_name):
        """Returns true if there is an attribute by this name in the
        record.

        has_attribute(string attr_name)->boolean
        """
        return attr_name in self.attrs

    def get_attr_values(self, key):
        """Get a list of attribute values.
        get_attr_values(string key)->['value1','value2']
        """
        return [a.decode() for a in self.attrs[key]]

    def get_attr_names(self):
        """Get a list of attribute names.
        get_attr_names()->['name1','name2',...]
        """
        return list(self.attrs.keys())

    def get_dn(self):
        """Get the DN string for the record.
        get_dn()->string dn
        """
        return self.dn

    def get_ou(self):
        """Return the primary OU as a lower case string"""
        # TODO: allow for multiple OU's?
        s_dn = self.dn.lower()
        ou_beg = s_dn.find("ou=") + 3
        ou_end = s_dn.find(",", ou_beg)
        return s_dn[ou_beg:ou_end]
                         
    def pretty_print(self):
        """Create a nice string representation of this object.

        pretty_print()->string
        """
        str_out = "DN: " + self.dn + "\n"
        for a, v_list in self.attrs.items():
            str_out = str_out + "Name: " + a + "\n"
            for v in v_list:
                str_out = str_out + "  Value: " + repr(v) + "\n"
        str_out += "========"
        return str_out

    def get_formatted_attributes(self):
        ts_to_convert = ['badPasswordTime',
                         'pwdLastSet',
                         'accountExpires',
                         'lastLogonTimestamp',
                         'lockoutTime',
                         ]
        attr_out = []
        for name in important_attributes:
            if name in self.attrs:
                if name in ts_to_convert:
                    values = [convert_timestamp(v) for v in self.attrs[name]]
                else:
                    values = [v.decode('utf8') for v in self.attrs[name]]
                for v in values:
                    attr_out.append([str(name), str(v)])
        return attr_out

    def to_ldif(self):
        """Get an LDIF representation of this record.

        to_ldif()->string
        """
        out = StringIO()
        ldif_out = ldif.LDIFWriter(out)
        ldif_out.unparse(self.dn, self.attrs)
        return out.getvalue()


def convert_timestamp(value):
    try:
        ad_ts = int(value)
    except ValueError:
        ad_ts = 0
    if ad_ts > 0:
        try:
            value = datetime.datetime(1601, 1, 1, tzinfo=datetime.timezone.utc) + datetime.timedelta(seconds=ad_ts / 10000000)
            return value.astimezone(pytz.timezone('US/Eastern'))
        except OverflowError:
            return None
    else:
        return None

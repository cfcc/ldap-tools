import sqlobject


class Rules(sqlobject.SQLObject):
    className = sqlobject.StringCol(length=50)
    groupName = sqlobject.StringCol(length=50)
    groupDesc = sqlobject.StringCol(length=255, default="")
    limitRemoval = sqlobject.BoolCol(default=False)
    afterHours = sqlobject.BoolCol(default=False)
    runAlone = sqlobject.BoolCol(default=False)
    allowEmpty = sqlobject.BoolCol(default=False)
    disabled = sqlobject.BoolCol(default=False)

    ruleData = sqlobject.MultipleJoin('RuleData', joinColumn='rule_id')
    ownerSet = sqlobject.ForeignKey('OwnerSet', dbName='owner_set_id')

    def get_as_dict(self):
        rule_out = dict()

        rule_out["class_name"] = self.className

        if self.groupName is not None:
            rule_out["group_name"] = self.groupName

        if self.groupDesc is not None:
            rule_out["desc"] = self.groupDesc

        rule_out["limit_removal"] = self.limitRemoval
        rule_out["after_hours"] = self.afterHours
        rule_out["allow_empty"] = self.allowEmpty

        for row in self.ruleData:
            if row.paramName in rule_out:
                if isinstance(rule_out[row.paramName], list):
                    rule_out[row.paramName].append(row.paramValue)
                else:
                    rule_out[row.paramName] = [rule_out[row.paramName], row.paramValue]
            else:
                rule_out[row.paramName] = row.paramValue

        if self.ownerSet:
            my_owners = []
            for row in self.ownerSet.owners:
                my_owners.append(row.personId)
            rule_out["owners"] = my_owners

        return rule_out


class RuleData(sqlobject.SQLObject):
    paramName = sqlobject.StringCol(length=255)
    paramValue = sqlobject.StringCol(length=255)

    rule = sqlobject.ForeignKey('Rules', dbName='rule_id')


class Owners(sqlobject.SQLObject):
    personId = sqlobject.StringCol(length=10)

    ownerSet = sqlobject.ForeignKey('OwnerSet', dbName='owner_set_id')


class OwnerSet(sqlobject.SQLObject):
    name = sqlobject.StringCol(length=25)

    owners = sqlobject.MultipleJoin('Owners', joinColumn="owner_set_id")

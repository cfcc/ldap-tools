"""Package to interface between a LDAP tree and Colleague.

Subpackages include:

config - connection information for the Colleague environments
dbutil - functions to retrieve information from Colleague
group - functions to update LDAP group membership
informer - functions to update informer group membership
unidata_model - Intercall definitions for the UniData files
user - Class to represent LDAP users
util - LDAP update functions

"""
import datetime
import logging
import logging.handlers
from . import util
import intercall
from . import dbutil

from .unidata_model import role_from_office_code, meta

from .unidata_model import AcadPrograms
from .unidata_model import Address
from .unidata_model import Applicants
from .unidata_model import Depts
from .unidata_model import Divisions
from .unidata_model import Faculty
from .unidata_model import Hrper
from .unidata_model import OrgEntity
from .unidata_model import OrgEntityEnv
from .unidata_model import Person
from .unidata_model import PersonPin
from .unidata_model import Perstat
from .unidata_model import Position
from .unidata_model import Schools
from .unidata_model import Staff
from .unidata_model import Students
from .unidata_model import StudentAdvisement
from .unidata_model import StuSecAttend
from .unidata_model import Valcodes
from .unidata_model import UtSeclass
from .unidata_model import UtOpers
from .unidata_model import X810IdCard

# public UniFile objects
acad_programs = None
address = None
applicants = None
core_valcodes = None
depts = None
divisions = None
hrper = None
faculty = None
org_entity = None
org_entity_env = None
person = None
person_pin = None
perstat = None
position = None
schools = None
staff = None
stu_sec_attend = None
students = None
student_advisement = None
ut_opers = None
ut_seclass = None
x810_id_card = None

# Max Log Size (20 MB)
max_log_bytes = 20 * 1024 * 1024


def connectAll(db_url, db_password = None):
    """Establish connections to UniData.

    The connection functions will request the input of a password.

    Create the file objects once the session is established.

    """
    global acad_programs
    global address
    global applicants
    global core_valcodes
    global depts
    global divisions
    global hrper
    global faculty
    global org_entity
    global org_entity_env
    global person
    global person_pin
    global position
    global perstat
    global schools
    global staff
    global students
    global student_advisement
    global stu_sec_attend
    global ut_opers
    global ut_seclass
    global x810_id_card

    dbutil.connect(db_url, db_password)
    db_session = dbutil.getDbSession()

    acad_programs = AcadPrograms(db_session)
    address = Address(db_session)
    applicants = Applicants(db_session)
    depts = Depts(db_session)
    divisions = Divisions(db_session)
    core_valcodes = Valcodes(db_session, 'CORE.VALCODES')
    hrper = Hrper(db_session)
    faculty = Faculty(db_session)
    org_entity = OrgEntity(db_session)
    org_entity_env = OrgEntityEnv(db_session)
    person = Person(db_session)
    person_pin = PersonPin(db_session)
    perstat = Perstat(db_session)
    position = Position(db_session)
    schools = Schools(db_session)
    staff = Staff(db_session)
    students = Students(db_session)
    student_advisement = StudentAdvisement(db_session)
    stu_sec_attend = StuSecAttend(db_session)
    ut_opers = UtOpers(db_session)
    ut_seclass = UtSeclass(db_session)
    x810_id_card = X810IdCard(db_session)


def disconnectAll():
    """Close all the active sessions"""
    dbutil.getDbSession().close()


def isOpen():
    """Return the status of the connection (true or false)"""
    return dbutil.isOpen()


def getTable(name, codetable=None):
    """Return a UniFile object named table in UniData.

    The object is connected to the current database session.

    If the UniFile class for that table has not been defined, then
    we'll generate a new one based on the UniData dictionary.

    """
    db_session = dbutil.getDbSession()
    # TODO: Need to check if the table is even available in UniData!
    try:
        obj = None
        cmd = "obj = %s(db_session" % (name)
        if codetable is not None:
            cmd += ", '%s'" % codetable
        cmd += ")"
        exec(cmd)
    except NameError as ex:
        cls = meta(name)
        if codetable is not None:
            obj = cls(db_session, codetable)
        else:
            obj = cls(db_session)
    return obj


def getEmail(person_id, email_type=None, required=False):
    """Returns the preferred email address in Colleague or None.

    If required is true, then return the first address instead of None.
    """
    logger1 = logging.getLogger('main.getEmail')
    new_mail = None
    per = person.get(person_id)
    which_email = None
    try:
        if email_type is not None:
            for i, v in enumerate(per.person_email_types):
                if v == email_type:
                    which_email = i
                    break
    except Exception as e:
        logger1.error("Failed to get email for %s: %s", person_id, e)

    # we will look for a preferred email if we didn't find one
    # matching the type code
    try:
        if which_email is not None:
            for i, v in enumerate(per.person_preferred_email):
                if v == "Y":
                    which_email = i
                    break
    except Exception as e:
        logger1.error("Failed to get email for %s: %s", person_id, e)

    if which_email is not None:
        new_mail = per.person_email_addresses[which_email]
    elif required:
        # this no preferred address was found, but the caller still wants an
        # address returned
        new_mail = per.person_email_addresses[0]

    # sometimes the preferred email is blank, we'll raise an warning
    # and set it back to None
    if new_mail is not None and new_mail.strip() == "":
        logger1.warning("Preferred email address is blank for %s", person_id)
        new_mail = None
    return new_mail


def getPositionTitle(person_id):
    """Return the title of the primary position"""
    logger1 = logging.getLogger('main.getPositionTitle')
    title = ""
    try:
        today = datetime.date.today()
        hrp_rec = hrper.get(person_id)
        for stat_id in hrp_rec.all_statuses:
            ps_rec = perstat.get(stat_id)
            if ps_rec.end_date == '' or ps_rec.end_date > today:
                pri_pos = position.get(ps_rec.primary_pos_id)
                title = pri_pos.title
                break
    except Exception:
        logger1.debug("Record not found for %s", person_id)
    return title


def getDeptDesc(person_id):
    """Return the department description

    Note: to search AD for records missing the dept desc, I used the following filter:

    '(&(!(departmentNumber=*))(objectClass=person))'

    I searched in ou=Employees,ou=CFCC Users,dc=ad,dc=cfcc,dc=edu and found about 45 records.

    """
    logger1 = logging.getLogger('main.getDeptDesc')
    desc = ""
    try:
        id_card = x810_id_card.get(person_id)
        if id_card.dept != "":
            pri_dept = depts.get(id_card.dept)
            desc = pri_dept.desc
    except Exception as exc:
        logger1.debug('DEPTS record not found for %s', person_id)
    return desc


def getOffice(person_id):
    """Attempt to get the office information from HRPER"""
    logger1 = logging.getLogger('main.getOffice')
    rec = None
    try:
        rec = hrper.get(person_id, must_exist=True)
    except intercall.main.IntercallError:
        logger1.debug("HRPER not found for %s", person_id)
    except IOError as e:
        logger1.debug("Unable to read HRPER for %s: %s", person_id, e)
    return rec


def getZip(person_id):
    """Attempt to return the zip code from the preferred address.

    Only the first 5 digits of the code are returned.

    If the zip is not found, return a blank string instead.

    """
    logger1 = logging.getLogger('main.getZip')
    my_zip = ""
    try:
        person_rec = person.get(person_id)
        addr_rec = address.get(person_rec.preferred_address)
        if addr_rec is not None:
            try:
                my_zip = addr_rec.zip[:addr_rec.zip.index("-")]
            except ValueError:
                my_zip = addr_rec.zip
    except intercall.main.IntercallError as exc:
        logger1.warning("No zipcode found for %s: %s", person_id, exc)
    return my_zip


def getLdapContext(person_id):
    """Returns a list of strings with the LDAP context (i.e. OU).

    Originally this was based on the DATATEL subroutine that used
    the WHERE.USED in PERSON to determine which LDAP context was
    appropriate.  But since we have experienced problems with
    WHERE.USED not being accurate or up to date and so we switched
    to a new algorithm that looks at multiple fields to determine
    what the context should be.

    """
    logger1 = logging.getLogger('main.getLdapContext')
    # first we check if the ID card is for employee
    try:
        rec = x810_id_card.get(person_id)
        id_type = rec.type
    except intercall.main.IntercallError:
        id_type = None
        logger1.warning("X810.ID.CARD not found for %s", person_id)
    if id_type == 'EMPLOYEE':
        # then we check WHERE.USED for a FACULTY status
        try:
            rec = person.get(person_id)
            where_used = rec.where_used
        except intercall.main.IntercallError:
            logger1.warning("PERSON WHERE.USED not found for %s", person_id)
            where_used = []
        if 'FACULTY' in where_used:
            context = util.WHERE_USED_CONTEXTS['FACULTY']
        else:
            context = util.WHERE_USED_CONTEXTS['EMPLOYES']
    else:
        # if not an employee, the user account will end up in the
        # Students OU
        if isinstance(util.DEFAULT_CONTEXT, str):
            context = [util.DEFAULT_CONTEXT]
        else:
            context = util.DEFAULT_CONTEXT
    return context


def initLogging(log_file="ldap_update.log", log_level=logging.INFO, full_logging=False, truncate=False, debug=False):
    """Establish the logging parameters.

    Keywords:
      log_file - filename for the log entries       (default: ldap_update.log)
      log_level - specify the report level          (default: logging.INFO)
      full_logging - record messages to a file      (default: False)
      truncate - overwrite the existing log file    (default: False)
      debug - set to True to override the log level (default: False)
    """
    global DEBUG
    
    # define a default logger for the main process
    logger = logging.getLogger('main')
    # if the calling code uses the debug option, the the log_level
    # parameter is ignored
    if debug:
        DEBUG = True
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(log_level)

    if full_logging:
        console_level = logging.ERROR
        if log_file is not None:
            m_handler = logging.handlers.RotatingFileHandler(log_file,
                                                             maxBytes=max_log_bytes,
                                                             backupCount=5)
            m_formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s', '%m-%d %H:%M')
            m_handler.setFormatter(m_formatter)
            logger.addHandler(m_handler)
        else:
            hdlr = logging.handlers.SysLogHandler(facility = logging.handlers.SysLogHandler.LOG_USER)
            formatter = logging.Formatter('%(filename)s: %(levelname)s: %(message)s')
            hdlr.setFormatter(formatter)
            logger.addHandler(hdlr)
    else:
        console_level = log_level

    # define a handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(console_level)

    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logger.addHandler(console)


def fullname(o):
    """Grab the class full name for logging"""
    module = o.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return o.__class__.__name__
    return module + '.' + o.__class__.__name__


class RemoveMaxPercentExceededError(Exception):
    def __init__(self, amount):
        self.amount = amount
        Exception.__init__(self)

    def __str__(self):
        in_percent = self.amount * 100
        return "Refusing to remove users from group, would be removing %d percent of the users" % in_percent

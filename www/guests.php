<html>
<head>
<title>Guest Account Status</title>
<style>
/* code from http://www.w3schools.com/css/tryit.asp?filename=trycss_table_fancy */
#guests {
	font-family: sans-serif;
	width: 100%;
	border-collapse: collapse;
}

#guests td,#guests th {
	font-size: 1em;
	border: 1px solid #005CE6;
	padding: 3px 7px 2px 7px;
}

#guests th {
	font-size: 1.1em;
	text-align: left;
	padding-top: 5px;
	padding-bottom: 4px;
	background-color: #2E8AE6;
	color: #ffffff;
}

#guests tr.alt td {
	color: #000000;
	background-color: #CCFFFF;
}

#guests td.pending {
	text-align: center;
	font-variant: small-caps;
}
</style>
</head>
<body>
<?php
/**
 * A quick PHP script that displays the contents of the Guest Account database on Garnet.
 *
 * Used as a reference for what is currently active and any accounts that have been requested but not yet created.
 *
 * @author AD\jfriant80
 *        
 */
include 'config.php';

$db = new mysqli($hostname, $username, $password, $dbname);

if ($db->connect_errno) {
	echo "Failed to connect to the database: " . $db->connect_error;
} else {
	$result = $db->query ( 'SELECT * FROM guest_accounts ORDER BY begin_date, end_date' );
	?>
<table id="guests">
		<tr>
			<th>Full Name</th>
			<th>Event Name</th>
			<th>Begin Date</th>
			<th>End Date</th>
			<th>Submitter</th>
			<th>CC</th>
			<th>Username</th>
		</tr>
<?php
	$alt = FALSE;
    if ($result) {
        while ( $row = $result->fetch_assoc() ) {
            if ($alt == TRUE) {
                echo '<tr class="alt">';
                $alt = FALSE;
            } else {
                echo "<tr>\n";
                $alt = TRUE;
            }
            echo "<td>" . $row ['user_first'] . " " . $row ['user_last'] . "</td>\n";
            echo "<td>" . $row ['event_name'] . "</td>\n";
            echo "<td>" . $row ['begin_date'] . "</td>\n";
            echo "<td>" . $row ['end_date'] . "</td>\n";
            echo "<td>" . $row ['submitter'] . "</td>\n";
            echo "<td>" . $row ['cc_addr'] . "</td>\n";
            if ($row ['username'] == "") {
                echo '<td class="pending">pending</td>';
            } else {
                echo "<td>" . $row ['username'] . "</td>\n";
            }
            echo "</tr>\n";
        }
        $result->free();
    }
	$db->close();
}
?>
</table>
</body>
</html>
